"""
DBcomphist.py
Plot comparitive histograms, includes a normalisation function, adapted for D/B signal comparison

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""

from ROOT import TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, TLorentzVector
import ROOT as r
import math

############################################################################################################################################################

# plot histograms
def plot_distr(variables1, variables2, tree_1, tree_2,  output_tree):

    # constants (needed for cuts)
    cLight      = 0.299792458
    massB       = 5279.58
    massK       = 493.677
    massPi      = 139.57
    massP       = 938.27
    massMu      = 105.658   
    massDp      = 1869.62 
    # define 4-vector and 3-vector classes
    MyLorentzVector = r.Math.PtEtaPhiMVector
    ThreeVector = r.Math.XYZVector
    
    # get variables from decay tree
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()

    input1=tree_1;
    input2=tree_2;

    print("input1", input1)
    print("input2", input2)
    
    file1 = TFile.Open(input1);
    file2 = TFile.Open(input2);
    Tree1 = file1.Get("DecayTree")
    Tree2 = file2.Get("DecayTree")

    nEntries1=Tree1.GetEntriesFast()                                                                                                                  
    nEntries2=Tree2.GetEntriesFast()
    
    #limit large dataset
    
    nEntries1 = 72001
    nEntries2 = 38746	
    
    n_vars1=len(variables1)
    n_vars2=len(variables2)
    
    # create arrays for data
    histograms1=[]
    histograms2=[]

    # create canvas
    c=r.TCanvas()
    # specify stat box content
    r.gStyle.SetOptStat(2222)
    
    # set up arrays of histograms to fill with data
    for i in range(n_vars1):
        title1="Kpipi "+str(variables1[i][0])
        name1=str(variables1[i][0])+'_Kpipi'
        histograms1.append(TH1F(name1,title1,variables1[i][3], variables1[i][1],variables1[i][2]))

    for i in range(n_vars2):
        title2="Kpipi "+str(variables2[i][0])
        name2=str(variables2[i][0])+'_Bsig'
        histograms2.append(TH1F(name2,title2,variables2[i][3], variables2[i][1],variables2[i][2]))
        
###KPiPi Data
            
    print("\n\nKpipi data:")    
    for entry in range(nEntries1):
        if(entry%10000==0):
            print(entry)
         
        Tree1.GetEntry(entry) 
        # get variables from decay tree
        K_PT          = getattr(Tree1,"K_PT")
        K_ETA         = getattr(Tree1,"K_ETA")
        K_PHI         = getattr(Tree1,"K_PHI")

        Pi1_PT        = getattr(Tree1,"Pi1_PT")
        Pi1_ETA       = getattr(Tree1,"Pi1_ETA")
        Pi1_PHI       = getattr(Tree1,"Pi1_PHI")

        Pi2_PT        = getattr(Tree1,"Pi2_PT")
        Pi2_ETA       = getattr(Tree1,"Pi2_ETA")
        Pi2_PHI       = getattr(Tree1,"Pi2_PHI")

        D_M           = getattr(Tree1,"D_M")
        D_PT          = getattr(Tree1,"D_PT")
        D_ETA         = getattr(Tree1,"D_ETA")

        # loop over number of histograms required (number of variables)  
        for i in range(n_vars1):
            value1 = getattr(Tree1,variables1[i][0]);
            # fill histogram with Kpipi data
            histograms1[i].Fill(value1)
            
###B signal Data

    print("\n\nBsig data:")    
    for entry in range(nEntries2):
        if(entry%10000==0):
            print(entry)
            
        Tree2.GetEntry(entry) 
        # get variables from decay tree
        PiT_PT        = getattr(Tree1,"PiT_PT")
        PiT_ETA       = getattr(Tree1,"PiT_ETA")
        PiT_PHI       = getattr(Tree1,"PiT_PHI")

        PiS_PT        = getattr(Tree1,"PiS_PT")
        PiS_ETA       = getattr(Tree1,"PiS_ETA")
        PiS_PHI       = getattr(Tree1,"PiS_PHI")

        # loop over number of histograms required (number of variables)  
        for i in range(n_vars2):
            value2 = getattr(Tree2,variables2[i][0]);
            # fill histogram with Bsigdata
            histograms2[i].Fill(value2)
       
    # create file to write out to?
    f = TFile(output_tree,"RECREATE")
    
###looping over variables to create histograms for
    
    for i in range(6):
        histograms1[i].SetMarkerColor(4)
        histograms1[i].SetLineColor(4)
        histograms1[i].GetXaxis().SetTitle(str(variables1[i][0]))
        histograms1[i].GetYaxis().SetTitle("Relative number of events")
        histograms1[i].Draw()
        c.Update()

    # normalise histograms for comparison
        histograms2[i].Scale(histograms1[i].Integral("width")/histograms2[i].Integral("width"))
        histograms2[i].SetMarkerColor(2)
        histograms2[i].SetLineColor(2)
        histograms2[i].Draw("same")
        
        #Setting y axes
        if (max(histograms1[i])>max(histograms2[i])):
            histograms1[i].GetYaxis().SetRangeUser(0,max(histograms1[i])+100)
            histograms2[i].GetYaxis().SetRangeUser(0,max(histograms1[i])+100)
        else:
            histograms1[i].GetYaxis().SetRangeUser(0,max(histograms2[i])+100)
            histograms2[i].GetYaxis().SetRangeUser(0,max(histograms2[i])+100)
        
        c.Update()
        
    # draw legend
        leg = r.TLegend(0.7,0.35,0.85,0.45)
        leg.SetFillStyle(0)
        leg.SetFillColor(0)
        leg.SetBorderSize(0)
        leg.SetTextFont(132)
        leg.SetTextSize(0.035)
        leg.AddEntry(histograms1[i], str(variables1[i][0])+' Kpipi', "P")
        #leg.AddEntry(histograms2[i], str(variables2[i][0])+' Bsignal', "P")
        leg.Draw()
        c.Update()

    # save histograms as .pdf
        c.SaveAs("./DBplottrials2/Plot_Kpipi"+str(variables1[i][0])+'_Bsig'+str(variables2[i][0])+'Trial.pdf')
        histograms1[i].Write()
        histograms2[i].Write()
        f.cd()
        c.Write()
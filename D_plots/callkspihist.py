"""
call kspi hist

calls histogram generating script for kspi

"""

import sys

#from comp_var_veto import plot_X
from kspihist import plot_distr

variables = [['D_ETA', 0, 5, 100], ['D_M', 1810, 1940, 100], ['D_PT', 2000, 15000, 100], ['KS_PT', 700, 8000, 100], ['KS_ETA', 1, 5, 100], ['KS_PHI', -3.5, 3.5, 100], ['H_PT', 1500, 10000, 100], ['H_ETA', 2, 5, 100], ['H_PHI', -3.5, 3.5, 100]]

tree_2 = "/afs/cern.ch/user/s/sahughes/public/D_plots/Kspi_cut.root" #Data

output_tree = "test2.root"

#test adding input file and output files
#input_file = "test_input.root"
#output_file = "test_output.root"

plot_distr(variables, tree_2, output_tree)
"""
triggersel.py
creates tuple with signal pion branch

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""

from ROOT import TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, TLorentzVector
import ROOT as r

############################################################################################################################################################

#define trigger pion selector cut

def write_small_tuple(input_file, output_file):

    import math

    cLight  = 0.299792458

    massB   = 5279.58
    massK   = 493.677
    massPi  = 139.57
    massP   = 938.27
    massMu  = 105.658


    fin = r.TFile(input_file)
    dirin = fin.GetDirectory("")
    tin = dirin.Get("DecayTree;1")
    fout = r.TFile(output_file,"RECREATE")

    tout = r.TTree("DecayTree","DecayTree")
    r.gROOT.ProcessLine(\
        "struct MyStruct{\
        Int_t Pi1IDint;\
        Int_t Pi2IDint;\
        Int_t PitIDint;\
        Int_t PisIDint;\
        Float_t Pi1ptfloat;\
        Float_t Pi1etafloat;\
        Float_t Pi1phifloat;\
        Float_t Pi2ptfloat;\
        Float_t Pi2etafloat;\
        Float_t Pi2phifloat;\
        Float_t Pitptfloat;\
        Float_t Pitetafloat;\
        Float_t Pitphifloat;\
        Float_t Pisptfloat;\
        Float_t Pisetafloat;\
        Float_t Pisphifloat;\
        Float_t Kptfloat;\
        Float_t Ketafloat;\
        Float_t Kphifloat;\
        };")
    from ROOT import MyStruct
    add_vars = MyStruct()

###initialise output branches
  
    tout.Branch('Pi1_PT', r.AddressOf(add_vars,'Pi1ptfloat'), 'Pi1_PT/F')
    tout.Branch('Pi1_ID', r.AddressOf(add_vars,'Pi1IDint'), 'Pi1_ID/I')
    tout.Branch('Pi1_ETA', r.AddressOf(add_vars,'Pi1etafloat'), 'Pi1_ETA/F')
    tout.Branch('Pi1_PHI', r.AddressOf(add_vars,'Pi1phifloat'), 'Pi1_PHI/F')
    
    tout.Branch('Pi2_PT', r.AddressOf(add_vars,'Pi2ptfloat'), 'Pi2_PT/F')
    tout.Branch('Pi2_ID', r.AddressOf(add_vars,'Pi2IDint'), 'Pi2_ID/I')
    tout.Branch('Pi2_ETA', r.AddressOf(add_vars,'Pi2etafloat'), 'Pi2_ETA/F')
    tout.Branch('Pi2_PHI', r.AddressOf(add_vars,'Pi2phifloat'), 'Pi2_PHI/F')
    
    tout.Branch('PiT_PT', r.AddressOf(add_vars,'Pitptfloat'), 'PiT_PT/F')
    tout.Branch('PiT_ID', r.AddressOf(add_vars,'PitIDint'), 'PiT_ID/I')
    tout.Branch('PiT_ETA', r.AddressOf(add_vars,'Pitetafloat'), 'PiT_ETA/F')
    tout.Branch('PiT_PHI', r.AddressOf(add_vars,'Pitphifloat'), 'PiT_PHI/F')
    
    tout.Branch('PiS_PT', r.AddressOf(add_vars,'Pisptfloat'), 'PiS_PT/F')
    tout.Branch('PiS_ID', r.AddressOf(add_vars,'PisIDint'), 'PiS_ID/I')
    tout.Branch('PiS_ETA', r.AddressOf(add_vars,'Pisetafloat'), 'PiS_ETA/F')
    tout.Branch('PiS_PHI', r.AddressOf(add_vars,'Pisphifloat'), 'PiS_PHI/F')

    tout.Branch('K_PT', r.AddressOf(add_vars,'Kptfloat'), 'K_PT/F')
    tout.Branch('K_ETA', r.AddressOf(add_vars,'Ketafloat'), 'K_ETA/F')
    tout.Branch('K_PHI', r.AddressOf(add_vars,'Kphifloat'), 'K_PHI/F')

#   nEntries=tin.GetEntriesFast()
    counter = 0   
#limit size
    nEntries = 422940

    for entry in xrange(nEntries):
        tin.GetEntry(entry)
        if(entry%10000==0):
            print("Processing event number /n Please wait a bit longer, innit", entry)

        counter += 1
        #Add variables

        add_vars.Pi1ptfloat = getattr(tin,"Pi1_PT")
        add_vars.Pi1phifloat = getattr(tin,"Pi1_PHI")
        add_vars.Pi1etafloat = getattr(tin,"Pi1_ETA")

        add_vars.Pi2ptfloat = getattr(tin,"Pi2_PT")
        add_vars.Pi2phifloat = getattr(tin,"Pi2_PHI")
        add_vars.Pi2etafloat = getattr(tin,"Pi2_ETA")
        
        add_vars.Kptfloat = getattr(tin,"K_PT")
        add_vars.Kphifloat = getattr(tin,"K_PHI")
        add_vars.Ketafloat = getattr(tin,"K_ETA")
        
        ### Trigger Pion
        if (getattr(tin,"Pi1_PT")>getattr(tin,"Pi2_PT")):
            add_vars.Pitptfloat = getattr(tin,"Pi1_PT")
            add_vars.Pitphifloat = getattr(tin,"Pi1_PHI")
            add_vars.Pitetafloat = getattr(tin,"Pi1_ETA")
            
            add_vars.Pisptfloat = getattr(tin,"Pi2_PT")
            add_vars.Pisphifloat = getattr(tin,"Pi2_PHI")
            add_vars.Pisetafloat = getattr(tin,"Pi2_ETA")
        else:
            add_vars.Pitptfloat = getattr(tin,"Pi2_PT")
            add_vars.Pitphifloat = getattr(tin,"Pi2_PHI")
            add_vars.Pitetafloat = getattr(tin,"Pi2_ETA")
            
            add_vars.Pisptfloat = getattr(tin,"Pi1_PT")
            add_vars.Pisphifloat = getattr(tin,"Pi1_PHI")
            add_vars.Pisetafloat = getattr(tin,"Pi1_ETA")
            
        ### End of input

        tout.Fill()
        
    print ("All events processed.")
    print(counter, " events passed the cuts \n")


    tout.Write()
    fout.Close()
    fin.Close()
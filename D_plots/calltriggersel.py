"""
calltriggersel.py
calls trigger pion selection program

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""

import sys

from triggersel import write_small_tuple

############################################################################################################################################################

tree_2 = "/afs/cern.ch/user/s/sahughes/public/Bsignalsamples/Data2015_MagDown.root" #Run I data

output_tree = "Bsig2k15MD.root"

write_small_tuple(tree_2,output_tree)
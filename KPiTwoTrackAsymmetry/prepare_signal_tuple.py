"""
This script trims your signal tuple down to the bare minimum,
practical for the use in the reweighting for memory consumption reasons.
"""
import ROOT
import argparse

def get_program_options():
    parser = argparse.ArgumentParser()

    parser.add_argument('--k_px_alias', required=False,
                        help='Name of the Kaon PX variable in your signal tuple. ')
    parser.add_argument('--k_py_alias', required=False,
                        help='Name of the Kaon PY variable in your signal tuple. ')
    parser.add_argument('--k_pz_alias', required=False,
                        help='Name of the Kaon PZ variable in your signal tuple. ')

    parser.add_argument('--pi_px_alias', required=False,
                        help='Name of the pion PX variable in your signal tuple. ')
    parser.add_argument('--pi_py_alias', required=False,
                        help='Name of the pion PX variable in your signal tuple. ')
    parser.add_argument('--pi_pz_alias', required=False,
                        help='Name of the pion PX variable in your signal tuple. ')

    ''' pt eta phi '''
    parser.add_argument('--pi_pt_alias', required=False,
                        help='Name of the pion PT variable in your signal tuple. ')
    parser.add_argument('--pi_eta_alias', required=False,
                        help='Name of the pion ETA variable in your signal tuple. ')
    parser.add_argument('--pi_phi_alias', required=False,
                        help='Name of the pion PHI variable in your signal tuple. ')

    parser.add_argument('--k_pt_alias', required=False,
                        help='Name of the Kaon PT variable in your signal tuple. ')
    parser.add_argument('--k_eta_alias', required=False,
                        help='Name of the Kaon ETA variable in your signal tuple. ')
    parser.add_argument('--k_phi_alias', required=False,
                        help='Name of the Kaon PHI variable in your signal tuple. ')

    parser.add_argument('--sweight_var', required=False,
                        help='When set, sWeights are used for the given signal tuple.')

    parser.add_argument('--output_filename', required=True,
                        help='Filename where the trimmed tuple is saved.')

    parser.add_argument( "-f", "--input_tuple_file", action="store", dest="s_file_kpi",
                       required=True,
                       help="Path to file with signal tuples.")

    parser.add_argument( "-t", "--input_tuple_name", action="store", dest="s_tree_kpi",
                       required=True,
                       help="Path to tree in the root file (e.g. PhiPi/DecayTree)")
    

    return parser


if __name__ == '__main__':
    """
        This program captures the user input to the configuration of the
        signal tuple. The possibility is to run this script with
        various weighting strategies and momentum variables (pt-eta-phi
        and Cartesian).

        Available options: see --help
    """
    parser = get_program_options()
    options = parser.parse_args()
    
    variables_to_save = []
    
    if options.k_pt_alias and options.k_eta_alias and options.k_phi_alias:
        """ use (pt,eta,phi) for the Kaon """
        print "Using pt/eta/phi variables for your signal Kaon."
        variables_to_save += [ options.k_pt_alias,
                              options.k_eta_alias,
                              options.k_phi_alias ]
    elif options.k_px_alias and options.k_py_alias and options.k_pz_alias:
        print "Using px/pt/pz variables for your signal Kaon."
        variables_to_save += [ options.k_px_alias,
                              options.k_py_alias,
                              options.k_pz_alias ]
    else:
        print "Please specify the kinematics of the signal kaon."
        exit()

    if (options.pi_pt_alias and options.pi_phi_alias and options.pi_eta_alias):
        """ use (pt,eta,phi) for the Pion """
        print "Using pt/eta/phi variables for your signal Pion."
        variables_to_save += [ options.pi_pt_alias,
                              options.pi_eta_alias,
                              options.pi_phi_alias ]
    elif options.pi_px_alias and options.pi_py_alias and options.pi_pz_alias:
        print "Using px/pt/pz variables for your signal Pion."
        variables_to_save += [ options.pi_px_alias,
                              options.pi_py_alias,
                              options.pi_pz_alias ]
    else:
        print "Please specify the kinematics of the signal pion."
        exit()

    if options.sweight_var:
        variables_to_save += [ options.sweight_var ]
    
    file_path = options.s_file_kpi
    tuple_path = options.s_tree_kpi

    input_file = ROOT.TFile.Open( file_path, "READONLY");
    input_tuple = input_file.Get( tuple_path )
    
    print "Saving: "
    print variables_to_save;
    print "From path " + tuple_path
    
    output_file = ROOT.TFile.Open( options.output_filename, "RECREATE" )
    
    input_tuple.SetBranchStatus("*", 0)
    for variable in variables_to_save:
        input_tuple.SetBranchStatus ( variable, 1 );
        
    new_tree = input_tuple.CopyTree("1")
    output_file.cd();
    new_tree.Write();
    
    output_file.Close()
    input_file.Close();
"""
Wrapper around HepML for the K-pi asymmetry.

@author Adam Davis <adam.davis@cern.ch>
"""

import sys, os

# is this needed?
from ROOT import *

import numpy as np
import root_numpy
import configuration
import pandas, root_pandas #datasets which are nicer.

from hep_ml import reweight

from ExplicitBinnedReweighter import BinsReweighter as ExplicitBR

import logging
import names

""" For debugging """
def mem_usage(pandas_obj):
    if isinstance(pandas_obj,pandas.DataFrame):
        usage_b = pandas_obj.memory_usage().sum()
    else: # we assume if not a df it's a series
        usage_b = pandas_obj.memory_usage()
    usage_mb = usage_b / 1024 ** 2 # convert bytes to megabytes
    return "{:03.2f} MB".format(usage_mb)

class HepMLReweighter:
    """
    Class to use hep_ml to reweight datasamples

    Usual process:
    0. Initialize
    1. Set the datasets
    2. Choose what you want to reweight first.
    3. Reweight
    4. Extract the weights.
    5. Append the weights to the dataset, then write out the data.
    """
    def __init__(self, output_directory, enable_bdt=False, 
                 explicit_bins_kpipi_to_signal=None,
                 explicit_bins_kspi_to_kpipi=None):
        #dataset dictionary
        self.dataset = {}
        self.weights = {}
        self.reweighters = {}

        self.logger = logging.getLogger('HepMLReweighter')

        self.logger.info("Using the HepML reweighter.")

        self.output_directory = output_directory
        
        self.enable_bdt = enable_bdt
        self.n_estimators=200
        self.max_depth=3
        
        self.n_bins = 7
        self.n_neighs = 0
        
        self.explicit_bins = {};
        
        # Convert the explicit binning to a correct array format.
        if explicit_bins_kpipi_to_signal is not None:
            self.explicit_bins["kpipi"] = [ np.array( x, dtype=float ) for x in explicit_bins_kpipi_to_signal]

        if explicit_bins_kspi_to_kpipi is not None:
            self.explicit_bins["kspi"] = [ np.array( x, dtype=float )  for x in explicit_bins_kspi_to_kpipi] 
        
    def get_weighted_tree_name(self):
        return names.tree_name_reweighter_output

    def set_dataset(self,
                    name,
                    tuple_file_configuration,
                    branches,
                    decay_configuration,
                    variables_required_for_weighting,
                    cuts = None,):

        print "Variables needed for weighting: {}".format(", ".join(variables_required_for_weighting))
        [fname, treename] = [tuple_file_configuration.file_path, tuple_file_configuration.tuple_name]

        final_columns = [str for str in branches]

        # collect the branches required from the decay configuration
        for [index, daughter_particle] in decay_configuration.daughter_particles.iteritems():
            particle_name = daughter_particle.canonical_name
            tuple_branch_configuration = daughter_particle.get_tuple_variable_configuration()

            if tuple_branch_configuration.isCartesian():
                px = tuple_branch_configuration.varA
                py = tuple_branch_configuration.varB
                pz = tuple_branch_configuration.varC

                if particle_name + "_" + configuration.variables["pt"].canonical_name in variables_required_for_weighting:
                    branches.append('sqrt((' + px + ')**2 + (' + py + ')**2)')
                    final_columns.append(particle_name + "_" + configuration.variables["pt"].canonical_name)
                    self.logger.debug("Adding column " + final_columns[-1])
                    self.logger.debug("Adding branch " + branches[-1])

                if particle_name + "_" + configuration.variables["eta"].canonical_name in variables_required_for_weighting:
                    branches.append('-0.5*TMath::Log( (1- ' + pz + '/sqrt(' + px + "**2 + " + py + "**2 + "+ pz + '**2))/(1+ '+pz+'/sqrt('+px+'**2 + ' + py + '**2 + ' + pz + '**2)) )')
                    final_columns.append(particle_name + "_" + configuration.variables["eta"].canonical_name)
                    self.logger.debug("Adding column " + final_columns[-1])
                    self.logger.debug("Adding branch " + branches[-1])

                if particle_name + "_" + configuration.variables["phi"].canonical_name in variables_required_for_weighting:
                    branches.append('TMath::ATan2(' + py + ',' + px + ')')
                    final_columns.append(particle_name + "_" + configuration.variables["phi"].canonical_name)
                    self.logger.debug("Adding branch " + branches[-1])
                    self.logger.debug("Adding column " + final_columns[-1])

                if particle_name + "_" + configuration.variables["momentum"].canonical_name in variables_required_for_weighting:
                    branches.append('sqrt((' + px + ')**2 + (' + py + ')**2 + (' + pz + ')**2)')
                    final_columns.append(particle_name + "_" + configuration.variables["momentum"].canonical_name)
                    self.logger.debug("Adding column " + final_columns[-1])
                    self.logger.debug("Adding branch " + branches[-1])

            else:
                pt = tuple_branch_configuration.varA
                eta = tuple_branch_configuration.varB
                phi = tuple_branch_configuration.varC
                momentum="sqrt({}**2 * (cosh({}))**2)".format(pt, eta)

                if particle_name + "_" + configuration.variables["pt"].canonical_name in variables_required_for_weighting:    
                    branches.append(pt)
                    final_columns.append(particle_name + "_" + configuration.variables["pt"].canonical_name)
                    self.logger.debug("Adding branch " + branches[-1])
                    self.logger.debug("Adding column " + final_columns[-1])

                if particle_name + "_" + configuration.variables["eta"].canonical_name in variables_required_for_weighting:    
                    branches.append(eta)
                    final_columns.append(particle_name + "_" + configuration.variables["eta"].canonical_name)
                    self.logger.debug("Adding branch " + branches[-1])
                    self.logger.debug("Adding column " + final_columns[-1])

                if particle_name + "_" + configuration.variables["phi"].canonical_name in variables_required_for_weighting:    
                    branches.append(phi)
                    final_columns.append(particle_name + "_" + configuration.variables["phi"].canonical_name)
                    self.logger.debug("Adding branch " + branches[-1])
                    self.logger.debug("Adding column " + final_columns[-1])
                    
                if particle_name + "_" + configuration.variables["momentum"].canonical_name in variables_required_for_weighting:    
                    branches.append( momentum )
                    final_columns.append(particle_name + "_" + configuration.variables["momentum"].canonical_name)
                    self.logger.debug("Adding branch " + branches[-1])
                    self.logger.debug("Adding column " + final_columns[-1])

        if decay_configuration.useSWeights():
            branches += [ decay_configuration.getSWeightVar() ]
            final_columns.append( decay_configuration.getSWeightVar() )
        if decay_configuration.canonical_name is not "signal":
            self.logger.debug( 'No weights for this channel.' )
            #branches.append('(abs({0} - 1869.) < 12)'.format(decay_configuration.get_mass_variable()))
            #final_columns.append('fakeSWeights')

        self.logger.info("Registering decay {} in the data store.".format(name))
        self.logger.info("Saving columns: {}.".format(", ".join(final_columns)))
        
        if decay_configuration.canonical_name is not "signal":
            branches += ["1."]
            final_columns += ["weight"];
        
        self.dataset[name] = root_numpy.root2array(fname, treename, branches, cuts)
        self.dataset[name].dtype.names = final_columns
        
        self.logger.info("Registered decay {} in the data store with {} entries.".format(name,
                                                                                         len(self.dataset[name])))
        

    def do_reweighting(self, data2reweight, 
                       fixedData, vars,
                       chan, test = False, 
                       weights_for_fixed_data = None,
                       original_weights = None,
                       multiply_weights=False, 
                       chan_to_save_as=None):
        """
        Function to actually do the reweighting
        data2reweight =  the data you want to reweight
        fixedData is the data you want to weight to
        chan =  name of the dataset to save the weights to
        test = Do you want to skip the reweighting to make things go faster?
        weights_for_fixed_data = weights for the data you want to weight to.
        """
        self.logger.info( 'starting reweighting' )

        self.logger.debug("Reweighting {} to {}".format(data2reweight, fixedData))

        localdata2reweight = pandas.DataFrame(self.dataset[data2reweight][vars])
        localfixeddata = pandas.DataFrame(self.dataset[fixedData][vars])

        if original_weights is None:
            orig_weights = np.ones(len(localdata2reweight)) #see if passing a weight of ones for the data to reweight helps
        else:
            orig_weights = original_weights
        
        if test ==True:
            gb_weights = np.ones(len(localdata2reweight))
            self.weights[chan]=gb_weights
            return;
        #print localdata2reweight,localfixeddata

        if self.enable_bdt:
            self.logger.info("BDT reweighting is enabled. This can take long.")
            self.reweighters[chan] = reweight.GBReweighter(n_estimators=self.n_estimators, 
                                                           learning_rate=0.1, 
                                                           max_depth=self.max_depth, 
                                                           min_samples_leaf=1000, 
                                                           gb_args={'subsample': 0.7})
        elif chan in self.explicit_bins:
            bin_sizes = [ len(x)+1 for x in self.explicit_bins[ chan ] ]
            print bin_sizes
            print self.explicit_bins[ chan ]
            self.reweighters[chan] = ExplicitBR(
                                       bin_sizes, 
                                       n_neighs=self.n_neighs)
            self.logger.info("Using explicit bins for {} reweighting. ".format(chan))
            
            for bin_size in bin_sizes:
                self.logger.debug("Using explicit bin size for {} reweighting: {}. ".format(chan, str(bin_size)))

        else:
            self.reweighters[chan] = reweight.BinsReweighter(n_bins=self.n_bins, n_neighs=self.n_neighs)

        #reweighter1.fit(sample1, sample2, sample1Weights, sample2Weights, binning)
        
        if chan in self.explicit_bins:
            self.reweighters[chan].fit(localdata2reweight, localfixeddata,
                                   original_weight=orig_weights,
                                   target_weight=weights_for_fixed_data,
                                   CustomBinning=self.explicit_bins[ chan ])
        else:
            self.reweighters[chan].fit(localdata2reweight, localfixeddata,
                                   original_weight=orig_weights,
                                   target_weight=weights_for_fixed_data) # add the weights for the local fixed data if need be
        
        #get the weights for the dataset
        if not multiply_weights:
            gb_weights = self.reweighters[chan].predict_weights(localdata2reweight)
        else:
            gb_weights = self.reweighters[chan].predict_weights(localdata2reweight, original_weight=orig_weights)
        
        if not chan_to_save_as:
            chan_to_save_as = chan
        
        self.weights[chan_to_save_as]=gb_weights
        self.logger.info( 'done reweighting for channel ' + chan )

    def get_output_filename(self, channel):
        return "{}temp_{}.root".format(self.output_directory, channel)

    def write_new_tree(self,channel):
        #append the weights:
        #make frame out of the weights
        output_filename = self.get_output_filename(channel)

        self.logger.debug("Writing new trees to {}.".format(output_filename))
        self.dataset[channel]['weight'] = self.weights[channel]
        root_numpy.array2root(self.dataset[channel], 
                              output_filename, 
                              treename=self.get_weighted_tree_name(), 
                              mode='recreate')
        
        self.logger.info("Wrote temporary tree for decay {} to {}".format(channel, output_filename))

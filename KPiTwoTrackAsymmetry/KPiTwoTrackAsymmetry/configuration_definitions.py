__author__ = "Laurent Dufour <laurent.dufour@cern.ch>"
__date__   = "September 2016"
__all__    = ( "MagnetPolarity",
               "DaughterParticle",
               "Variable",
               "TupleVariableConfiguration",
               "Decay",
               "Variable",
               "DaughterParticle"  )

class Named:
    """ Objects of this type have names, which have
        multiple representations (TLatex, LaTeX, file names
        and canonical names, which are passed to e.g.
        command line arguments

        A dictionary-like API is supported to provide
        backwards compatibility with a dictionary
        structure."""

    def __init__(self, canonical_name, file_name, tlatex_name, latex_name):
        self.canonical_name = canonical_name;
        self.file_name= file_name
        self.tlatex_name = tlatex_name
        self.latex_name = latex_name

    def __getitem__(self, attr):
        """ Backwards compatibility with dictionary-style access."""
        return self.__dict__[ attr ]

class MagnetPolarity(Named):
    def __init__(self, canonical_name, file_name, tlatex_name, latex_name):
        Named.__init__(self, canonical_name, file_name, tlatex_name, latex_name)

class TupleVariableConfiguration():
    def __init__(self, varA, varB, varC, isCartesian):
        self.varA = varA
        self.varB = varB
        self.varC = varC

        self.cartesian = isCartesian

    def isCartesian(self):
        return self.cartesian

class DaughterParticle(Named):
    def __init__(self, canonical_name, file_name, tlatex_name, latex_name, pid):
        Named.__init__(self, canonical_name, file_name, tlatex_name, latex_name)
        self.pid = pid

    def set_tuple_configuration(self, tuple_variable_configuration):
        self.tuple_variable_configuration = tuple_variable_configuration

    def get_tuple_variable_configuration(self):
        return self.tuple_variable_configuration

class Variable(Named):
    def __init__(self, canonical_name, file_name, tlatex_name, latex_name,
                 inline_description):
        Named.__init__(self, canonical_name, file_name, tlatex_name, latex_name)

        self.inline_description = inline_description

    def getMinimum(self):
        return self.minimum

    def setMinimum(self, minimum):
        return self.minimum

    def getMaximum(self):
        return self.maximum

    def setMaxiumum(self, maximum):
        self.maximum = maximum

class Decay(Named):
    def __init__(self, canonical_name, file_name, tlatex_name, latex_name, daughter_particles):
        Named.__init__(self, canonical_name, file_name, tlatex_name, latex_name)

        self.daughter_particles = daughter_particles
        self.extra_variables_required_for_cut = []
        self.cuts = None

    def set_mass_variable(self, mass_variable):
        self.mass_variable = mass_variable

    def get_mass_variable(self):
        return self.mass_variable;

    def setSWeightVar(self, sWeightVar):
        self.sweightvar = sWeightVar

    def getSWeightVar(self):
        return self.sweightvar

    def useSWeights(self):
        return hasattr(self, 'sweightvar')

    def getCuts(self):
        return self.cuts

    def setCuts(self, cuts):
        self.cuts = cuts

    def addCut(self, cut):
        if self.cuts:
            self.cuts +=  " && " + cut 
        else:
            self.cuts = cut

    def get_extra_variables_required_for_cut(self):
        return self.extra_variables_required_for_cut;
        
    def get_id_variable(self):
        return self.id_variable
    
    def set_id_variable(self, id_variable):
        self.id_variable = id_variable
        


class FitConfiguration:
    def __init__(self, fit_range, try_johnson, default_bkg, try_exponential_bkg):
        self.fit_range = fit_range
        self.try_johnson = try_johnson
        self.default_bkg = default_bkg
        self.try_exponential_bkg = try_exponential_bkg
        
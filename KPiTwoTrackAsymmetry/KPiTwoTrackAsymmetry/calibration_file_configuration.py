"""
 Standard file locations and tuple configuration of the
 calibration samples (i.e. K-pi-pi and Ks-pi) on EOS.

 Uses the fixed file library, which has a default tuple location.
"""
__author__ = "Laurent Dufour <laurent.dufour@cern.ch>"
__date__   = "September 2016"
__all__    = ( "calibration_configuration",
               "configure_reweighter"  )

import file_library
import configuration

from configuration_definitions import FitConfiguration

''' set the actual configuration of the tuple branches in the decay '''

def get( year, polarity, low_stats=False ):
    """ Currently this is just a fixed setting for all years/polarity.
    Can be changed. """

    calibration_file_library_kpipi = file_library.FixedFileLibrary("kpipi", "DecayTree", low_stats)
    calibration_file_library_kspi = file_library.FixedFileLibrary("kspi", "DecayTree", low_stats)

    kpipi_tuple_confuguration = configuration.decays["kpipi"]
    kpipi_kminus_config = configuration.TupleVariableConfiguration(
                                                          "K_PT",
                                                          "K_ETA",
                                                          "K_PHI", False)
    kpipi_probepi_config = configuration.TupleVariableConfiguration(
                                                          "Pi2_PT",
                                                          "Pi2_ETA",
                                                          "Pi2_PHI", False)
    kpipi_piplus_config = configuration.TupleVariableConfiguration(
                                                          "Pi1_PT",
                                                          "Pi1_ETA",
                                                          "Pi1_PHI", False)
    kpipi_dplus_config = configuration.TupleVariableConfiguration(
                                                          "D_PT",
                                                          "D_ETA",
                                                          "D_PHI", False)

    kpipi_tuple_confuguration.daughter_particles["kminus"].set_tuple_configuration( kpipi_kminus_config )
    kpipi_tuple_confuguration.daughter_particles["piplus"].set_tuple_configuration( kpipi_probepi_config )
    kpipi_tuple_confuguration.daughter_particles["trigger_pi"].set_tuple_configuration( kpipi_piplus_config )
    kpipi_tuple_confuguration.daughter_particles["dplus"].set_tuple_configuration( kpipi_dplus_config )
    kpipi_tuple_confuguration.set_mass_variable("D_DTF_M_PV")
    kpipi_tuple_confuguration.set_id_variable("D_ID")

    kspi_tuple_confuguration = configuration.decays["kspi"]
    kspi_kshort_config = configuration.TupleVariableConfiguration(
                                                          "KS_PT",
                                                          "KS_ETA",
                                                          "KS_PHI", False)
    kspi_D_config = configuration.TupleVariableConfiguration(
                                                          "D_PT",
                                                          "D_ETA",
                                                          "D_PHI", False)
    kspi_trigger_pi_config = configuration.TupleVariableConfiguration(
                                                          "H_PT",
                                                          "H_ETA",
                                                          "H_PHI", False)
    kspi_tuple_confuguration.daughter_particles["ks"].set_tuple_configuration( kspi_kshort_config )
    kspi_tuple_confuguration.daughter_particles["dplus"].set_tuple_configuration( kspi_D_config )
    kspi_tuple_confuguration.daughter_particles["trigger_pi"].set_tuple_configuration( kspi_trigger_pi_config )
    kspi_tuple_confuguration.set_mass_variable("D_DTF_M_PV_CON")
    kspi_tuple_confuguration.set_id_variable("D_ID")
    
    if year == 2015 or year == 2016:
        kspi_tuple_confuguration.setSWeightVar("sWeight")
    
    # fit_range, try_johnson, default_bkg, try_exponential_bkg):
    fit_configuration_kspi = FitConfiguration(fit_range = [1810, 1925], 
                                              try_johnson = True,
                                              default_bkg = "exponential",
                                              try_exponential_bkg=False )
    
    fit_configuration_kpipi = FitConfiguration(fit_range = [1800, 1935], 
                                              try_johnson = True,
                                              default_bkg = "polynomial",
                                              try_exponential_bkg=True )
    
    # smaller fit range for 2016 due to trigger cuts.
    if year == 2016:
        fit_configuration_kpipi.fit_range = [1819,1919]
    

    return {"kspi":
                             {"decay_configuration": kspi_tuple_confuguration,
                              "fit_configuration": fit_configuration_kspi,
                              "file_list": calibration_file_library_kspi.get_tuple_information(year, polarity)},
            "kpipi":
                             {"decay_configuration": kpipi_tuple_confuguration,
                              "fit_configuration": fit_configuration_kpipi,
                              "file_list": calibration_file_library_kpipi.get_tuple_information(year, polarity)}}
"""
 Library to bundle all functionality related to the actual
 reweighting. Subsequent calls are made to the HepML
 reweighter.
 
 Because it can happen that one only wants to reweigh 
 *one* of the datasets (e.g. kpipi to signal), or
 only one of the daugther particles in the signal decay
 (for A(kk) for example), there are some additional 
 if-statements that might not directly make sense for
 the standard use case ("K+pi- asymmetry").
 modified to use K_pt instead of kaon momentum
"""
__author__ = "Laurent Dufour <laurent.dufour@cern.ch>"
__date__   = "September 2016"
__all__    = ( "perform_weighting",
               "configure_reweighter"  )

import reweight_hepml
import calibration_file_configuration
import file_library
import plotting
import configuration
import logging
import utilities
import names

import ROOT

class ReweightVariable:
    """
        Container of two canonical names of the particles and 
        variables.
    """
    def __init__(self, decay_daughter_name, variable_name):
        self.decay_daughter_name = decay_daughter_name
        self.variable_name = variable_name
    
    def to_tformula(self):
        return self.decay_daughter_name + "_" + self.variable_name

default_reweigh_kpipi_to_signal = [
                ReweightVariable( "kminus", "pt" ),  #ReweightVariable( "kminus", "momentum" ), replaced from
                ReweightVariable( "kminus", "eta" ),
                ReweightVariable( "piplus", "pt" ),
                ReweightVariable( "piplus", "eta" )
                                   ]

#default_reweigh_kpipi_to_signal = [
#                ReweightVariable( "kminus", "momentum" ),
#                ReweightVariable( "kminus", "eta" )
#                                   ]

default_reweigh_kspi_to_kpipi = [
                ReweightVariable( "dplus", "pt" ),
                ReweightVariable( "dplus", "eta" ),
                ReweightVariable( "trigger_pi", "pt" )
                                   ]

default_reweigh_kpipi_to_signal_kaon_only = [
                ReweightVariable( "kminus", "eta" ),
                ReweightVariable( "kminus", "pt" )
                                   ]

def configure_reweighter( reweighter, calibration_configuration,
                          variables_for_reweighting ):
    """
    Configurator for the HepML reweighter.
    
    @var reweighter HepML reweighter, or abstract interface.
    @var calibration_configuration Configuration of calibration samples, dict.
    @return Reweighter
    """

    kspi = calibration_configuration["kspi"]
    kpipi = calibration_configuration["kpipi"]

    fileList_kpipi = kpipi["file_list"]
    fileList_kspi = kspi["file_list"]
    decay_config_kpipi = kpipi["decay_configuration"]
    decay_config_kspi = kspi["decay_configuration"]

    for file in fileList_kpipi:
        reweighter.set_dataset( "kpipi",
                                file, [decay_config_kpipi.get_mass_variable(),
                                       decay_config_kpipi.get_id_variable()] + 
                                       decay_config_kpipi.get_extra_variables_required_for_cut(),
                                decay_config_kpipi, 
                                variables_required_for_weighting=variables_for_reweighting,
                                cuts=decay_config_kpipi.getCuts(),)

    for file in fileList_kspi:
        reweighter.set_dataset( "kspi",
                                file, [decay_config_kspi.get_mass_variable(),
                                       decay_config_kspi.get_id_variable()]+ 
                                       decay_config_kspi.get_extra_variables_required_for_cut(),
                                decay_config_kspi, 
                                variables_required_for_weighting=variables_for_reweighting,
                                cuts=decay_config_kspi.getCuts(),)

def perform_weighting(
                      signal_file_configuration, signal_tuple_config,
                      calibration_settings,
                      reweigh_variables_kpipi_to_signal,
                      reweigh_variables_kspi_to_kpipi,
                      year, polarity, 
                      output_directory, 
                      use_BDT,
                      n_bins_kpipi, n_bins_kspi,
                      explicit_bins_kpipi_to_signal=None, 
                      explicit_bins_kspi_to_kpipi=None,
                      do_reweighting=True, 
                      reweighting_steps=["kpipi", "kspi"]):
    
    fileList = signal_file_configuration.get_tuple_information(year, polarity );

    reweighter = reweight_hepml.HepMLReweighter(output_directory, enable_bdt=use_BDT,
                                                explicit_bins_kspi_to_kpipi=explicit_bins_kspi_to_kpipi,
                                                explicit_bins_kpipi_to_signal=explicit_bins_kpipi_to_signal);

    if not do_reweighting:
        return reweighter
    
    signal_file_decay_combo = {"decay_configuration": signal_tuple_config,
                              "file_list": fileList}

    variables_required_for_reweighting = ["trigger_pi_phi"]
    if "kpipi" in reweighting_steps:
        variables_required_for_reweighting += [ variable.to_tformula() for variable in reweigh_variables_kpipi_to_signal ]
    
    if "kspi" in reweighting_steps:
        variables_required_for_reweighting += [ variable.to_tformula() for variable in reweigh_variables_kspi_to_kpipi ]
    
    for file in fileList:
        reweighter.set_dataset( "signal",
                                file,[],
                                signal_tuple_config, cuts=None,
                                variables_required_for_weighting=variables_required_for_reweighting
                                )

    # do not load KsPi if it's not needed - saves memory. 
    if "kspi" not in reweighting_steps:
        calibration_settings["kspi"]["file_list"] = []
    
    configure_reweighter( reweighter, calibration_settings, variables_required_for_reweighting )
    
    reweighter.n_bins = n_bins_kpipi

    calibration_configuration = calibration_settings
    kspi = calibration_configuration["kspi"]
    kpipi = calibration_configuration["kpipi"]

    """ Apply weights to the Kpipi sample to match signal """
    if "kpipi" in reweighting_steps:
        if signal_tuple_config.useSWeights():
            sweightVar = signal_tuple_config.getSWeightVar()
            logging.info( "Using sWeights in the reweighting." )
            
            reweighter.do_reweighting('kpipi','signal',
                          [a.to_tformula() for a in reweigh_variables_kpipi_to_signal ],'kpipi',
                            weights_for_fixed_data = reweighter.dataset["signal"][ sweightVar],
                            #original_weights = reweighter.dataset["kpipi"]["fakeSWeights"]
                            )
        else:
            reweighter.do_reweighting('kpipi','signal',
                          [a.to_tformula() for a in reweigh_variables_kpipi_to_signal ],'kpipi',
                            #original_weights = reweighter.dataset["kpipi"]["fakeSWeights"]
                            )
        reweighter.write_new_tree('kpipi')


    reweighter.n_bins = n_bins_kspi
    if "kspi" in reweighting_steps:
        """ Apply weights to the KsPi sample to match Kpipi """
        # It would be better to have sWeights for the KsPi sample at hand...
        sweightvar_kspi = kspi["decay_configuration"].getSWeightVar() if kspi["decay_configuration"].useSWeights() else None
        
        original_weights_kspi = reweighter.dataset["kspi"][sweightvar_kspi] if sweightvar_kspi is not None else None;
        
        reweighter.do_reweighting('kspi', 'kpipi', 
                          [a.to_tformula() for a in reweigh_variables_kspi_to_kpipi ],
                          'kspi',
                          weights_for_fixed_data = reweighter.weights['kpipi'],
                          original_weights = original_weights_kspi)
        
        reweighter.n_bins = 40
        reweighter.do_reweighting('kspi', 'kpipi', 
                              ["trigger_pi_phi" ],
                              'kspi',
                              weights_for_fixed_data = reweighter.weights['kpipi'],
                              original_weights = reweighter.weights["kspi"][:],
                              multiply_weights=True)
        reweighter.write_new_tree("kspi")
    
    for use_weights in [True, False]:
        if "kpipi" in reweighting_steps:
            if "kminus" in signal_tuple_config.daughter_particles:
                to_draw_variables = [ var for var in ["momentum", "pt", "eta", "phi"] if "kminus_"+var in variables_required_for_reweighting ]
                
                # Unless we use the friend tree, then we can generate *all* variables.
                if configuration.using_friend_tree_for_illustrations:
                    to_draw_variables = [ "momentum", "pt", "eta", "phi" ] 
                
                verify_weighting(reweighter=reweighter,
                         reweigh_from_configuration=kpipi,
                         reweigh_from_daughter=kpipi["decay_configuration"].daughter_particles['kminus'],
                         reweigh_to_configuration=signal_file_decay_combo,
                         reweigh_to_daughter=signal_tuple_config.daughter_particles['kminus'],
                         to_draw_variables=to_draw_variables, 
                         output_directory=output_directory + "verify_weighting/kpipi_to_signal_",
                         name="kpipi_to_signal_k_",
                         use_weights=use_weights)
    
            if "piplus" in signal_tuple_config.daughter_particles:
                to_draw_variables = [ var for var in ["momentum", "pt", "eta", "phi"] if "piplus_"+var in variables_required_for_reweighting ] 
                
                verify_weighting(reweighter=reweighter,
                         reweigh_from_configuration=kpipi,
                         reweigh_from_daughter=kpipi["decay_configuration"].daughter_particles['piplus'],
                         reweigh_to_configuration=signal_file_decay_combo,
                         reweigh_to_daughter=signal_tuple_config.daughter_particles['piplus'],
                         to_draw_variables=to_draw_variables, 
                         output_directory=output_directory + "verify_weighting/kpipi_to_signal_",
                         name="kpipi_to_signal_pi_",
                         use_weights=use_weights)
    
        if "kspi" in reweighting_steps:
            to_draw_variables = [ var for var in ["momentum", "pt", "eta", "phi"] if "trigger_pi_"+var in variables_required_for_reweighting ] 
            
            verify_weighting(reweighter=reweighter,
                         reweigh_from_configuration=kspi,
                         reweigh_from_daughter=kspi["decay_configuration"].daughter_particles['trigger_pi'],
                         reweigh_to_configuration=kpipi,
                         reweigh_to_daughter=kpipi["decay_configuration"].daughter_particles['trigger_pi'],
                         to_draw_variables=to_draw_variables, 
                         output_directory=output_directory + "verify_weighting/kspi_to_kpipi_",
                         name="ks_to_kpipi_pi_",
                         use_weights=use_weights)
    
            to_draw_variables = [ var for var in ["momentum", "pt", "eta", "phi"] if "dplus_"+var in variables_required_for_reweighting ] 
            
            verify_weighting(reweighter=reweighter,
                         reweigh_from_configuration=kspi,
                         reweigh_from_daughter=kspi["decay_configuration"].daughter_particles['dplus'],
                         reweigh_to_configuration=kpipi,
                         reweigh_to_daughter=kpipi["decay_configuration"].daughter_particles['dplus'],
                         to_draw_variables=to_draw_variables, 
                         output_directory=output_directory + "verify_weighting/kspi_to_kpipi_",
                         name="ks_to_kpipi_dplus_",
                         use_weights=use_weights)

    
    return reweighter

def get_spectrum( reweighter,
                  decay_file_configuration, daughter,
                  to_draw_variables, name,
                  use_weights=True ):
    decay = decay_file_configuration['decay_configuration']
    logger = logging.getLogger("get_spectrum")
    logger.debug("Drawing spectra for decay {}".format(decay.canonical_name))
    
    bins = {}
    spectra = {}
    file_original = None;
    
    for variable in to_draw_variables:
        logger.debug("Creating bins for {}".format(variable))
        bins[ variable ] = plotting.standard_bins[ variable ]
        
        spectra[variable] = ROOT.TH1F(name + variable, name + variable,
                             len(bins[ variable ])-1, plotting.to_root_bins(bins[ variable ]))

    if decay.canonical_name is not "signal":
        filename = reweighter.get_output_filename( decay.canonical_name )
        
        file = ROOT.TFile.Open( filename, "READONLY" )
        tree = file.Get( names.tree_name_reweighter_output )
        
        weight = names.leaf_name_weight
        
        file_information = decay_file_configuration[ "file_list"][-1]
        filename_original = file_information.file_path
        tuple_path_orignial = file_information.tuple_name
        
        if configuration.using_friend_tree_for_illustrations:
            file_original = ROOT.TFile.Open( filename_original, "READONLY" );
            tree_original = file_original.Get( tuple_path_orignial );
            
            tree.AddFriend( tree_original );

        #weight = weight+"*" + decay.getSWeightVar()+")"
    else:
        file_information = decay_file_configuration['file_list'][-1]
        filename = file_information.file_path
        file = ROOT.TFile.Open( filename, "READONLY" )
        tree = file.Get( file_information.tuple_name )
        
        weight = "1"
    
    if not use_weights:
        weight = "(1)"
    
    if decay.useSWeights():
        weight = "(" + weight + " * " + decay.getSWeightVar() + ")"
    
    for variable in to_draw_variables:
        logger.debug("Getting tformula for {}".format(variable))
    
        to_draw_variable = configuration.get_tformula_for_variable( 
                                                               decay, 
                                                               daughter,
                                                               variable )
        logger.debug( "Trying to draw {}".format(to_draw_variable) );
        
        #if variable == "momentum": # GeV/c
        #    to_draw_variable = "({})/1000.0".format(to_draw_variable);

        logger.debug("Drawing {}".format(to_draw_variable))
        
        spectrum_from_temp = ROOT.TH1F(name + "temp_" + variable, name + "temp_" + variable,
                             len(bins[variable])-1, plotting.to_root_bins(bins[variable]))
        tree.Draw( "{}>>{}temp_{}".format(to_draw_variable, name, variable), weight)
        
        spectra[variable].Add( spectrum_from_temp )
        
    file.Close()
    
    if file_original is not None:
        file_original.Close();
    
    return spectra

def verify_weighting ( reweighter, 
                       reweigh_from_configuration, reweigh_to_configuration,
                       reweigh_from_daughter, reweigh_to_daughter,
                       to_draw_variables, 
                       output_directory, name,
                       use_weights=True ):
    """ 
    Function which generates histograms showing the
    kinematic distributions of the decay daughters. 
    """
    spectra_from = get_spectrum(reweighter, 
                                reweigh_from_configuration, 
                                reweigh_from_daughter, 
                                to_draw_variables,
                                name + "spectra_from_",
                                use_weights=use_weights)
    spectra_to = get_spectrum(reweighter, 
                              reweigh_to_configuration, 
                              reweigh_to_daughter, 
                              to_draw_variables,
                              name + "spectra_to_",
                              use_weights=use_weights)
    
    if use_weights:
        output_prefix = "weighted/"
    else:
        output_prefix = "unweighted/"
    
    utilities.check_output_directory( output_directory + output_prefix );
    
    for variable in to_draw_variables:
        if spectra_from[ variable ].Integral() > 0:
            spectra_from[ variable ].Scale(1./spectra_from[ variable ].Integral())
        else:
            logging.fatal("One of the variables had an empty spectrum! ({})".format(variable) )
            break;
            
        if spectra_to[ variable ].Integral() > 0:
            spectra_to[ variable ].Scale(1./spectra_to[ variable ].Integral())
        else:
            logging.fatal("One of the variables had an empty spectrum! ({})".format(variable) )
            break;
        
        plotting.output_kinematic_distributions(
                                    ss_kaon_spectrum=spectra_from[ variable ],
                                    os_kaon_spectrum=spectra_to[ variable ],
                                    particleA=reweigh_from_daughter,
                                    particleB=reweigh_to_daughter,
                                    variable=configuration.variables[ variable ],
                                    header="",
                                    output_file_name="{}{}spectrum_{}_{}_{}".format(output_directory, 
                                                                                    output_prefix,
                                                                                  reweigh_from_daughter.canonical_name,
                                                                                  reweigh_to_daughter.canonical_name,
                                                                                  variable)
                                    )
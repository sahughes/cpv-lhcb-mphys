"""
Container of standard names for histograms, axes and
particles. Also contains helper functions to generate
TFormulas for the given variable.

Definition of classes describing "named" configuration,
such as magnet polarity, Dalitz region and x-axes.

This ensures a uniform API for the TLatex name, LaTeX name,
file name and canonical name. The base class, Named, is
not exported in the module.
"""
__author__ = "Laurent Dufour <laurent.dufour@cern.ch>"
__date__   = "September 2016"
__all__    = ( "get_tformula_for_variable",
               "polarities",
               "years",
               "decays",
               "variables",
               "particles",
               "output_directories" )

import logging

from configuration_definitions import MagnetPolarity
from configuration_definitions import TupleVariableConfiguration
from configuration_definitions import Variable, DaughterParticle, Decay

from errors import ConfigurationError

using_friend_tree_for_illustrations = True
polarities = {"magup": MagnetPolarity("magup", "MagUp", "MagDown", "MagDown"),
              "magdown": MagnetPolarity("magdown", "MagDown", "MagDown", "MagDown")}

variables = {"momentum": Variable("momentum", "momentum", "#it{momentum} [GeV/#it{c}]",
                                  r"\it{momentum} [GeV/\it{c}]", "momentum"),
             "pidk": Variable("pidk", "pidk", "#it{PIDK}", r"DLL(K-\pip)", "pidk"),
             "pt": Variable("pt", "pt", "#it{p_{T}} [MeV/#it{c}]",
                            r"\it{p_{T}} [MeV/\it{c}]", "transverse momentum"),
             "eta": Variable("eta", "eta", "#it{#eta}", r"\it{\eta}", "pseudo rapidity"),
             "phi": Variable("phi", "phi", "#it{#phi}", r"\it{\phi}", "phi")}

particles = {
             "kspi": {"ks": DaughterParticle("ks", "Ks", "K_{s}^{0}", r"K^{0}_{s}", 321),
                      "trigger_pi": DaughterParticle("trigger_pi", "Pi", "#pi^{+}", r"\pi^{+}", 211),
                      "dplus": DaughterParticle("dplus", "dplus", "D^{+}", r"D^{+}", 420) },
             "kpipi": {"kminus": DaughterParticle("kminus", "K", "K^{#mp}", r"K^{\mp}", 321),
                       "piplus": DaughterParticle("piplus", "PiPlus1", "#pi^{#pm}", r"\pi^{\pm}", 221),
                       "trigger_pi": DaughterParticle("trigger_pi", "PiPlus2", "#pi^{#pm}", r"\pi^{\pm}", 221),
                       "dplus": DaughterParticle("dplus", "dplus", "D^{+}", r"D^{+}", 420)},
             "signal": {"kminus": DaughterParticle("kminus", "K", "K^{#mp}", r"K^{\mp}", 321),
                       "piplus": DaughterParticle("piplus", "PiPlus", "#pi^{#pm}", r"\pi^{\pm}", 221)}
             }


decays = {"kspi": Decay("kspi",
                         "KsPi",
                         "#it{D^{+}#rightarrowK_{s}^{0}#pi^{+}}",
                         r"\it{D^+ \to K_s^0 \pi^+}",
                         particles[ "kspi"] ),
          "kpipi": Decay("kpipi", "KPiPi",
                         "#it{D^{#pm}#rightarrowK^{#mp}#pi^{#pm}#pi^{#pm}}",
                         r"\it{D^{\pm} \to K^{\mp} \pi^{\pm} \pi^{\pm}}",
                         particles[ "kpipi"] ),
          "signal": Decay("signal",
                         "signal",
                         "#it{Signal}",
                         r"\it{Signal}",
                         particles[ "signal"] ),
          }

for decay_name, decay in decays.iteritems():
    for daughter_name, daughter_particle in decay.daughter_particles.iteritems():
        daughter_particle.tlatex_name = "[{}] {}".format(decay.tlatex_name, daughter_particle.tlatex_name) 
decays["kpipi"].set_mass_variable("D_DTF_M_PV")

years = [2011, 2012, 2015, 2016]

def get_tformula_for_variable( decay, daughter_particle, variable ):
    logger = logging.getLogger("get_tformula_for_variable")
    
    if not using_friend_tree_for_illustrations and decay.canonical_name is not "signal":
        if variable == "momentum":
            logger.debug("Returning momentum for non-signal")
            return "{}_{}/1000.0".format(daughter_particle.canonical_name, variables["momentum"].canonical_name)
        if variable == "pt":
            logger.debug("Returning pt for non-signal")
            return "{}_{}".format(daughter_particle.canonical_name, variables["pt"].canonical_name)
        if variable == "eta":
            logger.debug("Returning eta for non-signal")
            return "{}_{}".format(daughter_particle.canonical_name, variables["eta"].canonical_name)
        if variable == "phi":
            logger.debug("Returning phi for non-signal")
            return "{}_{}".format(daughter_particle.canonical_name, variables["phi"].canonical_name)
        else:
            logger.fatal("Could not decode variable " + variable)
    else:
        if not daughter_particle.get_tuple_variable_configuration():
            logger.fatal("TFormula requested but the tuple configuration is not filled!")
            raise ConfigurationError("get_tformula_for_variable", "Could not find tuple var config for decay daughter.")
            return # normally I would throw here...
    
        tuple_configuration = daughter_particle.get_tuple_variable_configuration()
        
        tuple_branch_configuration = daughter_particle.get_tuple_variable_configuration()
        if tuple_branch_configuration.isCartesian():
            px = tuple_branch_configuration.varA
            py = tuple_branch_configuration.varB
            pz = tuple_branch_configuration.varC
            
            if variable == "momentum":
                return "1./1000 * sqrt({}**2 + {}**2 + {}**2)".format(px, py, pz)
            if variable == "pt":
                return "sqrt({}**2+{}**2)".format(px, py)
            if variable == "eta":
                return '-0.5*TMath::Log( (1- ' + pz + '/sqrt(' + px + "**2 + " + py + "**2 + "+ pz + '**2))/(1+ '+pz+'/sqrt('+px+'**2 + ' + py + '**2 + ' + pz + '**2)) )'
            if variable == "phi":
                return 'TMath::ATan2(' + py + ',' + px + ')'
            else:
                logging.fatal("Could not decode variable " + variable)
        else:
            pt = tuple_branch_configuration.varA
            eta_var = tuple_branch_configuration.varB
            phi_var = tuple_branch_configuration.varC

            if variable == "momentum":
                return "1./1000 * sqrt({}**2 * (cosh({}))**2)".format(pt, eta_var)
            if variable == "pt":
                return pt
            if variable == "eta":
                return eta_var
            if variable == "phi":
                return phi_var
            else:
                logging.fatal("Could not decode variable " + variable)
        
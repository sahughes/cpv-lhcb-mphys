"""
Definitions for the required configuration
"""
__author__ = "Laurent Dufour <laurent.dufour@cern.ch>"
__date__   = "September 2016"
__all__    = ( "FileLibrary",
               "MutableFileLibrary",
               "FixedFileLibrary",
               "TuplePathInformation"  )

import logging
from abc import ABCMeta, abstractmethod

class FileLibrary:
    """ Definition of the interface for the file library. Abstract. """
    __metaclass__ = ABCMeta
    
    def __init__(self, decay):
        self.decay = decay

    @abstractmethod
    def get_tuple_information(self, year, polarity): pass

class TuplePathInformation:
    def __init__(self, file_path, tuple_name):
        self.file_path = file_path
        self.tuple_name = tuple_name

class MutableFileLibrary (FileLibrary):
    """ File library in which all paths are  
    set explicitly beforehand ( add_tuple_information ). """
    def __init__(self, decay):
        self.file_list = {}

        FileLibrary.__init__(self, decay)

        self.logger = logging.getLogger('MutableFileLibrary')

    def add_tuple_information(self, year, polarity, tuple_information):
        """ Explicitly set the tuple information.
        Only used for the signal sample to reweight to.

        @return void"""

        if year not in self.file_list:
            self.logger.debug("Creating file list for new year (and polarity)")
            self.file_list[ year ] = { polarity: [ ] }

        if polarity not in self.file_list[ year ]:
            self.logger.debug("Creating file list for new polarity")
            self.file_list[ year ][ polarity ] = [  ]

        self.file_list[ year ][ polarity ].append( tuple_information )

        self.logger.debug("Added tuple information ({}, {}) for {} {}".format(tuple_information.file_path,
                                                                          tuple_information.tuple_name,
                                                                          year,
                                                                          polarity.canonical_name))

    def get_tuple_information(self, year, polarity):
        if year in self.file_list:
            if polarity in self.file_list[ year ]:
                return self.file_list[ year ][ polarity ]

        logging.critical("Could not find signal files for year {} and polarity {}".format(year, polarity))

class FixedFileLibrary (FileLibrary):
    """
    File library in which all paths are generated from the base tuple path.
    """
    def __init__(self, decay, tuple_name, low_stats=False):
        FileLibrary.__init__(self, decay)
        
        import socket
        hostname = socket.gethostname()

        # FIXME: These tuples should go to a more general EOS workspace
        #         e.g. a working group.
        
        if "stbc" in hostname:
             self.base_tuple_directory = "/user/laurentd/tuples/AdetKpi/"
        else:
            self.base_tuple_directory = "root://eoslhcb.cern.ch//eos/lhcb/user/l/ldufour/Tuples_KPi_Asymmetry/"

        self.tuple_name = tuple_name
        self.low_stats = low_stats

    def get_tuple_information(self, year, polarity):
        file_path_low_stats = self.base_tuple_directory + str(year) + "/" + polarity.canonical_name + "/" + self.decay + "_small.root";

        # I cannot check if the file exists, as it uses the ROOT file protocol...
        if self.low_stats and year == 2016 and self.decay == "kpipi":
            logging.info("Using a smaller file for {}".format(self.decay));
            
            file_path = file_path_low_stats
        else:
            
            file_path = self.base_tuple_directory + str(year) + "/" + polarity.canonical_name + "/" + self.decay + ".root"
        
        return [ TuplePathInformation(file_path, self.tuple_name ) ]
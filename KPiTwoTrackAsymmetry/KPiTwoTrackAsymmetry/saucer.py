__author__ = "Laurent Dufour <laurent.dufour@cern.ch>"
__date__   = "September 2016"
__all__    = ( "Saucer" )

from array import array
import ROOT
import logging
import names
import configuration
import random
import utilities
import numpy as n

# progress bar
from tqdm import tqdm

class Saucer:
    """
    Class responsible for generating a combined tuple 
    with kaons and pions
    """
    def __init__(self, 
                 signal_tree, 
                 path_reweighted_kpipi,
                 signal_decay):
        if "kminus" in signal_decay.daughter_particles:
            self.sauce_to_add = "piplus"
        elif "piplus" in signal_decay.daugther_particles:
            self.sauce_to_add = "kminus"
            
        self.signal_tree = signal_tree
        self.path_reweighted_kpipi = path_reweighted_kpipi
        self.signal_decay = signal_decay
        
        self.logger = logging.getLogger("Saucer")
        self.logger.debug("Saucer initialised.")
        self.signal_vars = []
        
    def add_branches (self, to_add_branches):
        self.signal_vars += to_add_branches
            
    def generate_new_tuple(self, output_file="output.root"):
        self.logger.debug("Saucer preparing to create new sauced tuple.")
        
        self.logger.debug("Opening {}.".format(self.path_reweighted_kpipi))
        kpipi_file = ROOT.TFile.Open( self.path_reweighted_kpipi, "READONLY")
        self.logger.debug("Trying to access {}.".format(names.tree_name_reweighter_output))
        kpipi_tree = kpipi_file.Get( names.tree_name_reweighter_output )
        
        self.logger.debug("Using as weight name {}.".format(names.leaf_name_weight))
        kpipi_weight_name = names.leaf_name_weight
        variables_kpipi=["pt", "eta", "phi"]

        if self.sauce_to_add == "piplus":
            kaon_tree = self.signal_tree;
            kaon_branch_config = self.signal_decay.daughter_particles["kminus"].get_tuple_variable_configuration();    
            self.signal_vars += [ kaon_branch_config.varA, kaon_branch_config.varB, kaon_branch_config.varC ]
            
            pion_tree = kpipi_tree;
            kpipi_vars = ["{}_{}".format(configuration.particles["kpipi"]["piplus"].canonical_name, variable) for variable in variables_kpipi]

        elif self.sauce_to_add == "kminus":
            pion_tree = self.signal_tree;
            pion_branch_config = self.signal_decay.daughter_particles["piplus"].get_tuple_variable_configuration();    
            self.signal_vars += [ pion_branch_config.varA, pion_branch_config.varB, pion_branch_config.varC ]
            
            kaon_tree = kpipi_tree;
            kpipi_vars = ["{}_{}".format(configuration.particles["kpipi"]["kminus"].canonical_name, variable) for variable in variables_kpipi]

        if self.signal_decay.useSWeights():
            self.signal_vars += [ self.signal_decay.getSWeightVar() ]
            
        self.logger.info("Opening {} for sauced tuple output.".format(output_file))
        output_file = ROOT.TFile.Open( output_file, "RECREATE" )
        
        self.signal_tree.SetBranchStatus("*", 0)
        for signal_var in self.signal_vars:
            self.signal_tree.SetBranchStatus( signal_var, 1)
        
        self.logger.info("Cloning original tree.")
        reduced_signal_tree = self.signal_tree.CloneTree()
        self.logger.info("Done cloning original tree.")
        
        self.logger.info("Creating friend tree.")
        friend_tree = ROOT.TTree("friend_tree", "friend_tree")
        
        self.logger.info("Creating a PDF for entry numbers out of the re-weighted kpipi tree.")
        total_entries = kpipi_tree.GetEntries()
        integral_of_weights = utilities.compute_integral_of_expression_in_tree( kpipi_tree, kpipi_weight_name)
        pdf_for_events = utilities.pdf_builder_from_weights(kpipi_tree, kpipi_weight_name)
        self.logger.info("Filling sauced tree.")
        #reduced_signal_tree.SetBranchStatus("*", 0)
        
        # containers of the variables to be written to the sauced tree
        pt = n.zeros(1, dtype=float)
        eta = n.zeros(1, dtype=float)
        phi = n.zeros(1, dtype=float)
        
        # add variables to the tree. 
        # these are called pt, eta, phi, but in reality could really be anything.
        added_pt = reduced_signal_tree.Branch ('sauced_' + kpipi_vars[0], pt,  "sauced_{}/D".format( kpipi_vars[0] ))
        added_eta = reduced_signal_tree.Branch('sauced_' + kpipi_vars[1], eta, "sauced_{}/D".format( kpipi_vars[1] ))
        added_phi = reduced_signal_tree.Branch('sauced_' + kpipi_vars[2], phi, "sauced_{}/D".format( kpipi_vars[2] ))
        
        self.logger.info("Entries to fill: {}".format(str(reduced_signal_tree.GetEntries())));
        
        #pbar = ProgressBar()
        for signal_entry_id in tqdm(range(0, reduced_signal_tree.GetEntries())):
            reduced_signal_tree.GetEntry(signal_entry_id)
            
            random_prob = random.random()
            entryNumber = pdf_for_events.fArray[ pdf_for_events.FindBin( random_prob ) ]
            kpipi_tree.GetEntry(entryNumber)
            
            pt[0]  = getattr(kpipi_tree, kpipi_vars[0])
            eta[0] = getattr(kpipi_tree, kpipi_vars[1])
            phi[0] = getattr(kpipi_tree, kpipi_vars[2])
            
            added_pt.Fill()
            added_eta.Fill()
            added_phi.Fill()
        
        reduced_signal_tree.Write()
        
        output_file.Close()
        
        self.logger.info("Sauced output complete.")
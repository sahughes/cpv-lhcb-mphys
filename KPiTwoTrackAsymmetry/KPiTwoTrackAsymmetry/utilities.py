import collections
import configuration

import argparse
import logging
import plotting

import os
import ROOT
import string, random

import numpy as np
from array import array

from ROOT import gDirectory

def ensure_list( variable ):
    """
    When the given variable is not a list,
    a singleton list will be made with this as single entry.
    """
    if type(variable) is not list:
        return [ variable ]
    else:
        return variable

def update(d, u):
    """
    Updates a nested dictionary, superseeds the
    standard Python .update
    """
    for k, v in u.iteritems():
        if isinstance(v, collections.Mapping):
            r = update(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d

def get_default_argument_parser(include_polarity=True, require_polarity=False):
    """Creates and returns the default argument parser.
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('--year', type=int, required=True,
                        choices=configuration.years, help='Year of dataset')

    if include_polarity:
        parser.add_argument('-p', '--polarity', required=require_polarity,
                        choices=configuration.polarities, help='Polarity')

    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='When true, debug logging is enabled')

    return parser

def enum(**enums):
    """ Helper for enum-support pre-Python 3"""
    return type('Enum', (), enums)

def parse_default_arguments( options, include_polarity=True ):
    """
    @return dict
    """

    year = options.year
    verbose = options.verbose

    if verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if include_polarity:
        magnetPolarity = configuration.polarities[ options.polarity ]

        return {"year": year, "polarity": magnetPolarity}

    return {"year": year}

def ensure_trailing_slash_on_output_directory( directory ):
    return os.path.join(directory, '')

def check_output_directory( directory ):
    """ Will check if the directory exists, if not create it.
    """
    if not os.path.exists(directory):
        os.makedirs(directory)

def id_generator(size=6, chars=string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))


def compute_integral_of_expression_in_tree ( input_tree, tformula_expression, cuts="(1)" ):
    """
    Quickly computes the integral of an expression in a ROOT.TTree by 
    exploiting the mean of the histogram (which is always exact and
    not influenced by the binning), together with the quick access to the
    number of entries. This is tested to be equal to a loop over entries and
    an explicit sum per entry. 
    
    @return double
    """
    
    histogram_name = id_generator(10)
    
    input_tree.Draw(("{}>>" + histogram_name).format(tformula_expression), cuts, "GOFF")
    histogram = gDirectory.Get(histogram_name);
    
    return histogram.GetMean()*histogram.GetEntries()

def normalise_weights( input_tree, weight_variable ):
    normalisation_factor = compute_integral_of_expression_in_tree( input_tree, weight_variable)
    
    normalisation = compute_integral_of_expression_in_tree( input_tree, "("+weight_variable+"*"+weight_variable+")")
    
    if normalisation > 0:
        normalisation_factor /= normalisation
    else:
        print "Got invalid result for weight*weight"
        print normalisation
        exit()
    
    return normalisation_factor

def get_weight_cut_efficiency_curve ( input_tree, weight_variable, multiply_by_weight=False ):
    """ Generates a TGraph of an inverted ROC-curve """
    input_tree.Draw( weight_variable + ">>temp_weight_hist" );
    temp_histogram = ROOT.gDirectory.Get("temp_weight_hist")
    to_use_maximum = temp_histogram.GetXaxis().GetXmax()
    
    n_bins_weight_histogram = 150
    
    x_axis_points = [0]*n_bins_weight_histogram
    y_axis_points = [0]*n_bins_weight_histogram
    
    weight_histogram = ROOT.TH1F("weight_histogram", "weight_histogram", n_bins_weight_histogram, 0., to_use_maximum);
    input_tree.Draw( weight_variable + ">>weight_histogram" );
    accumulated_events = 0
    
    to_use_normalisation = 0 if multiply_by_weight else temp_histogram.GetEntries()
    
    for weight_bin in range (1, n_bins_weight_histogram):
        x_axis_points[ weight_bin-1 ] = weight_histogram.GetXaxis().GetBinUpEdge( weight_bin );
        
        if multiply_by_weight:
            to_use_normalisation += weight_histogram.GetXaxis().GetBinCenter( weight_bin )
            accumulated_events += weight_histogram.GetXaxis().GetBinCenter( weight_bin ) *  weight_histogram.GetBinContent( weight_bin )
        else:
            accumulated_events += weight_histogram.GetBinContent( weight_bin )
        
        y_axis_points [ weight_bin-1 ] = accumulated_events
    
    # normalise
    y_axis_points = [ y/accumulated_events for y in y_axis_points ]
    
    graph = ROOT.TGraph( n_bins_weight_histogram, 
                         np.array( x_axis_points, dtype=float ), 
                         np.array( y_axis_points, dtype=float ));
                         
    return graph;

def get_inverted_efficiency_curve( input_tree, weight_variable, multiply_by_weight=False ):
    curve = get_weight_cut_efficiency_curve( input_tree, weight_variable, multiply_by_weight )
    
    x_points = curve.GetX();
    y_points = curve.GetY();
    
    return ROOT.TGraph(curve.GetN(), y_points, x_points) 

def pdf_builder_from_weights( tree, weight_var):
    """ 
    Generates a histogram with [0, 1] on the X-axis
    and with bin-sizes corresponding to the normalized 
    weights. Here the weights are seen as a probability 
    for each event, and thus the normalization is s.t.
    the sum equals 1.  
    
    To use the returned pdf: 
        random_number = random.random()
        value = pdf.GetBinContent( pdf.FindBin( random_number ) )
    
    In principle, this could be replaced with a RooHistPdf. 
    However, for a large generation, this showed some  
    unexpected behavior (the .get returned null pointers), 
    so this is kept instead. 
    """
    logger = logging.getLogger("pdf_builder_from_weights")
    normalisation = compute_integral_of_expression_in_tree(tree, weight_var)
        
    sum_of_weights = 0.0
    bin_edges = tree.GetEntries()*[0.0]
    
    logger.info( "Generating bin edges." )
    entryId = 0
    for entry in tree:
        sum_of_weights += (getattr(tree, weight_var) / normalisation)
        bin_edges[ entryId ] = sum_of_weights
        entryId+= 1
    
    logger.info( "Generating pdf histogram." )
    pdf = ROOT.TH1I( "pdf", "pdf", len(bin_edges)-1, plotting.to_root_bins(bin_edges) )
    for binId in range (0, tree.GetEntries() ):
        pdf.SetBinContent( binId, binId )
    
    return pdf

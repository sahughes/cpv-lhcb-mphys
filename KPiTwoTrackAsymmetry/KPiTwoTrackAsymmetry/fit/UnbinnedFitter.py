""""
Wrapper class around the DeltaACP-PDF provided by the
original KPi Asymmetry tool. 

Original wrapper by Adam Davis. Ported to Python by 
Laurent.

Obsolete.

@author Adam Davis <adam.davis@cern.ch>,
        Laurent Dufour <laurent.dufour@cern.ch>
"""

from ROOT import gROOT, RooRealVar, RooCategory
from ROOT import RooArgSet, TFile
from ROOT import RooDataSet
from ROOT import RooRealVar, TCanvas
from ROOT import RooPlot
from ROOT import RooFormulaVar

import GaudiPython
import ROOT
import utilities
import logging

class AsymmetryFitter:
    """ Wrapper around the DeltaACP pdf """
    def __init__(self, 
                 input_file_name,
                 input_tree_name,
                 to_fit_variable,
                 id_variable,
                 weight_variable,
                 decay_configuration,
                 output_directory):
        gROOT.ProcessLine(".L src/DeltaACP_pdf.cc+")
        from ROOT import DeltaACP_pdf
        
        self.input_file_name = input_file_name
        self.input_tree_name = input_tree_name
        self.to_fit_variable = to_fit_variable
        self.id_variable = id_variable
        self.weight_variable = weight_variable
        self.output_directory=output_directory
        
        self.logger = logging.getLogger("AsymmetryFitter")
        self.decay_configuration = decay_configuration
        
        self.initialised=False
    
    def read_tree(self):
        """ Reads the TTree from the file, saves it locally
        in the tree field (together with the TFile handle
        in the file field).
        
        Also computes the normalisation factor of the weights
        as sum(weights)/sum(weights^2) and defines an alias
        for the rescaled weights: "rescaled_weight"
        
        This variable however unfortunately is only used 
        for convenience in generating plots. RooFit does not
        support RoooDataSets with values from a tree alias. 
        
        Method should be run before creating the dataset.
        
        @return void
        """
        logging.debug("Accessing {}".format(  self.input_file_name ))
        self.file = TFile.Open( self.input_file_name, "READONLY" )
        self.tree = self.file.Get( self.input_tree_name )
        
        #self.normalisation_factor = utilities.normalise_weights(self.tree, "weight")
        self.normalisation_factor = 1
        logging.debug("Using as normalisation factor for the weights: {}".format(self.normalisation_factor))
        
        self.initialised = True
        
        return self.normalisation_factor
        
    def run_fit(self):
        """
        Reads the tree if needed, creates a dataset from
        the tree and performs a binned likelihood fit using
        the DeltaACP_pdf.
        """
        if not self.initialised:
            self.read_tree()
        
        mass_variable = self.to_fit_variable
        internal_suffix = self.decay_configuration.canonical_name
        
        #data = self.create_dataset()
        #weighted_dataset = data["dataset"]
        #varset = data["varset"]
        
        D_M = RooRealVar ( mass_variable, "D^{+} mass [MeV/#it{c}^{2}]", 1800, 1935 )
        D_ID = RooRealVar ( "D_ID", "D_ID", 0, -411, 411 )
        weight = RooRealVar("weight", "weight", 0.5, -1e5, 1e5)

        varset = RooArgSet();
        varset.add ( D_M );
        varset.add ( D_ID );
        varset.add ( weight );
        
        tag = RooCategory("tag", "tag")
        tag.defineType("minus", +411)
        tag.defineType("plus", -411)
        
        dataset = RooDataSet ( "dataset_" + internal_suffix, "dataset_"+internal_suffix, 
                                        self.tree, varset )
        
        self.logger.info("Splitting datasets in positive and negative charges.");
        weighted_dataset_positive = dataset.reduce("D_ID<0")
        weighted_dataset_negative = dataset.reduce("D_ID>0")
        
        split_dataset = RooDataSet("categorised_dataset_"+internal_suffix,"categorised_dataset_"+internal_suffix,
                                            varset, 
                                            ROOT.RooFit.Index(tag),
                                            ROOT.RooFit.Import("minus", weighted_dataset_negative),
                                            ROOT.RooFit.Import("plus", weighted_dataset_positive)) ;
        
        '''
          RooDataSet::RooDataSet(const char* name, const char* title, RooDataSet* data, 
          const RooArgSet& vars, const char* cuts = 0, const char* wgtVarName = 0) =>
    could not convert argument 5 (expected string or Unicode object, int found)
    '''
        rescaled_weight = RooFormulaVar("rescaled_weight_"+internal_suffix, "rescaled_weight_"+internal_suffix, 
                                        "(weight*" + str(self.normalisation_factor) + ")", 
                                        ROOT.RooArgList(weight));
        rescaled_weight_roorealvar = split_dataset.addColumn(rescaled_weight)
        #rescaled_weight_roorealvar.GetName()
        
        weighted_split_dataset = RooDataSet("categorised_dataset_"+internal_suffix,
                                            "categorised_dataset_"+internal_suffix,
                                            split_dataset, split_dataset.get(),
                                            "",
                                            rescaled_weight_roorealvar.GetName())
        
        binned_weighted_dataset = weighted_split_dataset.binnedClone("binned_dataset_"+internal_suffix,"binned_dataset_"+internal_suffix);
        binned_weighted_dataset.Print("v")
        
        if self.decay_configuration.canonical_name is "kpipi":
            internal_dacp_pdf_id = 3
        else:
            internal_dacp_pdf_id = 4

        decay_name = internal_suffix
        
        std = GaudiPython.gbl.std
        string = std.string
        vector = std.vector(string)
        
        vectorOfOptions = vector()
        #vectorOfOptions.push_back("pol")
        
        pdf_kpipi = ROOT.DeltaACP_pdf("pdf_{}".format( decay_name ),
                                      "kpipi", D_M, tag, internal_dacp_pdf_id,
                                      vectorOfOptions);  
        pdf_kpipi.setStartParameters (split_dataset);
        
        res_kpipi = pdf_kpipi.get_pdf().fitTo( binned_weighted_dataset,
                                               ROOT.RooFit.Save(ROOT.kTRUE),
                                               ROOT.RooFit.SumW2Error(ROOT.kTRUE),
                                               ROOT.RooFit.Extended(ROOT.kTRUE) )
        
        '''
        chi2 = ROOT.RooChi2Var("chi2","chi2",pdf_kpipi.get_pdf(),binned_weighted_dataset,ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2)) ;
        m = ROOT.RooMinuit(chi2)
        m.migrad()
        m.hesse()
        res_kpipi = m.save() ;
        '''
        
        kpipi_arglist_final = res_kpipi.floatParsFinal();
        asig_kpipi = kpipi_arglist_final.find("A_{sig}");
        '''
        
        list = binned_weighted_dataset.split(tag);
        dataSetPos =  list.FindObject ( "plus" );
        dataSetPos.Print("v");
        dataSetNeg = list.FindObject ( "minus" );
        dataSetNeg.Print("v");
        '''
        canvas = TCanvas()
        canvas.SetLogy();
        frame = D_M.frame()
        #frame.SetLogy();
        #list = binned_weighted_dataset.split ( tag );
        #dataSetA = list.FindObject ( "plus" );
        binned_weighted_dataset.plotOn(frame, ROOT.RooFit.Name("Data"),
                                       ROOT.RooFit.Cut("tag==tag::minus"),
                                       ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2));
        pdf_kpipi.get_pdf().plotOn(frame,
                                   ROOT.RooFit.Components("modelM"),
                                   ROOT.RooFit.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   ROOT.RooFit.Name("totalPDF"));
        
        pdf_kpipi.get_pdf().getComponents().Print("v")
        pdf_kpipi.get_pdf().plotOn(frame,
                                   ROOT.RooFit.Components("polyM"),
                                   ROOT.RooFit.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   ROOT.RooFit.Name("polyM"));
        
        frame.Draw();
        total_weight = binned_weighted_dataset.sumEntries()
        print "Effective statistics of fit: {}".format( total_weight );
        print "chiSquare of fit: " + str( frame.chiSquare("totalPDF", "Data", res_kpipi.floatParsFinal().getSize()) )

        latex = ROOT.TLatex()
        latex.SetTextColor(2);
        latex.SetNDC();
        latex.SetTextSize(0.04);
        latex.SetTextFont(132);
        latex.SetTextAlign(13);  # align at top
        #,TMath::Prob(chi2Test.getVal(),ndf))
        #latex.DrawLatex( 0.67, 0.73, 
        #                 "#font[12]{{P(#chi^{{2}}/#font[132]{{ndf}})}} = {}".format( ROOT.TMath.Prob(chi2Test.getVal(),ndf)) )

        canvas.SaveAs("{}fit_{}.pdf".format( self.output_directory, decay_name) );
        canvas.SaveAs("{}fit_{}.C".format( self.output_directory, decay_name ) );
        
        self.asymmetry_value = asig_kpipi.getVal()
        self.asymmetry_error = asig_kpipi.getError()
        
        return [self.asymmetry_value, self.asymmetry_error]
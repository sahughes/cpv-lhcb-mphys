"""
Uses the external "radiative tail" fit from Fabio to fit the KPiPi
""" 
import logging
import ROOT
from ROOT import RooFit as RF
from ROOT import gROOT, RooRealVar, RooCategory
from ROOT import RooArgSet, TFile
from ROOT import RooDataSet
from ROOT import RooRealVar, TCanvas
from ROOT import RooPlot
from ROOT import RooFormulaVar
from ROOT import RooGaussian, RooChebychev, RooExtendPdf, RooExponential, RooAddPdf
from ROOT import RooSimultaneous, RooCBShape, RooArgList


class RadiativeBinnedAsymmetryFitter:
    """
    Wrapper around the C++ RooRadiativeTail PDF.
    
    The flow should be the following:
     1) Create a RadiativeBinnedAsymmetryFitter object
     2) Call the run_fit() method.
     
    The idea is to use this in the HistogramBasedFitter.  
    """
    
    def __init__(self, 
                 unique_name,
                 to_fit_variable,
                 fit_range,
                 is_weighted):
        
        self.to_fit_variable = to_fit_variable # the x-axis on the histograms
        self.logger = logging.getLogger("RadiativeBinnedAsymmetryFitter")
        self.is_weighted=is_weighted
        self.fit_range = fit_range
        self.unique_name = unique_name
        
        self.chi2 = 0.0
        self.category = None # created at initialization.
        
        self.positive_pdfs = []
        self.negative_pdfs = []
        
        ROOT.gSystem.AddIncludePath(" -I/cvmfs/lhcb.cern.ch/lib/lcg/external/GSL/1.10/x86_64-slc5-gcc43-opt/include ");
        ROOT.gSystem.AddLinkedLibs(" -L/cvmfs/lhcb.cern.ch/lib/lcg/external/GSL/1.10/x86_64-slc5-gcc43-opt/lib -lgsl -lgslcblas");
        
        #ROOT.gROOT.ProcessLine(".L RooRadiativeTail3.cxx+");
        #ROOT.gROOT.ProcessLine(".L RooRadiativeTail.cxx+");
        #ROOT.gSystem.Load("RooRadiativeTail_cxx.so");
        #ROOT.gSystem.Load("RooRadiativeTail3_cxx.so");
        
        conf= ROOT.RooNumIntConfig.defaultConfig();
        conf.method1D().setLabel("RooAdaptiveGaussKronrodIntegrator1D");
        ROOT.RooAbsReal.defaultIntegratorConfig().setEpsAbs(1e-8) ;
        ROOT.RooAbsReal.defaultIntegratorConfig().setEpsRel(1e-8) ;
        ROOT.RooAbsReal.defaultIntegratorConfig().Print("v");
        
    def set_input_histograms(self, histogram_positive, histogram_negative):
        self.histogram_positive = histogram_positive
        self.histogram_negative = histogram_negative
        
    def initialize ( self ):
        """
        This method creates all the required RooRealVars for the fit.
        
        None of the parameters are fixed or related to each other, 
        as this is done in a later stage. PDFs are created in a later stage.
        """
        nEntries = 2.0*self.histogram_positive.Integral()
        
        self.category = RooCategory("tag", "tag")
        self.category.defineType("minus", +411)
        self.category.defineType("plus", -411)
        
        self.dataset_positive = ROOT.RooDataHist("dataset_positive", 
                                             "dataset_positive", 
                                        ROOT.RooArgList( self.to_fit_variable ), self.histogram_positive);
        self.dataset_negative = ROOT.RooDataHist("dataset_negative", 
                                             "dataset_negative", 
                                        ROOT.RooArgList( self.to_fit_variable ), self.histogram_negative);
        
        self.dataset = ROOT.RooDataHist("categorised_dataset_",
                                            "categorised_dataset",
                                            ROOT.RooArgList( self.to_fit_variable ),
                                            RF.Index( self.category ),
                                            RF.Import("plus", self.dataset_positive),
                                            RF.Import("minus", self.dataset_negative))
        
        # All total yields.
        self.total_background_yield = RooRealVar ( "total_background_yied", 
                                                   "total_background_yied", 
                                                   nEntries * 0.10, 
                                                   0.0, 
                                                   2.0*nEntries );
        self.total_signal_yield = RooRealVar ( "total_signal_yield", "total_signal_yield", 
                                               nEntries * 0.70, 
                                               0.3*nEntries, 
                                               2.0*nEntries );
        
        # All asymmetry variables. They are in PERCENT.
        self.background_asymmetry = RooRealVar("background_asymmetry", "background_asymmetry",
                                               0.00, -7.5, 7.5);
        self.signal_asymmetry = RooRealVar("A_{sig}", "A_{sig}",
                                               0.00, -7.5, 7.5);
        
        # All charge-dependent yields
        self.background_yield_positive = RooFormulaVar ( "background_yield_positive", "background_yield_positive", 
                                                         "(1+@0)*@1", 
                                                         RooArgList ( self.background_asymmetry, 
                                                                      self.total_background_yield ) );

        self.background_yield_negative = RooFormulaVar ( "background_yield_negative", "background_yield_negative", 
                                                         "(1-@0)*@1", 
                                                         RooArgList ( self.background_asymmetry, 
                                                                      self.total_background_yield ) );

        self.signal_yield_positive = RooFormulaVar ( "signal_yield_positive", "signal_yield_positive", 
                                                         "(1+@0/100.0)*@1", 
                                                         RooArgList ( self.signal_asymmetry, 
                                                                      self.total_signal_yield ) );

        self.signal_yield_negative = RooFormulaVar ( "signal_yield_negative", "signal_yield_negative", 
                                                         "(1-@0/100.0)*@1", 
                                                         RooArgList ( self.signal_asymmetry, 
                                                                      self.total_signal_yield ) );
        
        self.mean_plus_A = ROOT.RooRealVar("mean_plus_A", "mean_plus_A", 1869.9, 1850, 1880);
        self.mean_plus_B = ROOT.RooRealVar("mean_plus_B", "mean_plus_B", 1869.9, 1850, 1880);
        self.mean_minus_A = ROOT.RooRealVar("meanmean_minus_A", "mean_minus_A", 1869.9, 1850, 1880);
        self.mean_minus_B = ROOT.RooRealVar("mean_minus_B", "mean_minus_B", 1869.9, 1850, 1880);
        
        self.deltamean_plus = ROOT.RooRealVar("deltamean_plus", "deltamean_plus", 0.0, -1., 1.);
        self.deltamean_minus = ROOT.RooRealVar("deltamean_minus", "deltamean_minus", 0.0, -1., 1.);
        
        self.deltamean_b_plus = ROOT.RooRealVar("deltamean_b_plus", "deltamean_b_plus", 0.0, -2.5, 2.5);
        self.deltamean_b_minus = ROOT.RooRealVar("deltamean_b_minus", "deltamean_b_minus", 0.0, -2.5, 2.5);
        
        self.sigma1 = ROOT.RooRealVar("sigma1", "sigma1", 10, 0, 30);
        self.sigma2 = ROOT.RooRealVar("sigma2", "sigma2", 20, 0, 50);
        self.sigma3 = ROOT.RooRealVar("sigma3", "sigma3", 6, 0, 20);
        
        self.sigma1_pos = ROOT.RooRealVar("sigma1_pos", "sigma1_pos", 10, 0, 30);
        self.sigma2_pos = ROOT.RooRealVar("sigma2_pos", "sigma2_pos", 20, 0, 50);
        self.sigma3_pos = ROOT.RooRealVar("sigma3_pos", "sigma3_pos", 6, 0, 20);
        
        self.frac1 = ROOT.RooRealVar("frac1", "frac1", 0.4, 0, 1);
        self.frac2 = ROOT.RooRealVar("frac2", "frac2", 0.1, 0, 1);
        
        self.alpha = ROOT.RooRealVar("alpha", "alpha", -0.995, -0.9999, -0.90);
        self.alpha_pos = ROOT.RooRealVar("alpha_pos", "alpha_pos", -0.995, -0.9999, -0.90);
        
        self.slope = ROOT.RooRealVar("slope", "slope", 0, -5, 5);
        self.slope_pos = ROOT.RooRealVar("slope_pos", "slope_pos", 0, -5, 5);

        self.reverse_dm = ROOT.RooInt(0);
        
        ROOT.gSystem.AddIncludePath(" -I/cvmfs/lhcb.cern.ch/lib/lcg/external/GSL/1.10/x86_64-slc5-gcc43-opt/include ");
        ROOT.gSystem.AddLinkedLibs(" -L/cvmfs/lhcb.cern.ch/lib/lcg/external/GSL/1.10/x86_64-slc5-gcc43-opt/lib -lgsl -lgslcblas");
        
        from ROOT import RooRadiativeTail,RooRadiativeTail3

        self.g2_Dplus = ROOT.RooRadiativeTail("g2_Dplus","g2_Dplus",self.to_fit_variable, self.alpha, self.mean_plus_A, self.sigma1, self.sigma2, self.frac1);
        self.g2_Dminus = ROOT.RooRadiativeTail("g2_Dminus","g2_Dminus",self.to_fit_variable, self.alpha, self.mean_minus_A, self.sigma1, self.sigma2, self.frac1);
        self.g3_Dplus = ROOT.RooRadiativeTail3("g3_Dplus","g3_Dplus",self.to_fit_variable, self.alpha, self.mean_plus_B, self.deltamean_plus, self.deltamean_b_plus, self.sigma1, self.sigma2, self.sigma3_pos, self.frac1, self.frac2, self.reverse_dm);
        self.g3_Dminus = ROOT.RooRadiativeTail3("g3_Dminus","g3_Dminus",self.to_fit_variable, self.alpha, self.mean_minus_B, self.deltamean_minus, self.deltamean_b_minus, self.sigma1, self.sigma2, self.sigma3, self.frac1, self.frac2, self.reverse_dm);

        self.expo_Dplus = ROOT.RooExponential("expo_Dplus", "expo_Dplus", self.to_fit_variable, self.slope);
        self.expo_Dminus = ROOT.RooExponential("expo_Dminus", "expo_Dminus", self.to_fit_variable, self.slope);

        self.mean_extra_gauss = ROOT.RooRealVar("mean_extra_gauss",
                                                "mean_extra_gauss",
                                                1869.9, 1860, 1873)
        self.sigma_extra_gauss = ROOT.RooRealVar("sigma_extra_gauss", 
                                                 "sigma_extra_gauss", 10, 0, 30);
        self.extra_gaussian = ROOT.RooGaussian("extra_gauss",
                                               "extra_gauss", 
                                               self.to_fit_variable,
                                               self.mean_extra_gauss,
                                               self.sigma_extra_gauss)

        self.pdfsig_Dplus= self.g3_Dplus;
        self.pdfsig_Dminus  = self.g3_Dminus;


        
        self.pdfbkg_Dplus = self.expo_Dplus;
        self.pdfbkg_Dminus =  self.expo_Dminus;
    
        #else
        #  {
        #pdfsig_Dplus= new RooProdPdf("pdfsig_Dplus", "pdfsig_Dplus", RooArgSet(*tagsigDplus,*g3_Dplus));
        #pdfsig_Dminus = new RooProdPdf("pdfsig_Dminus", "pdfsig_Dminus", RooArgSet(*tagsigDminus,*g3_Dminus));
        #  }
          
    def create_extended_signal_pdf( self ):
        """
        Assuming the "simple" PDFs already exists and the RooRealVars
        for the yields have been initialized, the extended pdfs are created.
        """
        self.signal_positive = RooExtendPdf ( "signalP", "signalP", 
                                              self.pdfsig_Dplus, 
                                              self.signal_yield_positive );

        self.signal_negative = RooExtendPdf ( "signalM", "signalM", 
                                              self.pdfsig_Dminus, 
                                              self.signal_yield_negative );
                                              
                                              
                                              
        self.positive_pdfs += [self.signal_positive]
        self.negative_pdfs += [self.signal_negative]
        
        self.background_positive = RooExtendPdf ( "bkgP", "bkgP", 
                                              self.pdfbkg_Dplus, 
                                              self.background_yield_positive );

        self.background_negative = RooExtendPdf ( "bkgM", "bkgM", 
                                              self.pdfbkg_Dminus, 
                                              self.background_yield_negative );
    
    def run_fit(self, use_migrad=False):
        
        self.model_positive = RooAddPdf ( "modelP", "modelP",
                                     RooArgList( self.signal_positive, self.background_positive) );
        self.model_negative = RooAddPdf ( "modelM", "modelM", 
                                     RooArgList( self.signal_negative, self.background_negative) );
        
        self.model = RooSimultaneous ( "total_model", "total_model", self.category );
        self.model.addPdf ( self.model_positive, "plus" );
        self.model.addPdf ( self.model_negative, "minus" );

            
        params = self.model.getParameters(RooArgSet( self.to_fit_variable) );
        #params.readFromFile("params/params_"+self.unique_name+"_hist.txt");
        """
        
        self.fit_result = self.model.fitTo (self.dataset, 
                                 RF.Range (self.fit_range[0], self.fit_range[1]), 
                                 RF.Save (), 
                                 RF.Extended (True),
                                 RF.SumW2Error(self.is_weighted));
                                 
        if use_migrad:
            self.fit_result = self.model.fitTo (self.dataset, 
                                 RF.Range (self.fit_range[0], self.fit_range[1]), 
                                 RF.Save (), 
                                 RF.Extended (True),
                                 RF.Minos(True),
                                 RF.Strategy(2),
                                 RF.Minimizer ( "Minuit2" ),
                                 RF.SumW2Error(self.is_weighted));
        """
        nll = ROOT.RooChi2Var("nll", "nll", 
                                self.model, self.dataset, 
                                  RF.Verbose(True), 
                                  RF.Extended(True),
                                  RF.PrintLevel(8),
                                  RF.DataError(ROOT.RooAbsData.SumW2));
        
        
        m = ROOT.RooMinuit(nll);
        m.setStrategy(1);
        m.setVerbose(True);
        m.setPrintLevel(3);
        m.migrad();   
        m.hesse();
    
        
        paramSet =  self.model.getParameters( RooArgSet( self.to_fit_variable) ) ;
        paramList = ROOT.RooArgList(paramSet) ;

        _floatParamList = paramList.selectByAttrib("Constant",False) ;
        npar= _floatParamList.getSize();
                                 
        params.writeToFile("params/updated_params_"+self.unique_name+"_hist.txt");
        kpipi_arglist_final = _floatParamList;
        self.floatParsFinal = _floatParamList
        #asig_kpipi = kpipi_arglist_final.find("A_{sig}");
        
        self.fitted_asymmetry = [ self.signal_asymmetry.getVal(), self.signal_asymmetry.getError() ]
        
    def calculate_chi2( self ):
        """
        Creates a frame and plots the data on there to calculate the 
        frame chi2.
        """
        canvas = TCanvas("", "", 850,1000)
        frame = self.to_fit_variable.frame()
        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::minus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
                                       
        self.model.plotOn(frame,
                                   RF.Components("modelM"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(self.category), self.dataset),
                                   RF.Name("totalPDF"));

        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::minus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
 
        self.chi2_negative = frame.chiSquare("totalPDF", "Data", self.floatParsFinal.getSize())
        
        frame = self.to_fit_variable.frame()
        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::plus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
                                       
        self.model.plotOn(frame,
                                   RF.Components("modelP"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(self.category), self.dataset),
                                   RF.Name("totalPDF"));

        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::plus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
 
        self.chi2_positive = frame.chiSquare("totalPDF", "Data", self.floatParsFinal.getSize())

    
    def get_pdf(self):
        """
        This function is available if a fit has ran.
        """
        return self.model

    def get_tag( self ):
        """
        This function is available when initialized.
        """
        return self.category
    
    def getTag( self ):
        return self.get_tag()
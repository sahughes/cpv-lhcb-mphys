from ROOT import gROOT, RooRealVar, RooCategory
from ROOT import RooArgSet, TFile
from ROOT import RooDataSet
from ROOT import RooRealVar, TCanvas
from ROOT import RooPlot
from ROOT import RooFormulaVar
from ROOT import RooGaussian, RooChebychev, RooExtendPdf, RooExponential, RooAddPdf
from ROOT import RooSimultaneous, RooCBShape, RooArgList

from ROOT import RooFit as RF
import ROOT
import logging

class DplusToKpipiAsymmetryFitter:
    """
    Wrapper around a CB function together with a (set of) Gaussian(s).
    
    The means of the Gaussian and CB need not be the same, nor are any
    parameters a priori shared among the charges. Variables can be set
    to shared by explicitly setting the fields to be the same. Keep in mind
    that this is irreverisble. 
    
    The flow should be the following:
     1) Create a DplusToKpipiAsymmetryFitter object
     2) Create all RooRealVars required by calling the initialize() method
     3) Whichever parameter you would like to share, explicitly set so
        by making the fields identical.
     4) Select the PDF you want to use: set_exponential_background() and
        set_chebyshev_background() are available for the backgrounds.
        Similarly, for the signal one can choose between a CB with 1/2 Gauss
        or just 3 Gauss functions.
     5) Call the run_fit() method.
     
    The idea is to use this in the HistogramBasedFitter.  
    """
    def __init__(self, 
                 unique_name,
                 to_fit_variable,
                 fit_range,
                 is_weighted):
        self.to_fit_variable = to_fit_variable # the x-axis on the histograms
        self.logger = logging.getLogger("DplusToKpipiAsymmetryFitter")
        self.is_weighted=is_weighted
        self.fit_range = fit_range
        self.unique_name = unique_name
        
        self.chi2 = 0.0
        self.initialised=False
        self.category = None # created at initialization.
        
        self.positive_pdfs = []
        self.negative_pdfs = []
    
    
    def set_input_histograms(self, histogram_positive, histogram_negative):
        self.histogram_positive = histogram_positive
        self.histogram_negative = histogram_negative
        
    def set_exponential_background( self ):
        self.background_pdf_simple_positive = RooExponential ("background_pdf_simple_positive",
                                                               "background_pdf_simple_positive",
                                                               self.to_fit_variable,
                                                               self.exponential_background_parameter_positive);
                                                               
        self.background_pdf_simple_negative = RooExponential ("background_pdf_simple_negative",
                                                               "background_pdf_simple_negative",
                                                               self.to_fit_variable,
                                                               self.exponential_background_parameter_negative);
        self.create_extended_background_pdf()
        
    def set_chebyshev_background( self, order ):
        parameter_set_positive = RooArgList()
        parameter_set_negative = RooArgList()
        
        for parameter_id in range (0, order):
            parameter_set_positive.add( self.background_parameters_positive[ parameter_id ] )
            parameter_set_negative.add( self.background_parameters_negative[ parameter_id ] )
        
        self.background_pdf_simple_positive = RooChebychev ("background_pdf_simple_positive",
                                                               "background_pdf_simple_positive",
                                                               self.to_fit_variable,
                                                               parameter_set_positive);
                                                               
        self.background_pdf_simple_negative = RooChebychev ("background_pdf_simple_negative",
                                                               "background_pdf_simple_negative",
                                                               self.to_fit_variable,
                                                               parameter_set_negative);
        self.create_extended_background_pdf()
        
    def create_extended_background_pdf( self ):
        """
        Assuming the "simple" PDFs already exists and the RooRealVars
        for the yields have been initialized, the extended pdfs are created.
        """
        self.background_positive = RooExtendPdf ( "bkgP", "bkgP", self.background_pdf_simple_positive, self.background_yield_positive );
        self.background_negative = RooExtendPdf ( "bkgM", "bkgM", self.background_pdf_simple_negative, self.background_yield_negative );
        
        self.positive_pdfs += [self.background_positive]
        self.negative_pdfs += [self.background_negative]
        
    def set_double_gauss_signal_pdf( self ):
        """
        Uses only a Crystal Ball as signal PDF
        """
        self.dplus_firstgauss_positive = RooGaussian ("dplus_cb_positive",
                                             "dplus_cb_positive",
                                             self.to_fit_variable, 
                                             self.dPlus_mean_positive, 
                                             self.dPlus_sigma_positive);

        self.dplus_firstgauss_negative = RooGaussian ("dplus_cb_negative",
                                             "dplus_cb_negative",
                                             self.to_fit_variable, 
                                             self.dPlus_mean_negative, 
                                             self.dPlus_sigma_negative);
                                             
        self.signal_gauss_positive = RooGaussian("signal_gauss_positive", 
                                                 "signal_gauss_positive", 
                                                 self.to_fit_variable, 
                                                 self.dPlus_mean_positive_second, 
                                                 self.dPlus_sigma_positive_second);
        self.signal_gauss_negative = RooGaussian("signal_gauss_negative", 
                                                 "signal_gauss_negative", 
                                                 self.to_fit_variable,
                                                 self.dPlus_mean_negative_second, 
                                                 self.dPlus_sigma_negative_second);
                                                 
        self.signal_pdf_simple_positive = RooAddPdf ( "dplus_positive_simple", 
                                                      "dplus_positive_simple", 
                                                      RooArgList ( self.dplus_firstgauss_positive, 
                                                                   self.signal_gauss_positive ),
                                                      RooArgList ( self.frac_CB_GaussA_positive ) );
        
        self.signal_pdf_simple_negative = RooAddPdf ( "dplus_negative_simple", 
                                                      "dplus_negative_simple", 
                                                      RooArgList ( self.dplus_firstgauss_negative, 
                                                                   self.signal_gauss_negative ),
                                                      RooArgList ( self.frac_CB_GaussA_negative ) );
    
    def set_cb_single_gauss_signal_pdf( self ):
        """
        Uses only a Crystal Ball as signal PDF
        """
        self.dplus_cb_positive = RooCBShape ("dplus_cb_positive",
                                             "dplus_cb_positive",
                                             self.to_fit_variable, 
                                             self.dPlus_mean_positive, 
                                             self.dPlus_sigma_positive, 
                                             self.dPlus_alpha_positive, 
                                             self.dPlus_n_positive);

        self.dplus_cb_negative = RooCBShape ("dplus_cb_negative",
                                             "dplus_cb_negative",
                                             self.to_fit_variable, 
                                             self.dPlus_mean_negative, 
                                             self.dPlus_sigma_negative, 
                                             self.dPlus_alpha_negative, 
                                             self.dPlus_n_negative);
                                             
        self.signal_gauss_positive = RooGaussian("signal_gauss_positive", 
                                                 "signal_gauss_positive", 
                                                 self.to_fit_variable, 
                                                 self.dPlus_mean_positive_second, 
                                                 self.dPlus_sigma_positive_second);
        self.signal_gauss_negative = RooGaussian("signal_gauss_negative", 
                                                 "signal_gauss_negative", 
                                                 self.to_fit_variable,
                                                 self.dPlus_mean_negative_second, 
                                                 self.dPlus_sigma_negative_second);
                                                 
        self.signal_pdf_simple_positive = RooAddPdf ( "dplus_positive_simple", 
                                                      "dplus_positive_simple", 
                                                      RooArgList ( self.dplus_cb_positive, 
                                                                   self.signal_gauss_positive ),
                                                      RooArgList ( self.frac_CB_GaussA_positive ) );
        
        self.signal_pdf_simple_negative = RooAddPdf ( "dplus_negative_simple", 
                                                      "dplus_negative_simple", 
                                                      RooArgList ( self.dplus_cb_negative, 
                                                                   self.signal_gauss_negative ),
                                                      RooArgList ( self.frac_CB_GaussA_negative ) );
        
        self.create_extended_signal_pdf()


    def set_cb_double_gauss_signal_pdf( self ):
        """
        Uses a CB + Gauss + Gauss
        """
        self.dplus_cb_positive = RooCBShape ("dplus_cb_positive",
                                             "dplus_cb_positive",
                                             self.to_fit_variable, 
                                             self.dPlus_mean_positive, 
                                             self.dPlus_sigma_positive, 
                                             self.dPlus_alpha_positive, 
                                             self.dPlus_n_positive);

        self.dplus_cb_negative = RooCBShape ("dplus_cb_negative",
                                             "dplus_cb_negative",
                                             self.to_fit_variable, 
                                             self.dPlus_mean_negative, 
                                             self.dPlus_sigma_negative, 
                                             self.dPlus_alpha_negative, 
                                             self.dPlus_n_negative);
                                             
        self.signal_gauss_positive = RooGaussian("signal_gauss_positive", 
                                                 "signal_gauss_positive", 
                                                 self.to_fit_variable, 
                                                 self.dPlus_mean_positive_second, 
                                                 self.dPlus_sigma_positive_second);
                                                 
        self.signal_gauss_negative = RooGaussian("signal_gauss_negative", 
                                                 "signal_gauss_negative", 
                                                 self.to_fit_variable,
                                                 self.dPlus_mean_negative_second, 
                                                 self.dPlus_sigma_negative_second);
                                                 
        self.signal_gauss_extra_positive = RooGaussian("signal_gauss_extra_positive", 
                                                 "signal_gauss_extra_positive", 
                                                 self.to_fit_variable, 
                                                 self.dPlus_mean_positive_third, 
                                                 self.dPlus_sigma_positive_third);
                                                 
        self.signal_gauss_extra_negative = RooGaussian("signal_gauss_extra_negative", 
                                                 "signal_gauss_extra_negative", 
                                                 self.to_fit_variable,
                                                 self.dPlus_mean_negative_third, 
                                                 self.dPlus_sigma_negative_third);
                                                 
        self.signal_pdf_simple_positive = RooAddPdf ( "dplus_positive_simple", 
                                                      "dplus_positive_simple", 
                                                      RooArgList ( self.dplus_cb_positive, 
                                                                   self.signal_gauss_positive,
                                                                   self.signal_gauss_extra_positive ),
                                                      RooArgList ( self.frac_CB_GaussA_positive,
                                                                   self.frac_rest_GaussB_positive ), True);
        
        self.signal_pdf_simple_negative = RooAddPdf ( "dplus_negative_simple", 
                                                      "dplus_negative_simple", 
                                                      RooArgList ( self.dplus_cb_negative, 
                                                                   self.signal_gauss_negative,
                                                                   self.signal_gauss_extra_negative ),
                                                      RooArgList ( self.frac_CB_GaussA_negative,
                                                                   self.frac_rest_GaussB_negative ), True );
        
        self.create_extended_signal_pdf()
        
    def create_extended_signal_pdf( self ):
        """
        Assuming the "simple" PDFs already exists and the RooRealVars
        for the yields have been initialized, the extended pdfs are created.
        """
        self.signal_positive = RooExtendPdf ( "signalP", "signalP", 
                                              self.signal_pdf_simple_positive, 
                                              self.signal_yield_positive );
        self.signal_negative = RooExtendPdf ( "signalM", "signalM", 
                                              self.signal_pdf_simple_negative, 
                                              self.signal_yield_negative );
        self.positive_pdfs += [self.signal_positive]
        self.negative_pdfs += [self.signal_negative]
        
    def initialize ( self ):
        """
        This method creates all the required RooRealVars for the fit.
        
        None of the parameters are fixed or related to each other, 
        as this is done in a later stage. PDFs are created in a later stage.
        """
        nEntries = 2.0*self.histogram_positive.Integral()
        
        
        self.category = RooCategory("tag", "tag")
        self.category.defineType("minus", +411)
        self.category.defineType("plus", -411)
        
        self.dataset_positive = ROOT.RooDataHist("dataset_positive", 
                                             "dataset_positive", 
                                        ROOT.RooArgList( self.to_fit_variable ), self.histogram_positive);
        self.dataset_negative = ROOT.RooDataHist("dataset_negative", 
                                             "dataset_negative", 
                                        ROOT.RooArgList( self.to_fit_variable ), self.histogram_negative);
        
        self.dataset = ROOT.RooDataHist("categorised_dataset_",
                                            "categorised_dataset",
                                            ROOT.RooArgList( self.to_fit_variable ),
                                            RF.Index( self.category ),
                                            RF.Import("plus", self.dataset_positive),
                                            RF.Import("minus", self.dataset_negative))
        
        # All total yields.
        self.total_background_yield = RooRealVar ( "total_background_yied", 
                                                   "total_background_yied", 
                                                   nEntries * 0.10, 
                                                   0.0, 
                                                   2.0*nEntries );
        self.total_signal_yield = RooRealVar ( "total_signal_yield", "total_signal_yield", 
                                               nEntries * 0.70, 
                                               0.3*nEntries, 
                                               2.0*nEntries );
        
        # All asymmetry variables. They are in PERCENT.
        self.background_asymmetry = RooRealVar("background_asymmetry", "background_asymmetry",
                                               0.00, -7.5, 7.5);
        self.signal_asymmetry = RooRealVar("A_{sig}", "A_{sig}",
                                               0.00, -7.5, 7.5);
        
        # All charge-dependent yields
        self.background_yield_positive = RooFormulaVar ( "background_yield_positive", "background_yield_positive", 
                                                         "(1+@0)*@1", 
                                                         RooArgList ( self.background_asymmetry, 
                                                                      self.total_background_yield ) );

        self.background_yield_negative = RooFormulaVar ( "background_yield_negative", "background_yield_negative", 
                                                         "(1-@0)*@1", 
                                                         RooArgList ( self.background_asymmetry, 
                                                                      self.total_background_yield ) );

        self.signal_yield_positive = RooFormulaVar ( "signal_yield_positive", "signal_yield_positive", 
                                                         "(1+@0/100.0)*@1", 
                                                         RooArgList ( self.signal_asymmetry, 
                                                                      self.total_signal_yield ) );

        self.signal_yield_negative = RooFormulaVar ( "signal_yield_negative", "signal_yield_negative", 
                                                         "(1-@0/100.0)*@1", 
                                                         RooArgList ( self.signal_asymmetry, 
                                                                      self.total_signal_yield ) );

        # RooRealVars concerning the CB function.
        self.dPlus_mean_positive = RooRealVar  ( "dPlus_KKpi_mean_positive", "dPlus_KKpi_mean_positive", 1869.5, 1865., 1874 );
        self.dPlus_mean_negative = RooRealVar ( "dPlus_KKpi_mean_negative", "dPlus_KKpi_mean_negative", 1869.5, 1865., 1874 );

        self.dPlus_alpha_positive = RooRealVar ( "dPlus_alpha_positive", "dPlus_alpha_positive", 2.308, 2.2, 4.5 );
        self.dPlus_n_positive = RooRealVar ( "dPlus_n_positive", "dPlus_n_positive", 0.9, 0.01, 1.5 );

        self.dPlus_alpha_negative = RooRealVar ( "dPlus_alpha_negative", "dPlus_alpha_negative", 2.308, 2.2, 4.5 );
        self.dPlus_n_negative = RooRealVar ( "dPlus_n_negative", "dPlus_n_negative", 0.9, 0.01, 1.5 );
    
        self.dPlus_sigma_positive = RooRealVar ("dPlus_sigma_positive", "dPlus_sigma_positive", 6.186, 2.0, 10.5,"\\mevcc")
        self.dPlus_sigma_asymmetry = RooRealVar ("scale_of_cb_sigma", "scale of other charge", 1.0, 0.5, 1.50)
        self.dPlus_sigma_negative = RooFormulaVar("dPlus_sigma_negative","@0*@1", ROOT.RooArgList(self.dPlus_sigma_positive,self.dPlus_sigma_asymmetry) );

        # RooRealVars concerning the mean of a Gauss
        self.dPlus_delta_mean_positive = RooRealVar  ( "dPlus_delta_mean_positive", "dPlus_delta_mean_positive", 0.0, -2.5, 2.5 );
        self.dPlus_delta_mean_negative = RooRealVar ( "dPlus_delta_mean_negative", "dPlus_delta_mean_negative", 0.0, -2.5, 2.5 );

        self.dPlus_mean_positive_second = RooFormulaVar("dPlus_mean_positive_second","@0+@1", ROOT.RooArgList(self.dPlus_mean_positive,self.dPlus_delta_mean_positive) );
        self.dPlus_mean_negative_second = RooFormulaVar("dPlus_mean_negative_second","@0+@1", ROOT.RooArgList(self.dPlus_mean_negative,self.dPlus_delta_mean_negative) );

        self.dPlus_sigma_positive_second = RooRealVar ("\\sigma_{2,sig}", "sigma2", 1.36711e+01, 4.0, 20.0,"\\mevcc")
        self.dPlus_sigma_negative_second = RooRealVar ("\\sigma_{2,sig,P}", "sigma2P", 1.36711e+01, 4.0, 20.0,"\\mevcc")

        self.frac_CB_GaussA_positive = RooRealVar ("frac_CB_GaussA_positive", "frac_CB_GaussA_positive", 0.720, 0.1, 1.0);
        self.frac_CB_GaussA_negative = RooRealVar ("frac_CB_GaussA_negative", "frac_CB_GaussA_negative", 0.720, 0.1, 1.0);

        # RooRealVars concerning the mean of another Gauss
        self.dPlus_mean_positive_third = RooRealVar  ( "dPlus_mean_positive_third", "dPlus_mean_positive_third", 1869.5, 1865., 1874 );
        self.dPlus_mean_negative_third = RooRealVar ( "dPlus_mean_negative_third", "dPlus_mean_negative_third", 1869.5, 1865., 1874 );

        self.dPlus_sigma_positive_third = RooRealVar ("\\sigma_{2,sig}", "sigma2", 1.36711e+01, 7.0, 17.5,"\\mevcc")
        self.dPlus_sigma_negative_third = RooRealVar ("\\sigma_{2,sig,P}", "sigma2P", 1.36711e+01, 7.0, 17.5,"\\mevcc")

        self.frac_rest_GaussB_positive = RooRealVar ("frac_rest_GaussB_positive", "frac_rest_GaussB_positive", 0.2, 0.0, 0.3);
        self.frac_rest_GaussB_negative = RooRealVar ("frac_rest_GaussB_negative", "frac_rest_GaussB_negative", 0.2, 0.0, 0.3);

        # RooRealVars in case of an exponential background
        self.exponential_background_parameter_positive = RooRealVar ( "exponential_background_parameter_positive", "exponential_background_parameter_positive", -0.0019, -0.03, -1.20477e-4 );
        self.exponential_background_parameter_negative = RooRealVar ( "exponential_background_parameter_negative", "exponential_background_parameter_negative", -0.0019, -0.03, -1.20477e-4 );
        
        # RooRealVars in case of a Chebychev (order 0-3)
        order=3
        self.background_parameters_positive = [0]*order
        self.background_parameters_negative = [0]*order
        
        for power in range(0, order):
            name = "background_a{}_positive".format(power)
            self.background_parameters_positive[ power ] = RooRealVar ( name, name, 0.0, -0.2, 0.2 );
            name = "background_a{}_negative".format(power)
            self.background_parameters_negative[ power ] = RooRealVar ( name, name, 0.0, -0.2, 0.2 );
        
        self.initialised = True

    def run_fit(self, use_chi2=False, use_migrad=False):
        """
        Actually runs the fit. Assumes that you have created the 
        signal/background PDFs you want to use to fit.
        """
        
        arglist_of_positive_pdfs = RooArgList()
        arglist_of_negative_pdfs = RooArgList()
        
        for pdf in self.positive_pdfs:
            arglist_of_positive_pdfs.add( pdf );
        
        for pdf in self.negative_pdfs:
            arglist_of_negative_pdfs.add( pdf );
        
        self.model_positive = RooAddPdf ( "modelP", "modelP",
                                     RooArgList( self.signal_positive, self.background_positive) );
        self.model_negative = RooAddPdf ( "modelM", "modelM", 
                                     RooArgList( self.signal_negative, self.background_negative) );
        
        self.model = RooSimultaneous ( "total_model", "total_model", self.category );
        self.model.addPdf ( self.model_positive, "plus" );
        self.model.addPdf ( self.model_negative, "minus" );
        
        self.fit_result = self.model.fitTo (self.dataset, 
                                 RF.Range (self.fit_range[0], self.fit_range[1]), 
                                 RF.Save (), 
                                 RF.Extended (True),
                                 RF.SumW2Error(self.is_weighted));
                                 
        if use_migrad:
            self.fit_result = self.model.fitTo (self.dataset, 
                                 RF.Range (self.fit_range[0], self.fit_range[1]), 
                                 RF.Save (), 
                                 RF.Extended (True),
                                 RF.Minos(True),
                                 RF.Strategy(2),
                                 RF.Minimizer ( "Minuit2" ),
                                 RF.SumW2Error(self.is_weighted));
        
        if use_chi2:
            nll = ROOT.RooChi2Var("nll", "nll", self.model, self.dataset, 
                                  RF.Verbose(True), 
                                  RF.Extended(True),
                                  RF.PrintLevel(8),
                                  RF.NumCPU(1),
                                  RF.DataError(ROOT.RooAbsData.SumW2));
        
        
            m = ROOT.RooMinuit(nll);
            m.setStrategy(1);
            m.setVerbose(True);
            m.setPrintLevel(3);
            m.migrad();   
            m.hesse();

                                 
        kpipi_arglist_final = self.fit_result.floatParsFinal();
        self.floatParsFinal = kpipi_arglist_final
        asig_kpipi = kpipi_arglist_final.find("A_{sig}");
        
        self.fitted_asymmetry = [ self.signal_asymmetry.getVal(), self.signal_asymmetry.getError() ]
        
        return self.fit_result
    
    def calculate_chi2( self ):
        """
        Creates a frame and plots the data on there to calculate the 
        frame chi2.
        """
        canvas = TCanvas("", "", 850,1000)
        frame = self.to_fit_variable.frame()
        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::minus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
                                       
        self.model.plotOn(frame,
                                   RF.Components("modelM"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(self.category), self.dataset),
                                   RF.Name("totalPDF"));

        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::minus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
 
        self.chi2_negative = frame.chiSquare("totalPDF", "Data", self.fit_result.floatParsFinal().getSize())
        
        frame = self.to_fit_variable.frame()
        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::plus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
                                       
        self.model.plotOn(frame,
                                   RF.Components("modelP"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(self.category), self.dataset),
                                   RF.Name("totalPDF"));

        self.dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::plus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
 
        self.chi2_positive = frame.chiSquare("totalPDF", "Data", self.fit_result.floatParsFinal().getSize())

    
    def get_pdf(self):
        """
        This function is available if a fit has ran.
        """
        return self.model

    def get_tag( self ):
        """
        This function is available when initialized.
        """
        return self.category
    
    def getTag( self ):
        return self.get_tag()
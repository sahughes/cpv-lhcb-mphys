""" 
Wrapper around the DplusToKpipiAsymmetryFitter, 
which is also used for the KsPi fit. 

In a nutshell, this fitter first tries many iterations on the combined
D+ and D- binned dataset to create a good set of starting parameters for 
the fit. The fit pdf itself, i.e. a CB + n Gaussians, is selected by
looking at the best chi2 on the combined dataset. The start values are 
chosen in the same way.

@author Laurent Dufour <laurent.dufour@cern.ch>


modded in line ~568 by Samuel Hughes, 2 May 2019
"""

from ROOT import gROOT, RooRealVar, RooCategory
from ROOT import RooArgSet, TFile
from ROOT import RooDataSet
from ROOT import RooRealVar, TCanvas
from ROOT import RooPlot
from ROOT import RooFormulaVar
from ROOT import TLegend
from ROOT import RooFit as RF

from KPiTwoTrackAsymmetry.fit.fitter_kpipi import DplusToKpipiAsymmetryFitter
from KPiTwoTrackAsymmetry.fit import fitter_kpipi_external

from KPiTwoTrackAsymmetry import utilities

import ROOT
import logging

class HistogramBasedAsymmetryFitter:
    def __init__(self, 
                 fit_configuration,
                 input_file_name,
                 input_tree_name,
                 to_fit_variable,
                 id_variable,
                 weight_variable,
                 decay_configuration,
                 output_directory,
                 low_stats):
        self.fit_configuration = fit_configuration
        self.input_file_name = input_file_name
        self.input_tree_name = input_tree_name
        self.to_fit_variable = to_fit_variable
        self.id_variable = id_variable
        self.weight_variable = weight_variable
        self.output_directory=output_directory
        self.low_stats=low_stats
        self.use_johnson = False
        self.use_weight_cutting = False
        self.use_external_pdf=True
        
        # option for weight truncation: a maximum weight to be used.
        self.weight_truncation_value = 40
        
        self.logger = logging.getLogger("AsymmetryFitter")
        self.decay_configuration = decay_configuration
        
        self.initialised=False
    
    def read_tree(self):
        """ Reads the TTree from the file, saves it locally
        in the tree field (together with the TFile handle
        in the file field).
        
        Also computes the normalisation factor of the weights
        as sum(weights)/sum(weights^2) and defines an alias
        for the rescaled weights: "rescaled_weight"
        
        This variable however unfortunately is only used 
        for convenience in generating plots. RooFit does not
        support RoooDataSets with values from a tree alias. 
        
        Method should be run before creating the dataset.
        
        @return void
        """
        logging.debug("Accessing {}".format(  self.input_file_name ))
        self.file = TFile.Open( self.input_file_name, "READONLY" )
        self.tree = self.file.Get( self.input_tree_name )
        
        self.normalisation_factor = utilities.normalise_weights(self.tree, self.weight_variable)
        logging.debug("Using as normalisation factor for the weights: {}".format(self.normalisation_factor))
        
        self.initialised = True
        
        return self.normalisation_factor
        
    def __get_fitter_for_options (self, 
                                  unique_name,
                                  histogram_positive,
                                  histogram_negative,
                                  D_M,
                                  mass_min,
                                  mass_max,
                                  n_parameter,
                                  alpha_parameter,
                                  frac_parameter,
                                  number_of_added_gauss,
                              background_shape):
        """ 
        Given a set of options for the parameters (shared/not-shared), 
        creates an initialized DplusToKpipiAsymmetryFitter object and 
        returns this.
        """
        
        pdf_kpipi = DplusToKpipiAsymmetryFitter( unique_name, D_M, [ mass_min, mass_max ], True );
        pdf_kpipi.set_input_histograms( histogram_positive, histogram_negative );
        pdf_kpipi.initialize()

        if n_parameter == "fixed":
            pdf_kpipi.dPlus_n_negative = pdf_kpipi.dPlus_n_positive
            pdf_kpipi.dPlus_n_negative.setConstant()
        elif n_parameter == "shared":
            pdf_kpipi.dPlus_n_negative = pdf_kpipi.dPlus_n_positive
        
        if alpha_parameter == "fixed":
            pdf_kpipi.dPlus_alpha_negative = pdf_kpipi.dPlus_alpha_positive
            pdf_kpipi.dPlus_alpha_negative.setConstant()
        elif alpha_parameter == "shared":
            pdf_kpipi.dPlus_alpha_negative = pdf_kpipi.dPlus_alpha_positive
        
        if frac_parameter == "fixed":
            pdf_kpipi.frac_CB_GaussA_negative = pdf_kpipi.frac_CB_GaussA_positive
            pdf_kpipi.frac_CB_GaussA_negative.setConstant()
        elif frac_parameter == "shared":
            pdf_kpipi.frac_CB_GaussA_negative = pdf_kpipi.frac_CB_GaussA_positive
            
        if background_shape == "pol":
            pdf_kpipi.set_chebyshev_background(1)
        else:
            pdf_kpipi.set_exponential_background()
            
        if number_of_added_gauss == 2:
            pdf_kpipi.set_cb_double_gauss_signal_pdf()
        else:
            pdf_kpipi.set_cb_single_gauss_signal_pdf(    );      
        
        return pdf_kpipi
        
    def run_fit(self):
        """
        Reads the tree if needed, creates a dataset from
        the tree and performs a binned likelihood fit using
        a PDF. 
        
        The initial conditions for the PDF are chosen adaptively, where
        first a fit is performed on the sum of D+ and D- data.
        The fit with the best chi2 is chosen and these are then start values
        for subsequent strategies.
        """
        if not self.initialised:
            self.read_tree()
        
        mass_variable = self.to_fit_variable
        internal_suffix = self.decay_configuration.canonical_name
            
        mass_min = self.fit_configuration.fit_range[0]
        mass_max = self.fit_configuration.fit_range[1]

        if self.decay_configuration.canonical_name is "kpipi":
            mass_name = "K^{#minus}#pi^{+}#pi^{+}"
            nBins = 2*int(mass_max-mass_min)
        else:
            mass_name = "K^{0}_{S}#pi^{+}"
            nBins = 1*int(mass_max-mass_min)

        D_M = RooRealVar ( mass_variable, "#it{m}(#it{" + mass_name + "}) [MeV/#it{c}^{2}]", mass_min, mass_max )
        D_ID = RooRealVar ( "D_ID", "D_ID", 0, -411, 411 )
        weight = RooRealVar("weight", "weight", 0.5, -1e5, 1e5)

        varset = RooArgSet();
        varset.add ( D_M );
        varset.add ( D_ID );
        varset.add ( weight );
        
        tag = RooCategory("tag", "tag")
        tag.defineType("minus", +411)
        tag.defineType("plus", -411)
        
        histogram_positive = ROOT.TH1F("histogram_positive", "histogram_positive",
                                  nBins, mass_min, mass_max)
        histogram_negative = ROOT.TH1F("histogram_negative", "histogram_negative",
                                  nBins, mass_min, mass_max)
        weights_histogram = ROOT.TH1F("weights", "weights",
                                  200, -10, 200)
        weights_histogram.SetFillColor(30);
        
        D_M.setBins( nBins )
        self.logger.debug("Using as mass range [{}, {}].".format( mass_min, mass_max ))

        #graph_of_efficiency = utilities.get_inverted_efficiency_curve(  self.tree, 
        #                                                                    str(self.normalisation_factor) + "*" + self.weight_variable,
        #                                                                    True );
        #if self.use_weight_cutting:
        #    weight_cut_value = graph_of_efficiency.Eval(0.99);
        #    print "Ideal cut value for the weights: " + str(weight_cut_value);
        #    self.weight_cut_value = weight_cut_value;
        #else:
        self.weight_cut_value = 500.0*self.weight_truncation_value;
        
        self.tree.Draw(mass_variable + ">>histogram_positive", "TMath::Min(" + str(self.weight_truncation_value) + ", "+str(self.normalisation_factor) + "*" + self.weight_variable + ")*("+self.id_variable+">0 && "+str(self.normalisation_factor) + "*" + self.weight_variable + " < " + str(self.weight_cut_value) + ")")
        self.tree.Draw(mass_variable + ">>histogram_negative", "TMath::Min(" + str(self.weight_truncation_value) + ", "+str(self.normalisation_factor) + "*" + self.weight_variable + ")*("+self.id_variable+"<0 && "+str(self.normalisation_factor) + "*" + self.weight_variable + " < " + str(self.weight_cut_value) + ")")
        self.tree.Draw(self.weight_variable + ">>weights")
        
        if histogram_positive.Integral() < 1:
            self.logger.fatal("No entries found for the positive histogram!");
            return;
        
        if histogram_negative.Integral() < 1:
            self.logger.fatal("No entries found for the negative histogram!");
            return;

        c2 = TCanvas("c2")
        histogram_negative.Draw()
        c2.Print("{}data_negative_{}.pdf".format(self.output_directory, internal_suffix))
        histogram_positive.Draw();
        c2.Print("{}data_positive_{}.pdf".format(self.output_directory, internal_suffix));
        weights_histogram.Draw("HIST")
        c2.SetLogy()
        c2.Print("{}weights_{}.pdf".format(self.output_directory, internal_suffix))
        """
        test_pdf_histogram = utilities.pdf_builder_from_weights( self.tree, self.weight_variable );
        test_pdf_histogram.Draw("HIST");
        c2.Print("{}weights_pdf_{}.pdf".format(self.output_directory, internal_suffix))
        c2.Print("{}weights_pdf_{}.png".format(self.output_directory, internal_suffix))
        """
        #graph_of_efficiency.Draw("AC*");
        #c2.Print("{}weight_curve_{}.pdf".format(self.output_directory, internal_suffix)) 
        
        decay_name = internal_suffix
        
        if self.use_external_pdf:
            print "use external pdf."
            pdf_kpipi = fitter_kpipi_external.RadiativeBinnedAsymmetryFitter("radiative",
                                                                          D_M, 
                                                                          [mass_min, mass_max],
                                                                          True)
            pdf_kpipi.set_input_histograms(histogram_positive, histogram_negative);
            pdf_kpipi.initialize()
            pdf_kpipi.create_extended_signal_pdf() 
        else:
            best_chi2 = [50.0, ""]
            best_parameter_settings = None
            tail_parameters = {
                               "alpha": 0.0,
                               "frac_gaussA": 0.0,
                               "frac_gaussB": 0.0,
                               "n": 0.0,
                               "mean": 0.0,
                               "mean_Gauss": 0.0,
                               "mean_GaussB": 0.0,
                               "width_CB": 0.0,
                               "width_Gauss": 0.0,
                               "width_GaussB": 0.0
                               };
            best_error = [50.0, ""];
            
            total_histogram = histogram_positive.Clone("total_histogram");
            total_histogram.Add( histogram_negative, 1. );
            
            for n_parameter in ["fixed", "shared", "free"]:
                for alpha_parameter in ["fixed", "shared", "free"]:
                    for frac_parameter in ["fixed", "shared", "free"]:
                        for number_of_added_gauss in [2]:
                            for background_shape in ["exp"]:
                                unique_name = "n_{}_alpha_{}_frac_{}_nGauss_{}_background_{}".format(
                                            n_parameter,
                                            alpha_parameter,
                                            frac_parameter,
                                            str(number_of_added_gauss),
                                            background_shape);
                                parameter_settings = [n_parameter, alpha_parameter, frac_parameter, number_of_added_gauss, background_shape];
                
                                pdf_kpipi = self.__get_fitter_for_options(unique_name,
                                                                          total_histogram, total_histogram, 
                                                                          D_M, 
                                                                          mass_min, mass_max, 
                                                                          n_parameter, alpha_parameter, 
                                                                          frac_parameter, number_of_added_gauss, 
                                                                          background_shape)
                                                
                                roo_fit_result  = pdf_kpipi.run_fit()
                                pdf_kpipi.calculate_chi2()
                                chi2_positive = pdf_kpipi.chi2_positive;
                                chi2_negative = pdf_kpipi.chi2_negative;
                                asymmetry_value = pdf_kpipi.fitted_asymmetry[0]
                                asymmetry_error = pdf_kpipi.fitted_asymmetry[1]
                                kpipi_arglist_final = pdf_kpipi.floatParsFinal;
                                
                                if (chi2_positive+chi2_negative)/2. < best_chi2[0] and asymmetry_error < 1.0 or best_parameter_settings == None:
                                    best_chi2 = [ (chi2_positive+chi2_negative)/2., unique_name ] 
                                    best_parameter_settings = parameter_settings
                                    tail_parameters["alpha"] = pdf_kpipi.dPlus_alpha_positive.getVal()
                                    tail_parameters["n"] = pdf_kpipi.dPlus_n_positive.getVal()
                                    tail_parameters["width_CB"] = pdf_kpipi.dPlus_sigma_positive.getVal()
                                    tail_parameters["mean"] = pdf_kpipi.dPlus_mean_positive.getVal()
                                    
                                    tail_parameters["delta_mean"] = pdf_kpipi.dPlus_delta_mean_positive.getVal()
                                    
                                    tail_parameters["width_Gauss"] = pdf_kpipi.dPlus_sigma_positive_second.getVal()
                                    tail_parameters["frac_gaussA"] = pdf_kpipi.frac_CB_GaussA_positive.getVal()
                                    tail_parameters["frac_gaussB"] = pdf_kpipi.frac_rest_GaussB_positive.getVal()
                            
                                    tail_parameters["mean_GaussB"] = pdf_kpipi.dPlus_mean_positive_third.getVal()
                                    tail_parameters["width_GaussB"] = pdf_kpipi.dPlus_sigma_positive_third.getVal()
                                
                                if asymmetry_error < best_error[0]:
                                    best_error = [ asymmetry_error, unique_name ]
                                
                                del pdf_kpipi
    
            #adaptive_fit.find_best_fit()
            
            print best_chi2
            print best_error
            print best_parameter_settings
            [n_parameter, alpha_parameter, frac_parameter, number_of_added_gauss, background_shape] = best_parameter_settings; 
            unique_name = "n_{}_alpha_{}_frac_{}_nGauss_{}_background_{}".format(
                                        n_parameter,
                                        alpha_parameter,
                                        frac_parameter,
                                        str(number_of_added_gauss),
                                        background_shape);
            
            pdf_kpipi = self.__get_fitter_for_options(unique_name, histogram_positive, histogram_negative, 
                                                     D_M, mass_min, mass_max, "free", 
                                                     "free", "free", 
                                                     number_of_added_gauss, background_shape)
            
            
            pdf_kpipi.dPlus_alpha_positive.setVal(tail_parameters["alpha"])
            pdf_kpipi.dPlus_n_positive.setVal(tail_parameters["n"])
            pdf_kpipi.dPlus_sigma_positive.setVal(tail_parameters["width_CB"])
            pdf_kpipi.dPlus_mean_positive.setVal(tail_parameters["mean"])
            
            pdf_kpipi.dPlus_delta_mean_positive.setVal(tail_parameters["delta_mean"])
            
            pdf_kpipi.dPlus_sigma_positive_second.setVal(tail_parameters["width_Gauss"])
            pdf_kpipi.frac_CB_GaussA_positive.setVal(tail_parameters["frac_gaussA"])
            pdf_kpipi.frac_rest_GaussB_positive.setVal(tail_parameters["frac_gaussB"])
            pdf_kpipi.dPlus_mean_positive_third.setVal(tail_parameters["mean_GaussB"] )
            pdf_kpipi.dPlus_sigma_positive_third.setVal(tail_parameters["width_GaussB"])
            
            pdf_kpipi.dPlus_alpha_negative.setVal(tail_parameters["alpha"])
            pdf_kpipi.dPlus_n_negative.setVal(tail_parameters["n"])
            #pdf_kpipi.dPlus_sigma_negative.setVal(tail_parameters["width_CB"])
            pdf_kpipi.dPlus_mean_negative.setVal(tail_parameters["mean"])
            pdf_kpipi.dPlus_delta_mean_negative.setVal(tail_parameters["delta_mean"])
            pdf_kpipi.dPlus_sigma_negative_second.setVal(tail_parameters["width_Gauss"])
            pdf_kpipi.frac_CB_GaussA_negative.setVal(tail_parameters["frac_gaussA"])
            pdf_kpipi.frac_rest_GaussB_negative.setVal(tail_parameters["frac_gaussB"])
            pdf_kpipi.dPlus_mean_negative_third.setVal(tail_parameters["mean_GaussB"] )
            pdf_kpipi.dPlus_sigma_negative_third.setVal(tail_parameters["width_GaussB"])
         
        
        """" Best fit is determined, run the final fit. """""
        roo_fit_result  = pdf_kpipi.run_fit(True)
        pdf_kpipi.calculate_chi2()
        chi2_positive = pdf_kpipi.chi2_positive;
        chi2_negative = pdf_kpipi.chi2_negative;
        self.chi2_positive = chi2_positive
        self.chi2_negative = chi2_negative
        asymmetry_value = pdf_kpipi.fitted_asymmetry[0]
        asymmetry_error = pdf_kpipi.fitted_asymmetry[1]
        
        #res_kpipi = pdf_kpipi.run_fit()
        print "Fit complete."
        paramsFinal = pdf_kpipi.floatParsFinal;
        binned_weighted_dataset = pdf_kpipi.dataset
        
        kpipi_arglist_final = paramsFinal;
        print "Trying to find asymmetry"
        asig_kpipi = kpipi_arglist_final.find("A_{sig}");
        
        ROOT.gStyle.SetEndErrorSize(0.0)
        ROOT.gStyle.SetPadLeftMargin( 0.22 );
        
        print "Visualisation."
        """ Visualization of the fit result """
        canvas = TCanvas("c1", "c1", 890,1000)
        canvas.SetTopMargin(0.0)
        canvas.SetBottomMargin(0.0)
        canvas.SetLeftMargin(0.0)
        
        # Divide the canvas in two pads.
        pad1 = ROOT.TPad ( "data_pad", "data_pad", 0.05, 0.29, 0.98, 0.97 );
        pad1.SetBottomMargin(0.02);
        pad1.SetTopMargin(0.07);
        pad1.SetLeftMargin(0.17);
        
        #xlow, ylow, xup, yup
        pad2 = ROOT.TPad("pull_pad", "pull_pad", 0.05, 0.00, 0.98, 0.27);
        pad2.SetTopMargin(0.0);
        pad2.SetBottomMargin(0.65);
        pad2.SetTopMargin(0.0);
        pad2.SetLeftMargin(0.17);
        #pad2.SetFillColor(1); #added line, tryng to change the colour of the pulls so that they are distinguishable from the background
        
        frame = D_M.frame()
        frame.GetYaxis().SetTitleOffset(1.25)
        
        bin_size = frame.GetXaxis().GetBinUpEdge(1) - frame.GetXaxis().GetBinLowEdge(1);
        frame.GetYaxis ().SetTitle( "#it{{Candidates}} / ({0:.2f} MeV/#it{{c}}^{{2}})".format( bin_size ) );

        frame.GetXaxis().CenterTitle()
        frame.GetYaxis().CenterTitle()
        
        binned_weighted_dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::minus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
        pdf_kpipi.get_pdf().plotOn(frame,
                                   RF.Components("modelM"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   RF.Name("totalPDF"));

        pdf_kpipi.get_pdf().plotOn(frame,
                                   RF.Components("signalM"),
                                   RF.LineWidth(2),
                                   RF.LineStyle(ROOT.kDashed),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   RF.Name("signalM"));
        
        pdf_kpipi.get_pdf().getComponents().Print("v")
        pdf_kpipi.get_pdf().plotOn(frame,
                               RF.Components("bkgM"),
                               RF.LineColor(ROOT.kRed), RF.LineStyle(ROOT.kDashed),
                               RF.LineWidth(1),
                               RF.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                               RF.Name("bkgM"));
        
        pdf_kpipi.get_pdf().plotOn(frame,
                                   RF.Components("modelM"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   RF.Name("totalPDF"));
        binned_weighted_dataset.plotOn(frame, RF.Name("Data"),
                                       RF.Cut("tag==tag::minus"),
                                       RF.MarkerSize(1.0),
                                       RF.DataError(ROOT.RooAbsData.SumW2));
        #frame.SetMaximum( 1.1*frame.GetMaximum() )
        frame.GetXaxis().SetLabelSize(0);
        
        h_resid_errors = frame.pullHist ();
        pull_graph = ROOT.TGraph ( h_resid_errors.GetN (), h_resid_errors.GetX (), h_resid_errors.GetY () );
        pull_graph.GetXaxis ().SetLimits ( pull_graph.GetX ()[0], pull_graph.GetX ()[pull_graph.GetN () - 1] );
        pull_graph.GetYaxis().SetRangeUser(-5, 5);
        pull_graph.GetXaxis().SetNdivisions(4);
        pull_graph.GetYaxis().SetNdivisions(8);
        #pull_graph.GetYaxis().SetTitle("#it{Pull}");
        pull_graph.GetXaxis().SetTitle( D_M.GetTitle() );
        pull_graph.GetXaxis().SetTitleSize(0.06* (1.0/0.27));
        pull_graph.GetYaxis().SetTitleSize(0.052* (1.0/0.27));
        pull_graph.GetXaxis().SetLabelSize(0.06* (1.0/0.27));
        pull_graph.GetYaxis().SetTitleOffset(0.32);
        pull_graph.GetXaxis().SetTitleOffset(1.10);
        pull_graph.SetFillColor(1); ######## added
        
        dot_functie = ROOT.TF1("plus_twee_sigma", "2", pull_graph.GetX ()[0], pull_graph.GetX ()[pull_graph.GetN () - 1] )
        dot_functie.SetLineColor(ROOT.kRed)
        dot_functie.SetLineStyle( ROOT.kDotted )
        
        min_twee_sigma = ROOT.TF1("min_twee_sigma", "-2", pull_graph.GetX ()[0], pull_graph.GetX ()[pull_graph.GetN () - 1] )
        min_twee_sigma.SetLineColor(ROOT.kRed)
        min_twee_sigma.SetLineStyle( ROOT.kDotted )
    
        #pull_graph.SetYTitle("#it{Pull}");
        
        canvas.cd();
        pad1.Draw ();
        pad2.Draw()
        
        pad1.cd ();
        frame.Draw ();
        pad2.cd ();
        pull_graph.Draw("AB");
        dot_functie.Draw("SAME")
        min_twee_sigma.Draw("SAME")
        pull_graph.Draw("B SAME");
        
        canvas.cd();
        
        total_weight = binned_weighted_dataset.sumEntries()
        print "Effective statistics of fit: {}".format( total_weight );
        print "chiSquare of fit: " + str( frame.chiSquare("totalPDF", "Data", pdf_kpipi.floatParsFinal.getSize()) )
        
        self.chi2_negative = frame.chiSquare("totalPDF", "Data", pdf_kpipi.floatParsFinal.getSize())

        latex = ROOT.TLatex()
        latex.SetTextColor(2);
        latex.SetNDC();
        latex.SetTextSize(0.04);
        latex.SetTextFont(132);
        latex.SetTextAlign(13);  # align at top
        #,TMath::Prob(chi2Test.getVal(),ndf))
        #latex.DrawLatex( 0.67, 0.73, 
        #                 "#font[12]{{P(#chi^{{2}}/#font[132]{{ndf}})}} = {}".format( ROOT.TMath.Prob(chi2Test.getVal(),ndf)) )


        legend = ROOT.TLegend(0.70,0.67,0.925,0.91)
        entry=legend.AddEntry("Data","Data","LEP");
        entry.SetLineColor(1);
        entry.SetLineStyle(1);
        entry.SetLineWidth(1);
        entry.SetMarkerColor(1);
        entry.SetMarkerStyle(20);
        entry.SetMarkerSize(1);
        
        entry=legend.AddEntry("Fit","Fit","L");
        entry.SetLineColor(ROOT.kBlue);
        entry.SetLineStyle(1);
        entry.SetLineWidth(2);
        entry.SetMarkerColor(1);
        entry.SetMarkerStyle(21);
        entry.SetMarkerSize(1);
        
        entry=legend.AddEntry("Signal","Signal","L");
        entry.SetLineColor(ROOT.kBlue);
        entry.SetLineStyle(ROOT.kDashed);
        entry.SetLineWidth(2);
        entry.SetMarkerColor(1);
        entry.SetMarkerStyle(21);
        entry.SetMarkerSize(1);
        
        entry=legend.AddEntry("Background","Comb. Bkg.","L");
        entry.SetLineColor(ROOT.kRed);
        entry.SetLineStyle(ROOT.kDashed);
        entry.SetLineWidth(2);
        entry.SetMarkerColor(1);
        entry.SetMarkerStyle(21);
        entry.SetMarkerSize(1);

        legend.Draw()

        canvas.SaveAs("{}fit_negative_{}.pdf".format( self.output_directory, decay_name) );
        canvas.SaveAs("{}fit_negative_{}.C".format( self.output_directory, decay_name ) );
        
        #canvas.SetLogy();
        #canvas.SaveAs("{}fit_negative_{}_log.pdf".format( self.output_directory, decay_name) );
        #canvas.SaveAs("{}fit_negative_{}_log.C".format( self.output_directory, decay_name ) );
        
        """ Visualisation of Kpipi fit. This should be abstracted to a function..."""
        print "Visualisation of positive charge fit"
        canvas = TCanvas("c2", "c2", 890,1000)
        canvas.SetTopMargin(0.0)
        canvas.SetBottomMargin(0.0)
        canvas.SetLeftMargin(0.0)

        # Divide the canvas in two pads.
        pad1 = ROOT.TPad ( "data_pad", "data_pad", 0.05, 0.29, 0.98, 0.97 );
        pad1.SetBottomMargin(0.02);
        pad1.SetTopMargin(0.07);
        pad1.SetLeftMargin(0.17);
        
        #xlow, ylow, xup, yup
        pad2 = ROOT.TPad("pull_pad", "pull_pad", 0.05, 0.00, 0.98, 0.27);
        pad2.SetTopMargin(0.0);
        pad2.SetBottomMargin(0.65);
        pad2.SetTopMargin(0.0);
        pad2.SetLeftMargin(0.17);
        #pad2.SetFillColor(1); #added line, tryng to change the colour of the pulls so that they are distinguishable from the background
        print "Getting frame."
        frame = D_M.frame()
        frame.GetYaxis().SetTitleOffset(1.25)
        frame.GetYaxis ().SetTitle( "#it{{Candidates}} / ({0:.2f} MeV/#it{{c}}^{{2}})".format( bin_size ) );

        frame.GetXaxis().CenterTitle()
        frame.GetYaxis().CenterTitle()
        
        print "Getting dataset."
        binned_weighted_dataset.plotOn(frame, RF.Name("DataP"),
                                   RF.Cut("tag==tag::plus"),
                                   RF.MarkerSize(1.0),
                                   RF.DataError(ROOT.RooAbsData.SumW2));   
        print "Plotting model."
        pdf_kpipi.get_pdf().plotOn(frame,
                                   RF.Components("modelP"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(pdf_kpipi.getTag()), binned_weighted_dataset),
                                   RF.Name("totalPDFP"));
                                   
        print "Plotting signal." 

        pdf_kpipi.get_pdf().plotOn(frame,
                                   RF.Components("signalP"),
                                   RF.LineWidth(2),
                                   RF.LineStyle(ROOT.kDashed),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   RF.Name("signalPX"));

        pdf_kpipi.get_pdf().plotOn(frame,
                                   RF.Components("bkgP"),
                                   RF.LineColor(ROOT.kRed), RF.LineStyle(ROOT.kDashed),
                                   RF.LineWidth(1),
                                   RF.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   RF.Name("bkgPX"));

        pdf_kpipi.get_pdf().plotOn(frame,
                                   RF.Components("modelP"),
                                   RF.LineWidth(2),
                                   RF.LineColor(4),
                                   RF.ProjWData(RooArgSet(pdf_kpipi.getTag()),binned_weighted_dataset),
                                   RF.Name("totalPDFP"));
        
        binned_weighted_dataset.plotOn(frame, RF.Name("DataP"),
                                   RF.Cut("tag==tag::plus"),
                                   RF.MarkerSize(1.0),
                                   RF.DataError(ROOT.RooAbsData.SumW2));
        print "Creating frame pull."
        h_resid_errors = frame.pullHist ();
        pull_graph = ROOT.TGraph ( h_resid_errors.GetN (), h_resid_errors.GetX (), h_resid_errors.GetY () );
        pull_graph.GetXaxis ().SetLimits ( pull_graph.GetX ()[0], pull_graph.GetX ()[pull_graph.GetN () - 1] );
        pull_graph.GetYaxis().SetRangeUser(-5, 5);
        pull_graph.GetXaxis().SetNdivisions(4);
        pull_graph.GetYaxis().SetNdivisions(8);
        #pull_graph.GetYaxis().SetTitle("#it{Pull}");
        pull_graph.GetXaxis().SetTitle( D_M.GetTitle() );
        pull_graph.GetXaxis().SetTitleSize(0.06* (1.0/0.27));
        pull_graph.GetYaxis().SetTitleSize(0.052* (1.0/0.27));
        pull_graph.GetXaxis().SetLabelSize(0.06* (1.0/0.27));
        pull_graph.GetYaxis().SetTitleOffset(0.32);
        pull_graph.GetXaxis().SetTitleOffset(1.10);
        pull_graph.SetFillColor(1); #####
    
        #pull_graph.SetYTitle("#it{Pull}");
        print "Going to canvas."
        canvas.cd();
        pad1.Draw ();
        pad2.Draw()
        
        pad1.cd ();
        frame.Draw ();
        pad2.cd ();
        pull_graph.Draw("AB");
        dot_functie.Draw("SAME")
        min_twee_sigma.Draw("SAME")
        pull_graph.Draw("B SAME");
        
        canvas.cd();
        
        legend.Draw()

        self.chi2_positive = frame.chiSquare("totalPDFP", "DataP", pdf_kpipi.floatParsFinal.getSize())
        
        canvas.SetLogy(False);
        canvas.SaveAs("{}fit_positive_{}.pdf".format( self.output_directory, decay_name) );
        canvas.SaveAs("{}fit_positive_{}.C".format( self.output_directory, decay_name ) );
        
        #canvas.SetLogy();
        #canvas.SaveAs("{}fit_positive_{}_log.pdf".format( self.output_directory, decay_name) );
        #canvas.SaveAs("{}fit_positive_{}_log.C".format( self.output_directory, decay_name ) );
        
        self.asymmetry_value = asig_kpipi.getVal()
        self.asymmetry_error = asig_kpipi.getError()
        
        return [self.asymmetry_value, self.asymmetry_error]
// $Id: $
// Include files 

// local
#include "../../KPiTwoTrackAsymmetry/src/DeltaACP_pdf.h"

#include "RooCBShape.h"
#include "RooNovosibirsk.h"
#include <boost/lexical_cast.hpp>
#include <string>
#include <algorithm>
#include <vector>
//#include "libRooJohnsonSU/RooJohnsonSU.h"

#include "myPdf.cc"
#include "RooJohnsonSU.cc"
#include "RooJohnsonSU.h"
//-----------------------------------------------------------------------------
// Implementation file for class : DeltaACP_pdf
//
// 2012-01-06 : Sascha Stahl
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
//DeltaACP_pdf::DeltaACP_pdf(  ) {
//
//
//}

DeltaACP_pdf::DeltaACP_pdf(std::string name, std::string title, RooRealVar& mD0, RooCategory& tag,
                           unsigned int mode, std::vector<std::string> pdfOptions ):
  myPdf (mD0, tag, mode),
  //RooAbsPdf(name,title),
  mean("\\mu_{sig}", "mean", 1866.83, 1850.,1880.,"\\mevcc"),
  deltaMean("\\Delta\\mu_{sig}", "deltaMean", 0.0, -5.,5.,"\\mevcc"),
  //meanP("\\mu_{sig,P}", "meanP", 1865.83, 1850.,1880.),
  sigma1("\\sigma_{1,sig}", "sigma1", 6.0, 2.0, 25.5,"\\mevcc"),
  sigma2("\\sigma_{2,sig}", "sigma2", 9.0, 2.0, 25.5,"\\mevcc"),
  sigma1PX("\\sigma_{1,sig,PX}", "sigma1PX", 6.0, 2.0, 25.5,"\\mevcc"),
  sigma2P("\\sigma_{2,sig,P}", "sigma2P", 9.0, 2.0, 25.5,"\\mevcc"),
  sigma3("\\sigma_{3,sig}", "sigma3", 15.0, 5.0, 25.5,"\\mevcc"),
  frac1("f_{1,sig}", "fraction of g2", 0.20, 0.5, 1.00),
  frac1P("f_{1,sig,P}", "fraction of g2", 0.20, 0.0, 1.00),
  frac2("f_{2,sig}", "fraction of g3", 0.10, 0.0, 1.00),
  scale("s_{2,sig}", "scale of second gaussian", 1.60, 1.0, 4.00),
  scaleP("s_{sig,P}", "scale of other charge", 1.0, 0.5, 1.50),
  a1P("a_{bkg,+}","a1P", -0.0023, -0.01, 0.0001,"(\\mevcc)^{-1}"),  // both exp
  a1M("a_{bkg,-}","a1M", -0.0023, -0.01, 0.0001,"(\\mevcc)^{-1}"),
  a1P2("a_{bkg2,+}","a1P2", -0.01, -0.1, 0.1,"(\\mevcc)^{-1}"),  // both exp
  a1M2("a_{bkg2,-}","a1M2", -0.01, -0.1, 0.1,"(\\mevcc)^{-1}"),
  polycoeffP("gradientP","poly gradient",-1.77552e-01,-1.,0.1),
  polycoeffM("gradientM","poly gradient",-1.79096e-01,-1.,0.1),
  polycoeffP2("gradientP2","poly gradient2",-2.34205e-01,-0.7,0.1),
  polycoeffM2("gradientM2","poly gradient2",-2.15636e-01,-0.7,0.1),
  polycoeffP3("gradientP3","poly gradient3",-0.0,-0.7,1.),
  polycoeffM3("gradientM3","poly gradient3",-0.0,-0.7,1.),

  mean_ref("\\mu_{ref}", "mean_ref", 1796.7, 1700.,1820.,"\\mevcc"),
  sigma_ref("\\sigma_{ref}", "sigma_ref", 7.9, 0.7, 25.5,"\\mevcc"),
  nsig("n_{sig}", "#font[12]{N}_{sig}", 1.4e+5, 0., 2.e+7),
  nrefY("n_{ref,Y}", "# ref yield", 1.4e+5, 0., 2.e+7),
  fref("f_{ref}", "# ref", 0.02, 0., 1.),
  nbkg("n_{bkg}", "# bkg",    2.e+5, 0., 2.e+7),
  asig("A_{sig}","#font[12]{A}_{#kern[-0.01]{sig}}", 0.0, -100., 100.,"\\%"),
  abkg("A_{bkg}","Raw asymmetry bkg", 0.0, -100., 100.,"\\%"),
  aref("A_{ref}","Raw asymmetry ref", -1.7, -100., 100.,"\\%"),
  alpha("\\alpha","\\alpha",1.5,-10.,10.),
  n("n","n",2.0,0,50),
  alphaP("\\alphaP","\\alpha",1.5,-10.,10.),
  nP("nP","n",2.0,0,50),
  alpha2("\\alpha2","\\alpha2",-1.5,-10.,10.),
  delta("\\delta","\\delta",50.0,0.,1000.),
  gamma("\\gamma","\\gamma",1.0,-10.,10.),
  n2("n2","n2",2.0,0,50),
  f_bkgP("f_bkgP","f_bkgP",0.7,0.0,1.0),
  f_bkgM("f_bkgM","f_bkgM",0.7,0.0,1.0),
  m_name(name),
  m_title(title)
{
  /** modes
      0 = pipi
      1 = kk
      2 = kpi
      3 = kpipi
      4 = kspiLL
      5 = kspiDD
  **/

  m_mass = &mD0;
  m_tag = &tag;
  m_result = &asig;
  
  m_mode = mode;
  //Options
  useDG = true;
  bool useSG = false;
  bool noCB = false;
  m_fixScale = false;
  
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("fixScale"))!=pdfOptions.end() ){
    m_fixScale = true;
    scaleP.setConstant();
  }
  
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("sg"))!=pdfOptions.end() ){
    useSG = true;
    useDG = false;
    noCB = true;
  }
  
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("noCB"))!=pdfOptions.end() ){
    useSG = false;
    useDG = true;
    noCB = true;
    std::cout<<"Fitoption noCB"<<std::endl;
  }
  
  bool diffDG = false;
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("diffDG"))!=pdfOptions.end() ){
    useSG = false;
    useDG = true;
    noCB = true;
    diffDG = true;
  }
  
  bool useJohnson = false;
  
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("johnson"))!=pdfOptions.end() ){
    useJohnson = true;
    useDG = true;
    noCB = true;
  }

  bool isSWeightMode = false;
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("sWeightMode"))!=pdfOptions.end() ){
    isSWeightMode = true;
  }

  useExpBkgP = true;
  useExpBkgM = true;
  //useDG = false;
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("pol"))!=pdfOptions.end() ){
    useExpBkgP = false;
    useExpBkgM = false;
    m_usePol = true;  
  }
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("johnsonPol"))!=pdfOptions.end() ){
    useJohnson = true;
    useDG = true;
    noCB = true;
    useExpBkgP = false;
    useExpBkgM = false;
    m_usePol = true;  
  }
  
  m_useJohnson = useJohnson;  

  m_fixReflection = false;
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("fixRef"))!=pdfOptions.end() ){
    m_fixReflection = true;
    
  }
  m_isHighStatistics = false;
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("highStatistics"))!=pdfOptions.end() ){
    m_isHighStatistics = true;
  }

  
  // Signal model

  // Crystal Ball
  alpha.setVal(2.0);
  alphaP.setVal(2.0);
  //alpha.setConstant();
  alphaP.setConstant();
  n.setVal(1.5);
  nP.setVal(1.5);
  //n.setConstant();
  nP.setConstant();
  //RooFormulaVar minus_deltaM("minus_deltaM","-1*@0",deltaM);
  sigma2S = new RooFormulaVar("sigma2S","@0*@1",RooArgList(scale,sigma1));
  //sigma2SP = new RooFormulaVar("sigma2SP","@0*@1",RooArgList(scaleP,sigma1P));
  sigma1P = new RooFormulaVar("sigma1P","@0*@1",RooArgList(scaleP,sigma1));
  sigma2SP = new RooFormulaVar("sigma2SP","@0*@1",RooArgList(scale,*sigma1P));


  meanP = new RooFormulaVar("meanP","@0+@1",RooArgList(mean,deltaMean));
  //sigma2S = RooFormulaVar("sigma2S","sigma2S","(@0*@1)",RooArgList(scale,sigma2));
  
  //Gauss1 = new RooCBShape("crystal","CB PDF",mD0,mean,sigma1, alpha, n);
  //if (mode == 0 || mode==1){
  
  //Gauss1 = new RooNovosibirsk ("novo","CB PDF",mD0,mean,sigma1, alpha);
  //Gauss1 = new RooGaussian("Gauss1", "Gauss1", mD0, mean, sigma1);
  //Gauss2 = new RooCBShape ("crystal","CB PDF",mD0,mean,sigma2, alpha, n);
  //Gauss1 = new RooCBShape ("crystal","CB PDF",mD0,mean,sigma1, alpha, n);
  //Gauss2 = new RooGaussian("Gauss2", "Gauss2", mD0, mean, *sigma2S);
  


  if (noCB){
    Gauss1M = new RooGaussian("Gauss1M", "Gauss1m", mD0, mean, sigma1);
    if (!diffDG){
      Gauss1P = new RooGaussian("Gauss1P", "Gauss1P", mD0, *meanP, *sigma1P);
    }else{
      Gauss1P = new RooGaussian("Gauss1P", "Gauss1P", mD0, *meanP, sigma1PX);
    }
    
  }else{
    Gauss1M = new RooCBShape ("crystalM","CB PDFM", mD0, mean, sigma1, alpha, n);
    Gauss1P = new RooCBShape ("crystalP","CB PDFP", mD0, *meanP, *sigma1P, alpha, n);
  }
  
  
  //Gauss3M = new RooGaussian("Gauss3M", "Gauss3M", mD0, mean, sigma3);  
  //Gauss3P = new RooGaussian("Gauss3P", "Gauss3P", mD0, *meanP, sigma3);
  if (useJohnson){
    Gauss2M = new RooJohnsonSU("Gauss2P", "Gauss2P", mD0, mean, *sigma2S,delta,gamma);
    Gauss2P = new RooJohnsonSU("Gauss2M", "Gauss2M", mD0, *meanP, *sigma2SP,delta,gamma);
  }else{
    if(!diffDG){
      Gauss2M = new RooGaussian("Gauss2M", "Gauss2M", mD0, mean, *sigma2S);
      Gauss2P = new RooGaussian("Gauss2P", "Gauss2P", mD0, *meanP, *sigma2SP);
    }else{
      Gauss2M = new RooGaussian("Gauss2M", "Gauss2M", mD0, mean, sigma2);
      Gauss2P = new RooGaussian("Gauss2P", "Gauss2P", mD0, *meanP, sigma2P);
    }
    
  }
  
  
  if (!m_isHighStatistics){
    signalM = new RooAddPdf("signalM", "signalM", RooArgList(*Gauss1M, *Gauss2M), frac1);
    //signalM = new RooAddPdf("signalM", "signalM", RooArgList(*Gauss1M, *Gauss2M,*Gauss3M), RooArgList(frac1,frac2),true);
    if (!diffDG){
      signalP = new RooAddPdf("signalP", "signalP", RooArgList(*Gauss1P, *Gauss2P), frac1);
    }else{
      signalP = new RooAddPdf("signalP", "signalP", RooArgList(*Gauss1P, *Gauss2P), frac1P);
    }
  }else{
    //Gauss3M = new RooJohnsonSU("Gauss3P", "Gauss3P", mD0, mean, sigma3,delta,gamma);
    //Gauss3P = new RooJohnsonSU("Gauss3M", "Gauss3M", mD0, *meanP, sigma3,delta,gamma);
    Gauss3M = new RooGaussian("Gauss3P", "Gauss3P", mD0, mean, sigma3);
    Gauss3P = new RooGaussian("Gauss3M", "Gauss3M", mD0, *meanP, sigma3);

    signalM = new RooAddPdf("signalM", "signalM", RooArgList(*Gauss1M, *Gauss2M, *Gauss3M), RooArgList(frac1,frac2),true);
    signalP = new RooAddPdf("signalP", "signalP", RooArgList(*Gauss1P, *Gauss2P, *Gauss3M), RooArgList(frac1,frac2),true);

  }
  
  
  //signalP = new RooAddPdf("signalP", "signalP", RooArgList(*Gauss1P, *Gauss2P,*Gauss3P), RooArgList(frac1,frac2),true);  
  
  if (useDG == false)
  {
    frac1.setVal(1.);
    frac1.setConstant();
    sigma2.setConstant();
    scale.setConstant();
    
  }
  
   // Background model
  
  bkgP_poly = new RooChebychev("polyP","#Lambda_{c} Mass Poly",mD0,RooArgList(polycoeffP,polycoeffP2,polycoeffP3));
  bkgM_poly = new RooChebychev("polyM","#Lambda_{c} Mass Poly",mD0,RooArgList(polycoeffM,polycoeffM2,polycoeffM3));
  //polycoeff.setConstant();
  polycoeffM2.setVal(0.0);
  polycoeffP2.setVal(0.0);
  //polycoeffM2.setConstant();
  //polycoeffP2.setConstant();
  
  polycoeffM3.setVal(0.0);
  polycoeffP3.setVal(0.0);
  polycoeffM3.setConstant();
  polycoeffP3.setConstant();
  
  
  bkgP_exp = new RooExponential("bkgP", "First order exponential", mD0, a1P );
  bkgM_exp = new RooExponential("bkgM", "First order exponential", mD0, a1M );
  
  
  if (useExpBkgP){
    bkgP = bkgP_exp;
  }else{
    bkgP = bkgP_poly;
  }
  
  if (useExpBkgM){
    bkgM = bkgM_exp;
  }else{
    bkgM = bkgM_poly;
  }
  
   
  reflection= new RooGaussian("reflection", "reflection", mD0, mean_ref, sigma_ref);

  aref.setVal(-1.63);
  fref.setVal(0.04);
  
  aref.setVal(0);
  
  fref.setVal(0.);
  aref.setConstant();
  fref.setConstant();
  
  
  
 
  // Asymmetry
  // set up blinding
  bool useBlinding = false;
  std::string blindingString = "";
  asig_bs = new RooCategory("asig_bs","blindig state of abs");
  asig_bs->defineType("Unblind",0);
  asig_bs->defineType("Blind",1);
  
  if(std::find(pdfOptions.begin(), pdfOptions.end(),std::string("Blind"))!=pdfOptions.end() ){
    if (mode==0){
      useBlinding = true;
      blindingString = "klarelinie";
    }
    if (mode==1){
      useBlinding = true;
      blindingString = "klarerkurs";
    }
    std::cout<<"#######   Blind the result with string "<<blindingString<<std::endl;
  }
  

  if ( useBlinding ){
    asig_unblind = new RooUnblindPrecision("asig_unblind","asig (unblind)",blindingString.c_str(),asig.getVal(),-0.3,asig,*asig_bs,false);
    asig_bs->setLabel("Blind");  
    //asig_bs->setLabel("Unblind");  
    asig_bs->setConstant();
    nsigP = new RooFormulaVar("nsigP","number of signal events plus","0.5*@0*(1-@1/100.)",RooArgList(nsig,*asig_unblind));
    nsigM = new RooFormulaVar("nsigM","number of signal events minus","0.5*@0*(1+@1/100.)",RooArgList(nsig,*asig_unblind));
  }else{
    asig_unblind = 0;
    nsigP = new RooFormulaVar("nsigP","#font[12]{N}_{#kern[-0.25]{sig}}^{+}","0.5*@0*(1-@1/100.)",RooArgList(nsig,asig));
    nsigM = new RooFormulaVar("nsigM","#font[12]{N}_{#kern[-0.25]{sig}}^{#font[122]{-}}","0.5*@0*(1+@1/100.)",RooArgList(nsig,asig));
    
    if (mode>2){
      nsigM->SetTitle("#font[12]{N}_{#kern[-0.25]{sig}}^{+}");
      nsigP->SetTitle("#font[12]{N}_{#kern[-0.25]{sig}}^{#font[122]{-}}");
    }
  }
  

  
  //nsigP = new RooFormulaVar("nsigP","number of signal events plus","0.5*@0*(1-@1/100.)",RooArgList(nsig,asig));
  //nsigM = new RooFormulaVar("nsigM","number of signal events minus","0.5*@0*(1+@1/100.)",RooArgList(nsig,asig));

  nref = new RooFormulaVar("nref","number of reflection events","@0*@1",RooArgList(fref,nsig));
  if (!isSWeightMode){
    nrefP = new RooFormulaVar("nrefP","number of reflection events plus","0.5*@0*(1-@1/100.)",RooArgList(*nref,aref));
    nrefM = new RooFormulaVar("nrefM","number of reflection events minus","0.5*@0*(1+@1/100.)",RooArgList(*nref,aref));
  }else{
    nrefP = new RooFormulaVar("nrefP","number of reflection events plus","0.5*@0*(1-@1/100.)",RooArgList(nrefY,aref));
    nrefM = new RooFormulaVar("nrefM","number of reflection events minus","0.5*@0*(1+@1/100.)",RooArgList(nrefY,aref));
  }
  


  nbkgP = new RooFormulaVar("nbkgP","number of background events plus","0.5*@0*(1-@1/100.)",RooArgList(nbkg,abkg));
  nbkgM = new RooFormulaVar("nbkgM","number of background events minus","0.5*@0*(1+@1/100.)",RooArgList(nbkg,abkg));

  if (m_mode == 0 ){
    fref.setVal(0.04);
    aref.setVal(0.0);
    aref.setConstant(false);
    fref.setConstant(false);
    extraP = reflection;
    extraM = reflection;
    modelP = new RooAddPdf("modelP", "modelP", RooArgList(*signalP,*bkgP,*reflection),RooArgList(*nsigP,*nbkgP,*nrefP));
    modelM = new RooAddPdf("modelM", "modelM", RooArgList(*signalM,*bkgM,*reflection),RooArgList(*nsigM,*nbkgM,*nrefM));
  }else{
    modelP = new RooAddPdf("modelP", "modelP", RooArgList(*signalP,*bkgP),RooArgList(*nsigP,*nbkgP));
    modelM = new RooAddPdf("modelM", "modelM", RooArgList(*signalM,*bkgM),RooArgList(*nsigM,*nbkgM));
    extraP = 0;
    extraM = 0;
  }
  

  modelMass =  new RooAddPdf("modelMass", "modelMass", RooArgList(*signalP,*bkgP),
                              RooArgList(*nsigP,*nbkgP));
  
  
  simPdf = new RooSimultaneous(name.c_str(),name.c_str(),tag);
  simPdf->addPdf(*modelM,"minus");
  simPdf->addPdf(*modelP,"plus");
  
  if (simPdf) {
    std::cout<<"defined pdf"<<std::endl;
  }
  
  fixDefaultShape();
  if (useSG){
    frac1.setVal(1.);
  }
  if (m_fixReflection){
    aref.setConstant();
  }
  
  m_signalYield = &nsig;
  m_signalYieldP = nsigP;
  m_signalYieldM = nsigM;
  
}

void DeltaACP_pdf::fixDefaultShape()
{
  alpha.setMin(0.0);
  // fix parameters
  if (m_mode==0) fixShapePiPi();
  if (m_mode==1) fixShapeKK(); 
  if (m_mode==2) fixShapeKPi(); 
  if (m_mode==3) fixShapeKpipi();
  if (m_mode==4) fixShapeKspiLL();
  std::cout<<"Mode "<<m_mode<<std::endl;
}

void DeltaACP_pdf::fixShapeKpipi()
{
  sigma1.setVal(7.14);
  mean.setVal(1869.817);
  //abkg.setVal(-0.108);
  //a1P.setVal(-0.0020336);
  //a1M.setVal(-0.0021468);
  
  //nsig.setVal( 235338/2. );
  //nbkg.setVal( 449818/2. );
  

  
  scale.setVal( 1.70 );
  //scale.setConstant();
  frac1.setVal( 0.527 );
  frac1.setConstant();
  
  //sigma1.setConstant();
  
  
  //frac1.setVal( 1.0 );
  //frac1.setConstant();
  
  // Crystal Ball
  alpha.setVal(2.116);
  //alpha.setConstant();
  n.setVal(0.9);
  n.setConstant();
  scaleP.setVal(1.0);
  //scaleP.setConstant();
  
  if (m_useJohnson){
    
    // for johnson tests only temporary comment out again
    scale.setVal( 1.265 );
    //scale.setConstant();
    
    frac1.setVal(0.53);
    frac1.setConstant();
    //deltaMean.setConstant();
    delta.setVal(1.263);
    //delta.setConstant();
    gamma.setVal(0.1718);
    //gamma.setConstant();
  }
  if (m_isHighStatistics){
    delta.setVal(1.3);
    delta.setConstant();
    gamma.setVal(0.12);
    //n.setConstant(false);
    gamma.setConstant();
    frac2.setVal(0.4);
    frac2.setConstant();
    sigma3.setVal(9.0);
    //sigma3.setConstant(9.0);
  }
  
  polycoeffP.setVal(-2.34205e-01);
  polycoeffM.setVal(-1.79096e-01);
  polycoeffP2.setVal(-2.34205e-01);
  polycoeffM2.setVal(-2.34205e-01);




}


void DeltaACP_pdf::fixShapeKspiLL()
{
  
  sigma1.setVal(7.14);
  mean.setVal(1869.817);
  //abkg.setVal(-0.108);
  //a1P.setVal(-0.0020336);
  //a1M.setVal(-0.0021468);
  
  //nsig.setVal( 235338/2. );
  //nbkg.setVal( 449818/2. );
  

  
  scale.setVal( 1.70 );
  scale.setConstant();
  frac1.setVal( 0.627 );
  //frac1.setConstant();
  
  //sigma1.setConstant();
  
  
  //frac1.setVal( 1.0 );
  //frac1.setConstant();
  
  // Crystal Ball
  alpha.setVal(2.5);
  alpha.setConstant();
  n.setVal(1.5);
  n.setConstant();

  // for johnson tests only temporary comment out again
  scaleP.setVal(1.0);
  //scaleP.setConstant();

  if (m_useJohnson){
    scale.setVal( 1.265 );
    //scale.setConstant();
    
    frac1.setVal(0.53);
    //frac1.setConstant();
    //deltaMean.setConstant();
    delta.setVal(1.5);
    //delta.setConstant();
    gamma.setVal(0.05);
    //gamma.setConstant();
  }
  
}


void DeltaACP_pdf::fixShapePiPi()
{
  //no meam mass difference or resolution difference
  deltaMean.setVal(0.0);
  deltaMean.setConstant();
  //scale.setConstant();
  scaleP.setVal(1.0);
  scaleP.setConstant();
  frac1.setVal( 0.25 );
  // Crystal Ball
  alpha.setVal(1.5);
  alpha.setConstant(1.0);

  n.setVal(2.0);
  n.setConstant();

  aref.setVal( -1.6 );

  fref.setVal( 0.04 );
  mean_ref.setVal( 1792.3 );
  //mean_ref.setConstant();
  //sigma_ref.setVal( 9.9);
  //sigma_ref.setConstant();

  //aref.setConstant();
  // for johnson tests only temporary comment out again
  
  scaleP.setVal(1.0);
  scaleP.setConstant();
  if (m_useJohnson){
    fref.setVal( 0.052 );
    //fref.setConstant();
    //aref.setConstant();
    
    //mean_ref.setConstant();
    //sigma_ref.setConstant();
    
    scale.setVal( 1.43 );
    scale.setConstant();

    frac1.setVal(0.53);
    frac1.setConstant();
    delta.setVal(1.3);
    delta.setConstant();
    gamma.setVal(0.155);
    gamma.setConstant();
    
  }

  //if (m_usePol){
    //fref.setConstant();
    //aref.setConstant();
    //mean_ref.setConstant();
    //sigma_ref.setConstant();
  //}
  
  //sigma1.setVal(7.14);
  //mean.setVal(1864.817);
  //abkg.setVal(-0.108);
  //a1P.setVal(-0.0020336);
  //a1M.setVal(-0.0021468);
  //
  //
  //
  //
  //scale.setVal( 1.577 );
  ////scale.setConstant();
  //frac1.setVal( 0.527 );
  ////frac1.setConstant();
  //
  ////sigma1.setConstant();
  //
  //
  ////frac1.setVal( 1.0 );
  ////frac1.setConstant();
  //
  //fref.setVal(0.0483);
  ////fref.setVal(0.0);
  ////fref.setConstant();
  //aref.setVal( -1.64 );
  ////aref.setConstant();
  //
  //mean_ref.setVal( 1794.49 );
  //mean_ref.setConstant();
  //sigma_ref.setVal( 9.30);
  //sigma_ref.setConstant();
  //
  //
  //// Crystal Ball
  //alpha.setVal(2.0);
  ////alpha.setConstant();
  //n.setVal(1.2);
  ////n.setConstant();
}
void DeltaACP_pdf::setDefaultsPiPi()
{
  
  sigma1.setVal(7.03);
  mean.setVal(1866.552);
  abkg.setVal(-0.106);
  a1P.setVal(-0.002157);
  a1M.setVal(-0.002245);
  
  //up
  nsig.setVal( 98827 );
  nbkg.setVal( 148179 );
 
  //down
  //nsig.setVal( 139441 );
  //nbkg.setVal( 212780 );
  
  scale.setVal( 1.573 );
  //scale.setConstant();
  frac1.setVal( 0.479 );
  //frac1.setConstant();

  fref.setVal(0.0478);
  fref.setConstant();

  aref.setVal( -1.63 );
  aref.setConstant();


  mean_ref.setVal( 1796.03 );
  mean_ref.setConstant();
  sigma_ref.setVal( 7.92);
  sigma_ref.setConstant();
  
}

void DeltaACP_pdf::fixShapeKK()
{
  std::cout<<"fix shape for KK"<<std::endl;
  //no meam mass difference or resolution difference
  deltaMean.setVal(0.0);
  deltaMean.setConstant();
  scaleP.setVal(1.0);
  scaleP.setConstant();
  // Crystal Ball
  alpha.setVal(2.7);
  alpha.setConstant();
  n.setVal(1.5);
  n.setConstant();
  //std::cout<<alpha.getVal()<<std::endl;
  frac1.setVal( 0.65 );
  sigma1.setVal(5.5);
  scale.setVal(1.6);
  //mean.setVal(1866.399);
  //abkg.setVal(-0.253);
  a1P.setVal(-0.0054);
  a1M.setVal(-0.0054);
  
  //nsig.setVal( 594951/2 );
  //nbkg.setVal( 262102/2. );
  
  if (m_useJohnson){
    //scale.setVal( 1.265 );
    //scale.setConstant();

    frac1.setVal(0.52);
    frac1.setConstant();
    //deltaMean.setConstant();
    delta.setVal(1.4);
    //delta.setConstant();
    gamma.setVal(0.02);
    //gamma.setConstant();
    
  }
  
  //scale.setVal( 1.613 );
  //scale.setConstant();
  
  //frac1.setConstant();
  
  //frac1.setVal( 1.0 );
  //frac1.setConstant();
  
  //alpha.setVal(0.0);
  //alpha.setConstant();
  //n.setVal(1.2);
  //n.setConstant();

  //fref.setVal(0.0);
  //fref.setConstant();
  //aref.setVal( -1.63 );
  //aref.setConstant();
  

  //mean_ref.setVal( 1796.03 );
  //mean_ref.setConstant();
  //sigma_ref.setVal( 7.92);
  //sigma_ref.setConstant( );

  
}

void DeltaACP_pdf::setDefaultsKK()
{
  sigma1.setVal(5.497);
  mean.setVal(1866.139);
  abkg.setVal(-0.253);
  a1P.setVal(-0.0057677);
  a1M.setVal(-0.0060705);
  
  //nsig.setVal( 594951/2. );
  //nbkg.setVal( 262102/2. );
  nsig.setVal( 594951 );
  nbkg.setVal( 262102 );
  

  //up
  nsig.setVal( 250069 );
  nbkg.setVal( 108199 );


  //down
  //nsig.setVal( 354267 );
  //nbkg.setVal( 156092 );
  
  
  scale.setVal( 1.598 );
  scale.setConstant();
  frac1.setVal( 0.623 );
  frac1.setConstant();
  fref.setVal(0.0);
  fref.setConstant();
  aref.setVal( -1.63 );
  aref.setConstant();
  mean_ref.setVal( 1796.03 );
  mean_ref.setConstant();
  sigma_ref.setVal( 7.92);
  sigma_ref.setConstant( );

  
}


void DeltaACP_pdf::setDefaultsKPi()
{
}

void DeltaACP_pdf::fixShapeKPi()
{
  sigma1.setVal(6.703);
  mean.setVal(1866.2965);
  abkg.setVal(-0.684);
  a1P.setVal(-0.003210);
  a1M.setVal(-0.00321);
  
  //nsig.setVal( 1217023 );
  //nbkg.setVal( 37329*2. );
  

  
  scale.setVal( 1.6330 );
  //scale.setConstant();
  frac1.setVal( 0.7027 );
  //frac1.setConstant();
  
  //frac1.setVal( 1.0 );
  //frac1.setConstant();
  
  alpha.setVal(2.3225);
  //alpha.setConstant();
  n.setVal(1.5);
  n.setConstant();
  scaleP.setVal(1.0);
  
  //scaleP.setConstant();

  //n.setVal(0.988);
  //n.setConstant();

  fref.setVal(0.0);
  fref.setConstant();
  aref.setVal( -1.63 );
  aref.setConstant();
  

  mean_ref.setVal( 1796.03 );
  mean_ref.setConstant();
  sigma_ref.setVal( 7.92);
  sigma_ref.setConstant( );

  if (m_useJohnson){
    //tests only temporary comment out again
    scaleP.setVal(1.0);
    //scaleP.setConstant();
    scale.setVal( 1.78 );
    //scale.setConstant();
    
    frac1.setVal(0.7);
    //frac1.setConstant();
    //deltaMean.setConstant();
    delta.setVal(1.3);
    delta.setConstant();
    gamma.setVal(0.12);
    gamma.setConstant();
  }
  
  
  frac1.setVal(0.7);
  //frac1.setConstant();
  //deltaMean.setConstant();
  if (m_isHighStatistics){
    delta.setVal(1.3);
    delta.setConstant();
    gamma.setVal(0.12);
    //n.setConstant(false);
    gamma.setConstant();
    frac2.setVal(0.4);
    frac2.setConstant();
    sigma3.setVal(9.0);
    //sigma3.setConstant(9.0);
  }
  
  
}


void DeltaACP_pdf::setStartParameters(RooAbsData* m_dataBinned)
{
  std::cout<<"Set start Parameters for the fit"<<std::endl;
  m_dataBinned->Print("v");
  std::cout<<"Sum Entries: "<<m_dataBinned->sumEntries()<<std::endl;
  double max = getMass()->getMax();
  double min = getMass()->getMin();
  std::string cut = "";
  std::string s_mass = std::string(getMass()->GetName());
  cut =  s_mass + ">" + boost::lexical_cast<std::string>(max-20);
  cut = cut + " && "+ s_mass +" < " + boost::lexical_cast<std::string>(max);
  std::cout<<cut<<std::endl;
  std::cout<<"Sum Entries cut: "<<m_dataBinned->sumEntries(cut.c_str(),s_mass.c_str())<<std::endl;
  double count  = m_dataBinned->sumEntries(cut.c_str(),s_mass.c_str());
  double bkgEst = (max-min)/(20.)*count;
  nbkg.setVal(bkgEst);
  //nbkgP.setVal(bkgEst/2.);
  //nbkgM.setVal(bkgEst/2.);
  //nbkgP.setMin(m_dataBinned->sumEntries()/1000.);
  //nbkgM.setMin(m_dataBinned->sumEntries()/1000.);
  nbkg.setMin(0.0);
  nbkg.setMax(m_dataBinned->sumEntries()*3.5);
  //nbkgP.setMax(m_dataBinned->sumEntries()/2.);
  //nbkgM.setMax(m_dataBinned->sumEntries()/2.);
  double sigEst = m_dataBinned->sumEntries()-bkgEst;
  //nsig.setVal(sigEst);
  //m_dataBinned->sum
  nsig.setMax(1000*m_dataBinned->sumEntries());
  nsig.setMin(m_dataBinned->sumEntries()/100000.);
 
  nrefY.setVal(0.04*sigEst);
  nrefY.setMax(sigEst);
  nrefY.setMin(0.0);

  if (!deltaMean.isConstant()){
    deltaMean.setVal(0.);
    deltaMean.setConstant(false);
  }
  if (!scaleP.isConstant()){
    scaleP.setVal(1.);
    scaleP.setConstant(false);
  }
  if (!scale.isConstant()){
    scale.setVal(1.6);
    scale.setConstant(false);
  }

  //std::cout<<alpha.getVal()<<std::endl;
  asig.setVal(0.);
  abkg.setVal(0.);
  //fixDefaultShape();
  
 
  std::cout<<"bkgEst "<<bkgEst<<" sigEst "<<sigEst<<std::endl;
  return;
}

void DeltaACP_pdf::generate()
{
  simPdf->generate(RooArgSet(*m_mass,*m_tag),RooFit::Name("data_unbinned"),
                   RooFit::NumEvents(1000));
}

  

void DeltaACP_pdf::calculateQuantities(RooFitResult* fitResult)
{
  //double width = sqrt(frac1.getVal()*sigma1.getVal()*sigma1.getVal()
  //                    +(1-frac1.getVal())*sigma2.getVal()*sigma2.getVal());

  RooFormulaVar totalWidth("totalWidth","sqrt(@0*@1*@1+(1-@0)*@2*@2*@1*@1)",
                           RooArgList(frac1,sigma1,scale));
  std::cout<<"total width: "<<totalWidth.getVal()<<" "<<totalWidth.getPropagatedError(*fitResult)<<std::endl;
  RooRealVar totalWidth_var("\\sigma_{av}","\\sigma_{av}",totalWidth.getVal());
  totalWidth_var.setError(totalWidth.getPropagatedError(*fitResult));
  
}

//=============================================================================
// Destructor
//=============================================================================

//=============================================================================

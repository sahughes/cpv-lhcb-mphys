#ifndef ROO_RADIATIVETAIL
#define ROO_RADIATIVETAIL

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
class RooRealVar;

class RooRadiativeTail : public RooAbsPdf {
public:
  RooRadiativeTail() {} ;
  RooRadiativeTail(const char *name, const char *title,
                   RooAbsReal& _mass, RooAbsReal& _slope, 
                   RooAbsReal& _mean, RooAbsReal& _sigma1,
                   RooAbsReal& _sigma2,
	           RooAbsReal& _frac1);
  
  RooRadiativeTail(const RooRadiativeTail& other, const char* name=0) ;
  double f(double r);
  
  virtual TObject* clone(const char* newname) const { return new RooRadiativeTail(*this,newname); }
  inline virtual ~RooRadiativeTail() { }

  //Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  //Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:

  RooRealProxy mass ;
  RooRealProxy slope ;
  RooRealProxy mean ;
  RooRealProxy sigma1 ;
  RooRealProxy sigma2 ;
  RooRealProxy frac1 ; 
  //  friend double f_GSL(double r, void * param);
  Double_t evaluate() const ;
  gsl_integration_workspace *w;
  
  

private:

  ClassDef(RooRadiativeTail,1)
};

#endif

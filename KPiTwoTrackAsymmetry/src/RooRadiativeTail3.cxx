//////////////////////////////////////////////////////////////////////////////
//
// BEGIN_HTML
// Plain Gaussian p.d.f
// END_HTML
//

#include "RooFit.h"

#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_erf.h>
#include "RooRadiativeTail3.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"
#include "RooMath.h"
#include "RooFormulaVar.h"
#include "RooGaussian.h"



ClassImp(RooRadiativeTail3)

double RooRadiativeTail3_f_GSL(double,void*);

  
//_____________________________________________________________________________
RooRadiativeTail3::RooRadiativeTail3(const char *name, const char *title,
				     RooAbsReal& _mass, RooAbsReal& _slope,
				     RooAbsReal& _mean,
					 RooAbsReal& _deltaMean,
					 RooAbsReal& _deltaMean_b,
					 RooAbsReal& _sigma1,
				     RooAbsReal& _sigma2, RooAbsReal& _sigma3,
				     RooAbsReal& _frac1, RooAbsReal& _frac3,
				     RooInt& _reverse ) :
  RooAbsPdf(name,title),
  mass("mass","Observable",this,_mass),
  slope("slope","Slope",this,_slope),
  mean("mean","Mean",this,_mean),
  deltamean("deltamean", "deltaMean", this, _deltaMean),
  deltamean_b("deltamean_b", "deltaMean_b", this, _deltaMean_b),
  sigma1("sigma1","Sigma1",this,_sigma1),
  sigma2("sigma2","Sigma2",this,_sigma2),
  sigma3("sigma3","Sigma3",this,_sigma3),
  frac1("frac1","Frac1",this,_frac1),
  frac3("frac3","Frac3",this,_frac3),
  reverse(_reverse)
{
  w=gsl_integration_workspace_alloc(100);
}



//_____________________________________________________________________________
RooRadiativeTail3::RooRadiativeTail3(const RooRadiativeTail3& other, const char* name) : 
  RooAbsPdf(other,name), 
  mass("mass",this,other.mass), slope("slope",this,other.slope),
  mean("mean",this,other.mean),
  deltamean("deltamean", this, other.deltamean),
  deltamean_b("deltamean_b", this, other.deltamean_b),
  sigma1("sigma1",this,other.sigma1),
  sigma2("sigma2",this,other.sigma2), sigma3("sigma3",this,other.sigma3),
  frac1("frac1",this,other.frac1), frac3("frac3",this,other.frac3),
  reverse(other.reverse)
{
    w=gsl_integration_workspace_alloc(100);
}


double RooRadiativeTail3::f(double r) 
{
  
  double v = frac1*0.3989422806/sigma1*exp(-(mass-r)*(mass-r)/2./sigma1/sigma1)+
    (1-frac1-frac3)*0.3989422806/sigma2*exp(-(mass-r+deltamean)*(mass-r+deltamean)/2./sigma2/sigma2)+
    frac3*0.3989422806/sigma3*exp(-(mass-r)*(mass-r+deltamean_b)/2./sigma3/sigma3);

  return v;

}


double RooRadiativeTail3_f_GSL(double r, void * param)
{
  RooRadiativeTail3 * istance= (RooRadiativeTail3*) param;
  return istance->f(r);
}



//_____________________________________________________________________________
Double_t RooRadiativeTail3::evaluate() const {


  //double start, stop;  
  


  double result(0), error(0);
  gsl_integration_qaws_table *t =0;
  gsl_function F;


  if(reverse == 0 ) {
    t=gsl_integration_qaws_table_alloc(0, slope<=-1?-0.999:slope, 0, 0);
  }
  if(reverse == 1 ) {
    t=gsl_integration_qaws_table_alloc(slope<=-1?-0.999:slope,0, 0, 0);  
    }

  F.function = &RooRadiativeTail3_f_GSL;
  F.params = (void*)this;
  
  
  double high = mass.max()+5*sigma3; 
  double low = mass.min()-5*sigma3;
  if(reverse == 0 ) gsl_integration_qaws(&F, low, mean, t,0,1e-7,100,w, &result, &error);
  if(reverse == 1 ) gsl_integration_qaws(&F,mean,high,t,0,1e-7,100,w, &result, &error);
  gsl_integration_qaws_table_free(t);
  
  return result;

}



// $Id: $
#ifndef TRUNK_DELTAACP_PDF_H 
#define TRUNK_DELTAACP_PDF_H 1

// Include files

/** @class DeltaACP_pdf DeltaACP_pdf.h trunk/DeltaACP_pdf.h
 *  
 *
 *  @author Sascha Stahl
 *  @date   2012-01-06
 */
#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooAbsReal.h"
#include "RooCategory.h"
#include "TObject.h"
#include "RooAbsPdf.h"
#include "RooAbsData.h"
#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "RooFormulaVar.h"
#include "RooChebychev.h"
#include "RooExponential.h"
#include "RooUnblindPrecision.h"
#include "RooFitResult.h"

#include <string>

#include "myPdf.h"


class DeltaACP_pdf : public myPdf {
public: 
  /// Standard constructor
  DeltaACP_pdf( ); 
  DeltaACP_pdf(std::string name, std::string title, RooRealVar& mD0, RooCategory& tag, 
               unsigned int mode, std::vector<std::string> pdfOptions = std::vector<std::string>()); 
  /*DeltaACP_pdf(const char* name, const char * title, RooRealVar& mD0, RooCategory& tag, 
               unsigned int mode)
  {
    std::vector<std::string> pdfOptions;
    DeltaACP_pdf(name, title, mD0, tag, 
                 mode, pdfOptions); 
                 }*/
  
  /*DeltaACP_pdf(const DeltaACP_pdf& other, const char* name = 0 );
  
  virtual TObject* clone (const char * newname) const 
  {
    return new DeltaACP_pdf(*this, newname);
  }
  */

  void setDefaultsPiPi();
  void setDefaultsKK();
  void fixShapePiPi();
  void fixShapeKK();
  void fixShapeKPi();
  void fixShapeKpipi();
  void fixShapeKspiLL();
  //void fixDefaultShape();
  void setDefaultsKPi();

  void fitOnlyMass(bool option = true)
  {
    if (option){
      asig.setVal(-100.);
      asig.setConstant(-100.);
      abkg.setVal(-100.);
      abkg.setConstant(-100.);
      aref.setVal(-100.);
      aref.setConstant(-100.);
    }
    return;
  };
  
  

  virtual ~DeltaACP_pdf( ) {} ; ///< Destructor

  //options

  bool useDG;
  bool useExpBkgP;
  bool useExpBkgM;
  
  //TObject* clone(const char* newname) const { return new DeltaACP_pdf(this->name); } ;
  
  //protected:

  //private:

  virtual void setStartParameters(RooAbsData* m_dataBinned);
  virtual void secondTry(){
    std::cout<<"second try not implemented"<<std::endl;
    return;
    //scale.setVal(0.);
    //scale.setError(0.);
    //scale.setConstant();
    deltaMean.setVal(0.);
    deltaMean.setError(0.);
    deltaMean.setConstant();
    scaleP.setVal(1.);
    scaleP.setError(0.);
    scaleP.setConstant();
    return;
  }
  virtual void thirdTry(){
    scale.setVal(1.6);
    scale.setError(0.);
    scale.setConstant();
    return;
  }
  virtual void calculateQuantities(RooFitResult* fitResult);
  void fixDefaultShape();
  
  const RooRealVar& getResult () const
  {
    return asig;
  };
  
  virtual void massOnlyFit()
  {
    asig.setVal(-100.);
    abkg.setVal(-100.);
    aref.setVal(-100.);
    
    asig.setConstant();
    abkg.setConstant();
    aref.setConstant();
    return;
  };


  RooRealVar mean;
  //RooRealVar meanP;
  RooRealVar deltaMean;
  RooRealVar sigma1;
  RooRealVar sigma2;
  RooRealVar sigma1PX;
  RooRealVar sigma2P;
  RooRealVar sigma3;
  RooRealVar frac1;
  RooRealVar frac1P;
  RooRealVar frac2;
  RooRealVar scale;
  RooRealVar scaleP;
  
  RooRealVar a1P;
  RooRealVar a1M;
  RooRealVar a1P2;
  RooRealVar a1M2;

  RooRealVar polycoeffP;
  RooRealVar polycoeffM;
  RooRealVar polycoeffP2;
  RooRealVar polycoeffM2;
  RooRealVar polycoeffP3;
  RooRealVar polycoeffM3;

  RooRealVar mean_ref;
  RooRealVar sigma_ref;

  RooRealVar nsig;
  RooRealVar nrefY;  
  RooRealVar fref;
  RooRealVar nbkg;
  
  RooRealVar asig;
  RooRealVar abkg;
  RooRealVar aref;
  
  RooRealVar alpha;
  RooRealVar n;
  RooRealVar alphaP;
  RooRealVar nP;
  RooRealVar alpha2;
  RooRealVar delta;
  RooRealVar gamma;
  RooRealVar n2;
  RooRealVar f_bkgP;
  RooRealVar f_bkgM;
  
  RooFormulaVar* meanP;
  RooFormulaVar* sigma2S;
  RooAbsPdf* Gauss1;
  RooAbsPdf* Gauss2;
  RooAbsPdf* Gauss3;
  RooAddPdf* signal;
  //RooAddPdf* signalM;
  //RooRealVar sigma1M;
  //RooRealVar sigma2M;
  //RooRealVar scaleM;
  //RooRealVar frac1M;

  RooFormulaVar* sigma2SM;
  RooAbsPdf* Gauss1M;
  RooAbsPdf* Gauss2M;
  RooAbsPdf* Gauss3M;
  
  RooFormulaVar* sigma1P;

  RooFormulaVar* sigma2SP;
  //RooAddPdf* signalP;
  RooAbsPdf* Gauss1P;
  RooAbsPdf* Gauss2P;
  RooAbsPdf* Gauss3P;

  RooGaussian* reflection;
  


  RooUnblindPrecision* asig_unblind;
  RooCategory* asig_bs;
  

  RooFormulaVar* nbkgP;
  RooFormulaVar* nbkgM;

  //RooRealVar nref;

  RooFormulaVar* nref;


  //RooRealVar nrefP;
  //RooRealVar nrefM;
 
  RooFormulaVar* nrefP;
  RooFormulaVar* nrefM;
  
  

  RooFormulaVar* nsigP;
  RooFormulaVar* nsigM;
  //RooAddPdf* modelM;
  //RooAddPdf* modelP;
  RooAddPdf* modelMass;
  


 
  


  RooChebychev* bkgP_poly;
  RooChebychev* bkgM_poly;
  //RooAbsPdf* bkgP;
  //RooAbsPdf* bkgM;

 
  
  RooExponential* bkgP_exp;
  RooExponential* bkgM_exp;
  
  RooExponential* bkgP_exp2;
  RooExponential* bkgM_exp2;
  
  
  //RooSimultaneous* simPdf;
  
  
  //Double_t evaluate() const{
  //  return simPdf->evaluate();
  //};
  
  // const RooRealVar * getMass () const
//   {
//     return m_mass;
//   };
  
 
//   const RooCategory * getTag () const
//   {
//     return m_tag;
//   };
  
 
  RooRealVar* get_mD0() {return m_mass;};
  
  RooRealVar get_nsig() { return nsig; };
    
  void generate();
  
private:
  bool m_useJohnson;
  bool m_usePol;
  bool m_fixScale;
  bool m_fixReflection;
  bool m_isHighStatistics;
  std::string m_name;
  std::string m_title;
  
  
  //RooRealVar* m_mass;
  //RooCategory* m_tag;
  //int m_mode;
  
  //ClassDef(DeltaACP_pdf,1)
};
#endif // TRUNK_DELTAACP_PDF_H

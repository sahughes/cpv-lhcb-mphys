//////////////////////////////////////////////////////////////////////////////
//
// BEGIN_HTML
// Plain Gaussian p.d.f
// END_HTML
//

#include "RooFit.h"

#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_erf.h>
#include "RooRadiativeTail.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"
#include "RooMath.h"
#include "RooFormulaVar.h"
#include "RooGaussian.h"



ClassImp(RooRadiativeTail)

double f_GSL(double,void*);

  
//_____________________________________________________________________________
RooRadiativeTail::RooRadiativeTail(const char *name, const char *title,
                                   RooAbsReal& _mass, RooAbsReal& _slope,
                                   RooAbsReal& _mean, RooAbsReal& _sigma1,
                                   RooAbsReal& _sigma2,
				   RooAbsReal& _frac1) :
  RooAbsPdf(name,title),
  mass("mass","Observable",this,_mass),
  slope("slope","Slope",this,_slope),
  mean("mean","Mean",this,_mean),
  sigma1("sigma1","Sigma1",this,_sigma1),
  sigma2("sigma2","Sigma2",this,_sigma2),
  frac1("frac1","Frac1",this,_frac1)
{

  w=gsl_integration_workspace_alloc(100);
}



//_____________________________________________________________________________
RooRadiativeTail::RooRadiativeTail(const RooRadiativeTail& other, const char* name) : 
  RooAbsPdf(other,name), 
  mass("mass",this,other.mass), slope("slope",this,other.slope),
  mean("mean",this,other.mean), sigma1("sigma1",this,other.sigma1),
  sigma2("sigma2",this,other.sigma2), 
  frac1("frac1",this,other.frac1)
{
    w=gsl_integration_workspace_alloc(100);
}


double RooRadiativeTail::f(double r) 
{
  
  double v=
    frac1/0.3989422806/sigma1*exp(-(mass-r)*(mass-r)/2./sigma1/sigma1)+
    (1-frac1)/0.3989422806/sigma2*exp(-(mass-r)*(mass-r)/2./sigma2/sigma2);
  
  return v;

}


double f_GSL(double r, void * param)
{
  RooRadiativeTail * istance= (RooRadiativeTail*) param;
  return istance->f(r);
}



//_____________________________________________________________________________
Double_t RooRadiativeTail::evaluate() const {


  double start, stop;  
  


  double result(0), error(0);
  gsl_integration_qaws_table *t;
  gsl_function F;


  t=gsl_integration_qaws_table_alloc(0, slope<=-1?-0.999:slope, 0, 0);
  


  F.function = &f_GSL;
  F.params = (void*)this;
  
  
  double low = mass.min()-5*sigma2;
  
  gsl_integration_qaws(&F, low, mean<low?low+0.1:mean, t,0,1e-8,100,w, &result, &error);
  gsl_integration_qaws_table_free(t);
  
  return result;

}



#ifndef ROO_RADIATIVETAIL3
#define ROO_RADIATIVETAIL3

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooInt.h"
class RooRealVar;

class RooRadiativeTail3 : public RooAbsPdf {
public:
  RooRadiativeTail3() {} ;
  RooRadiativeTail3(const char *name, const char *title,
		    RooAbsReal& _mass, RooAbsReal& _slope, 
		    RooAbsReal& _mean,
			RooAbsReal& _deltaMean,
			RooAbsReal& _deltaMean_b,
			RooAbsReal& _sigma1,
		    RooAbsReal& _sigma2,
			RooAbsReal& _sigma3,
		    RooAbsReal& _frac1, RooAbsReal& _frac3,
		    RooInt& reverse);
  
  RooRadiativeTail3(const RooRadiativeTail3& other, const char* name=0) ;
  double f(double r);
  
  virtual TObject* clone(const char* newname) const { return new RooRadiativeTail3(*this,newname); }
  inline virtual ~RooRadiativeTail3() { }

  //Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  //Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:

  RooRealProxy mass ;
  RooRealProxy slope ;
  RooRealProxy mean ;
  RooRealProxy deltamean ;
  RooRealProxy deltamean_b ;
  RooRealProxy sigma1 ;
  RooRealProxy sigma2 ;
  RooRealProxy sigma3 ;
  RooRealProxy frac1 ;
  RooRealProxy frac3 ;
  RooInt reverse;
  //  friend double f_GSL(double r, void * param);
  Double_t evaluate() const ;
  gsl_integration_workspace *w;
  
  

private:

  ClassDef(RooRadiativeTail3,1)
};

#endif

// $Id: $
#ifndef TRUNK_MYPDF_H 
#define TRUNK_MYPDF_H 1

// Include files
#include "RooRealVar.h"
#include "RooCategory.h"
#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooSimultaneous.h"
#include "RooFitResult.h"

/** @class myPdf myPdf.h trunk/myPdf.h
 *  
 *
 *  @author Sascha Stahl
 *  @date   2013-08-26
 */
class myPdf {
public: 
  /// Standard constructor
  myPdf(  RooRealVar& mass, RooCategory& tag, int mode );
  
  virtual ~myPdf( ); ///< Destructor
  virtual void setStartParameters(RooAbsData* data) = 0;
  virtual void secondTry() = 0;
  virtual void thirdTry() = 0;
  virtual void calculateQuantities(RooFitResult* fitResult) = 0;
  virtual void fixDefaultShape() = 0;
  virtual void massOnlyFit() = 0;
  
  void plot();
  
  
  virtual const RooRealVar& getResult () const = 0;
  
  //void setResult (RooRealVar* result){
  //  m_result = result;
  //};

  RooRealVar * getMass () const{
    return m_mass;
  };

  RooCategory * getTag () const{
    return m_tag;
  };

  int getMode () const{
    return m_mode;
  };

  RooAbsPdf* get_bkgP () const
  {
    return bkgP;
  };

  RooAbsPdf* get_bkgM () const
  {
    return bkgM;
  };

  RooAbsPdf* get_signalP () const
  {
    return signalP;
  };

  RooAbsPdf* get_signalM () const
  {
    return signalM;
  };

  RooAbsPdf* get_modelP () const
  {
    return modelP;
  };

  RooAbsPdf* get_modelM () const
  {
    return modelM;
  };
  
  RooAbsPdf* get_extraP () const
  {
    return extraP;
  };

  RooAbsPdf* get_extraM () const
  {
    return extraM;
  };

  RooSimultaneous* get_pdf () const
  {
    if (simPdf==NULL){
      std::cout<<"something is wrong here"<<std::endl;
    }
    return simPdf;
  };

  RooRealVar* get_signalYield () const
  {
    return m_signalYield;
  };

  RooFormulaVar* get_signalYieldP () const
  {
    return m_signalYieldP;
  };

  RooFormulaVar* get_signalYieldM () const
  {
    return m_signalYieldM;
  };

  
protected:
  RooAbsPdf* bkgP;
  RooAbsPdf* bkgM;
  
  RooAbsPdf* signalP;
  RooAbsPdf* signalM;

  RooAbsPdf* extraP;
  RooAbsPdf* extraM;
  
  RooAbsPdf* modelM;
  RooAbsPdf* modelP;

  RooSimultaneous* simPdf;

  RooRealVar* m_signalYield;
  RooFormulaVar* m_signalYieldP;
  RooFormulaVar* m_signalYieldM;
  
  RooRealVar* m_result;
  RooRealVar* m_mass;
  RooCategory* m_tag;
  int m_mode;

private:
   
};
#endif // TRUNK_MYPDF_H

//c++
#include <iostream>
#include <cmath>
#include <stdlib.h>//for validation files getenv.
//root

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TMath.h>
#include <TLine.h>
#include <TVector3.h>
#include <TPaveText.h>
#include <TRandom3.h>
#include <TString.h>
#include <TFile.h>
#include <TObject.h>
#include <TTree.h>
#include <TCut.h>
#include <THStack.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLeaf.h>
#include <TProfile2D.h>
#include <TChain.h>
#include <TPaveStats.h>
#include <TArrow.h>
#include <TString.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TText.h>

//roofit
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include <RooFitResult.h>
#include <RooRealVar.h>
#include <RooDataSet.h>
#include <RooGaussian.h>
#include <RooChebychev.h>
#include <RooPolynomial.h>
#include <RooArgusBG.h>
#include <RooAddPdf.h>
#include <RooPlot.h>
#include <RooHist.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooBinning.h>
#include <RooDstD0BG.h>
#include <RooNLLVar.h>
#include <Roo1DTable.h>

//external
#include "../../KPiTwoTrackAsymmetry/src/DeltaACP_pdf.h"
using namespace std;
using namespace RooFit;

int main(int argc, char* const argv[]){
  //gROOT->ProcessLine(".L path/to/lhcbStyle.C");
  cout<<"Code to simply get the raw asymmetry from weighted files"<<endl;
  if(argc<2){
    cout<<"========================"<<endl;
    cout<<"expecting"<<endl;
    cout<<"[1] the weighted Kpipi file"<<endl;
    cout<<"[2] the weighted Kspi file"<<endl;
    cout<<"========================"<<endl;
    return 0;

  }
  TFile *fKpipi =TFile::Open(argv[1]);
  TFile *fKspi = TFile::Open(argv[2]);

  TTree* trKpipi = (TTree*)fKpipi->Get("weightedTree");
  //calculate sascha's normalization
  double SumW = 0;
  double SumW2 = 0;
  for(int i=0; i<trKpipi->GetEntriesFast();++i){
    trKpipi->GetEntry(i);
    double val = trKpipi->GetLeaf("weight")->GetValue();
    SumW+=val;
    SumW2+=val*val;
  }
  double norm = SumW/SumW2;
  cout<<"kpipi: sascha's normalization factor = "<<norm<<endl;
  TString zeFormm = Form("%f*@0",norm);
  //  cout<<Form("Kpipi: using formula %f*@0",norm)<<endl;
  //kspi
  TTree* trKspi = (TTree*)fKspi->Get("weightedTree");
  SumW = 0;
  SumW2= 0;
  for(int i=0; i<trKspi->GetEntriesFast();++i){
    trKspi->GetEntry(i);
    double val = trKspi->GetLeaf("weight")->GetValue();
    SumW+=val;
    SumW2+=val*val;
  }
  double normKs = SumW/SumW2;
  cout<<"Kspi: sascha's normalization factor = "<<normKs<<endl;
  //TString zeFormm = Form("%f*@0",norm);
  //cout<<Form("using formula %f*@0",norm)<<endl;


  //fits
  double minMassDplus = 1815;
  double maxMassDplus = 1920;
  RooRealVar mDKpipi("D_DTF_M_PV","D_DTF_M_PV",1869.6,minMassDplus,maxMassDplus);
  RooRealVar D_ID("D_ID","D_ID",0,-411,411);
  RooCategory tag("D_ID","D_ID");
  tag.defineType("minus", +411);
  tag.defineType("plus", -411);

  RooRealVar weight("weight","weight",0.5,0,1e5);
  RooArgSet theSet(mDKpipi,tag,weight);
  //RooFormulaVar weightNorm("weightNorm",zeFormm.Data(),RooArgList(weight));//sascha's weight
  RooDataSet kpipi_dataset("kpipi_dataset","kpipi_dataset",theSet);
  //make the dataset by hand.
  double mass;
  double did;
  double ww;
  trKpipi->SetBranchAddress("D_DTF_M_PV",&mass);
  trKpipi->SetBranchAddress("D_ID",&did);
  trKpipi->SetBranchAddress("weight",&ww);
  trKpipi->SetBranchStatus("*",0);
  trKpipi->SetBranchStatus("D_DTF_M_PV",1);
  trKpipi->SetBranchStatus("D_ID",1);
  trKpipi->SetBranchStatus("weight",1);
  for(int i=0; i<trKpipi->GetEntries();++i){
    trKpipi->LoadTree(i);
    trKpipi->GetEntry(i);
    if ( did < 0){ theSet.setCatLabel("D_ID","plus");}
    else{theSet.setCatLabel("D_ID","minus");}
    if(mass<minMassDplus||mass>maxMassDplus)continue;
    weight = ww*norm;
    mDKpipi = mass;
    kpipi_dataset.add(theSet);
  }
  
  //RooDataSet kpipi_dataset("kpipi_dataset","kpipi dataset",trKpipi,
  //RooArgSet(mDKpipi,tag,weight),0,"weight");//
  //RooDataSet kpipi_dataset("kpipi_dataset","kpipi_dataset",RooArgSet(mDKpipi,tag,weight),Import(*trKpipi));
  
  //kpipi_dataset.Print("v");
  // Roo1DTable* table = kpipi_dataset.table(tag) ;
  // table->Print();
  
  //   auto list = kpipi_dataset.split(tag);
  //   auto dataSetPos =  (RooDataSet*)( list->FindObject ( "plus" ) );
  //   //dataSetPos->Print("v");
  //   auto dataSetNeg =  (RooDataSet*)( list->FindObject ( "minus" ) );
  //   dataSetNeg->Print("v");
  //   dataSetPos->Print("v");
  TCanvas cc("cc","cc");
  RooPlot* frame = mDKpipi.frame();
  //RooPlot* frame2 = mDKpipi.frame();

  DeltaACP_pdf* pdf_kpipi = new DeltaACP_pdf("pdf_kpipi","kpipi", mDKpipi,tag,3);  
  //dataset.

  //RooRealVar* w = (RooRealVar*) kpipi_dataset.addColumn(weightNorm);
  RooDataSet wdata(kpipi_dataset.GetName(),kpipi_dataset.GetTitle(),&kpipi_dataset,*(kpipi_dataset.get()),0,weight.GetName()) ;
  cout<<"printing wdata"<<endl;
  wdata.Print("v");
  //RooDataHist *kpipi_dataset2 = kpipi_dataset.binnedClone("kpipi_dataset2","kpipi_dataset2");
  RooDataHist *kpipi_dataset2 = wdata.binnedClone("kpipi_dataset2","kpipi_dataset2");
  kpipi_dataset2->Print("v");
  pdf_kpipi->setStartParameters(kpipi_dataset2);
  RooFitResult * res_kpipi = pdf_kpipi->get_pdf()->fitTo( *kpipi_dataset2,Save(kTRUE),SumW2Error(kFALSE),/*,NumCPU(20)*/Extended(kTRUE)/*,Strategy(2)*/);
  const RooArgList& kpipi_arglist_final = res_kpipi->floatParsFinal();
  RooRealVar* asig_kpipi = (RooRealVar*)kpipi_arglist_final.find("A_{sig}");
  //if (res_kpipi->covQual()!=3 || res_kpipi->status()!=0){
  //pdf_kpipi->secondTry();
    //res_kpipi = pdf_kpipi->get_pdf()->fitTo(*kpipi_dataset2, RooFit::Save(kTRUE),RooFit::SumW2Error(true));
  //}
  kpipi_dataset2->plotOn(frame);
  pdf_kpipi->get_pdf()->plotOn(frame,RooFit::ProjWData(RooArgSet(*(pdf_kpipi->getTag())),*kpipi_dataset2),
			       RooFit::Name("totalPDF"));
    
  pdf_kpipi->get_pdf()->plotOn(frame,
			       Components(RooArgSet(*(pdf_kpipi->get_bkgP()),*(pdf_kpipi->get_bkgM()))),
			       RooFit::ProjWData(RooArgSet(*(pdf_kpipi->getTag())),*kpipi_dataset2),
			       RooFit::Name("bkg"), LineColor(kGreen+2),LineStyle(6));

  pdf_kpipi->get_pdf()->plotOn(frame,
                           Components(RooArgSet(*(pdf_kpipi->get_signalM()),*(pdf_kpipi->get_signalP()))),
                           RooFit::ProjWData(RooArgSet(*(pdf_kpipi->getTag())),*kpipi_dataset2),
                           RooFit::Name("signal"), LineColor(kMagenta+2),LineStyle(2));
  //pdf_kpipi->get_pdf()->slice()->plotOn(frame);
  frame->Draw();
  cc.SaveAs("thefit_kpipi.pdf");
  cc.SaveAs("thefit_kpipi.C");
  cc.SetLogy(true);
  cc.SaveAs("thefit_kpipi_logy.pdf");
  cc.SetLogy(false);
  cc.Clear();
  //kspi

  RooRealVar mDKspi("D_DTF_M_PV_CON","D_DTF_M_PV_CON",1869.6,minMassDplus,maxMassDplus);
  //RooRealVar D_ID("D_ID","D_ID",0,-411,411);
  //RooCategory tag("D_ID","D_ID");
  //tag.defineType("minus", +411);
  //tag.defineType("plus", -411);

  //RooRealVar weight("weight","weight",0.5,0,1e5);
  RooArgSet theSetKspi(mDKspi,tag,weight);
  //RooFormulaVar weightNorm("weightNorm",zeFormm.Data(),RooArgList(weight));//sascha's weight
  RooDataSet kspi_dataset("kspi_dataset","kspi_dataset",theSetKspi);
  //make the dataset by hand.
  mass = 0;
  trKspi->SetBranchAddress("D_DTF_M_PV_CON",&mass);
  trKspi->SetBranchAddress("D_ID",&did);
  trKspi->SetBranchAddress("weight",&ww);
  trKspi->SetBranchStatus("*",0);
  trKspi->SetBranchStatus("D_DTF_M_PV_CON",1);
  trKspi->SetBranchStatus("D_ID",1);
  trKspi->SetBranchStatus("weight",1);
  for(int i=0; i<trKspi->GetEntries();++i){
    trKspi->LoadTree(i);
    trKspi->GetEntry(i);
    if ( did < 0){ theSetKspi.setCatLabel("D_ID","plus");}
    else{theSetKspi.setCatLabel("D_ID","minus");}
    if(mass<minMassDplus||mass>maxMassDplus)continue;
    weight = ww*normKs;
    mDKspi = mass;
    //cout<<"the_ks_mass = "<<mass<<endl;
    kspi_dataset.add(theSetKspi);
  }
  kspi_dataset.Print("v");
  RooPlot* frame2 = mDKspi.frame();

  DeltaACP_pdf* pdf_kspi = new DeltaACP_pdf("pdf_kspi","kspi", mDKspi,tag,4);  
  RooDataSet wdata2(kspi_dataset.GetName(),kspi_dataset.GetTitle(),&kspi_dataset,*(kspi_dataset.get()),0,weight.GetName()) ;
  wdata2.Print("v");
  wdata2.plotOn(frame2);
  frame2->Draw();
  cc.SaveAs("tmp.png");
  RooDataHist* kspi_dataset2 = wdata2.binnedClone("kspi_dataset2","kspi_dataset2");
  pdf_kspi->setStartParameters(kspi_dataset2);
  RooFitResult * res_kspi = pdf_kspi->get_pdf()->fitTo( *kspi_dataset2,Save(kTRUE),SumW2Error(kFALSE),/*,NumCPU(20)*/Extended(kTRUE)/*,Strategy(2)*/);
  const RooArgList& kspi_arglist_final = res_kspi->floatParsFinal();
  RooRealVar* asig_kspi = (RooRealVar*)kspi_arglist_final.find("A_{sig}");
  cc.Clear();
  kspi_dataset2->plotOn(frame2);

  pdf_kspi->get_pdf()->plotOn(frame2,RooFit::ProjWData(RooArgSet(*(pdf_kspi->getTag())),*kspi_dataset2),
			       RooFit::Name("totalPDF"));
  pdf_kspi->get_pdf()->plotOn(frame2,
			      Components(RooArgSet(*(pdf_kspi->get_bkgP()),*(pdf_kspi->get_bkgM()))),
			      RooFit::ProjWData(RooArgSet(*(pdf_kspi->getTag())),*kspi_dataset2),
			      RooFit::Name("bkg"), LineColor(kGreen+2),LineStyle(6));
  pdf_kspi->get_pdf()->plotOn(frame,
                           Components(RooArgSet(*(pdf_kspi->get_signalM()),*(pdf_kspi->get_signalP()))),
                           RooFit::ProjWData(RooArgSet(*(pdf_kspi->getTag())),*kspi_dataset2),
                           RooFit::Name("signal"), LineColor(kMagenta+2),LineStyle(2));  
  frame2->Draw();
  cc.SaveAs("thefit_kspi.pdf");
  cc.SaveAs("thefit_kspi.C");
  cc.SetLogy(true);
  cc.SaveAs("thefit_kspi_logy.pdf");
  cc.SetLogy(false);
  cout<<"*******************************"<<endl;
  cout<<"ARaw(Kpipi): "<<asig_kpipi->getVal()<<" +/- "<<asig_kpipi->getError()<<endl;
  cout<<"ARaw(Kspi) : "<<asig_kspi->getVal()<<" +/- "<<asig_kspi->getError()<<endl;
  return 0;
}

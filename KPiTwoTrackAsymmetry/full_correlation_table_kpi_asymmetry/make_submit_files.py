#! /usr/bin/env python

from optparse import OptionParser
import math

years = [2011]
polarities = ["MagUp"]

pt_bins = [ [2000,4700], [4700, 6500], [6500, 8500], [8500, 25000] ]
y_bins = [ [20,30], [30,35], [35,45] ]
dalitz_regions = ["PhiPi", "KstarK", "KKpiNR"]

job_configuration = []

for year in years:
    for polarity in polarities:
        for [pt_bin_min, pt_bin_max] in pt_bins:
            for [y_bin_min, y_bin_max] in y_bins:
                for dalitz_region in dalitz_regions:
                    job_configuration.append( 
                            {"polarity": polarity,
                             "magnet_polarity": polarity,
                             "year": year,
                             "pt_bin_min": pt_bin_min,
                             "pt_bin_max": pt_bin_max,
                             "y_bin_min": y_bin_min,
                             "y_bin_max": y_bin_max,
                             "dalitz_region": dalitz_region})
numJobs = len(job_configuration)

parser = OptionParser()
parser.add_option( "--queue",      default = 'long')
parser.add_option( '--maxNumJobs', default = -1, type=int )
parser.add_option( '--sleepTime',  default = 1*60, type=int )
parser.add_option( "--parName", default = "material_data" )
parser.add_option( "--fileId", default = 1, type=int )
parser.add_option( "--numJobs",   default = numJobs, type = int )
(options,args) = parser.parse_args()

projParName = options.parName
numOfJobs   = options.numJobs
maxJobsInQ  = 1000
startJobIdx = 0
endJobIdx   = numOfJobs - 1
sleepTime   = options.sleepTime # seconds
username   = 'laurentd'

# sanity checks
if projParName.endswith('/'):  projParName = projParName.split('/')[0]

import os
workDir = os.getcwd() + '/'
dataOutDir = workDir + 'output/'

init_script = '/project/bfys/lhcb/sw/setup.sh'
submit_script = '/project/bfys/vsyropou/repos/variusScripts/handle_distributed_data/submitingScripts/submitAndWaitToStbQ.sh'
correlation_script = '/user/laurentd/projectdir/tuple_analysis/AsymmetryTools/projects/KPiTwoTrackAsymmetry/akk/calculate_correlation.py'

# make output directories
for folder_name in ['input', 'output','logs','conf_bash_job']:
    if not os.path.exists(folder_name):
        os.system('mkdir %s'%folder_name)

print 'Prepared %s input files on %s'%(numOfJobs,projParName)

for jobID in range( numOfJobs ):
    internalJobId = jobID
    scriptname = workDir + 'conf_bash_job/AKK_Correlations_%s_%s.sh' % (projParName,jobID)
    fscript=open(scriptname,'w')
    polarityInt = 0
    
    job_config = job_configuration[ jobID ]
    for key,val in job_config.items():
        exec(key + '=val')

    fscript.write('#PBS -o %s \n'%( scriptname[:-2].replace('conf_bash_job','logs') + 'log') )
    fscript.write('#PBS -e %s \n'%( scriptname[:-2].replace('conf_bash_job','logs') + 'err') )

    fscript.write('source  %s \n'%init_script)
    fscript.write('. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v8r4p1/InstallArea/scripts/SetupProject.sh DaVinci v41r2p1\n')
    fscript.write('cd /user/laurentd/projectdir/tuple_analysis/AsymmetryTools/projects/KPiTwoTrackAsymmetry \n')

    parFileIn  = workDir + 'input/'  + '%s_%s.par' % ( projParName, jobID )
    parFileOut = workDir + 'output/' + '%s_%s.par' % ( projParName, jobID )
    
    output_filename = "output_"+str(year);
    output_filename += "_" + dalitz_region
    output_filename += "_" + polarity
    output_filename += "_pt_" + str(pt_bin_min)
    output_filename += "_" + str(pt_bin_max)
    output_filename += "_y_" + str(y_bin_min)
    output_filename += "_" + str(y_bin_max)
    outputName = "full_correlation_table_kpi_asymmetry/output/stbc_" + output_filename
    output_total = outputName + "/"
    
    fscript.write('echo "Hello, I am running on `hostname` in `pwd` and will call `which python` with arguments ${ARGUMENTS_P1}" \n')
    fscript.write('cd /user/laurentd/projectdir/tuple_analysis/AsymmetryTools/projects/KPiTwoTrackAsymmetry \n')

    fscript.write('eval "mkdir '+ outputName +'" \n')

    
    for other_job_configuration in job_configuration:
        directoryA = "/data/bfys/ldufour/scratch_KPiTwoTrackAsymmetry/stbc_" + output_filename
        
        other_output_filename = "output_"+str(other_job_configuration["year"]);
        other_output_filename += "_" + other_job_configuration["dalitz_region"]
        other_output_filename += "_" + other_job_configuration["polarity"]
        other_output_filename += "_pt_" + str(other_job_configuration["pt_bin_min"])
        other_output_filename += "_" + str(other_job_configuration["pt_bin_max"])
        other_output_filename += "_y_" + str(other_job_configuration["y_bin_min"])
        other_output_filename += "_" + str(other_job_configuration["y_bin_max"])
        directoryB = "/data/bfys/ldufour/scratch_KPiTwoTrackAsymmetry/stbc_" + other_output_filename
            
        fscript.write('eval "mkdir '+ outputName +'/' + other_output_filename + '" \n')
        output_for_this_run = outputName +'/' + other_output_filename + "/"
        
        fscript.write('ARGUMENTS_CORRELATION="%s --directoryA %s --directoryB %s --output_directory %s"\n'
                      %( correlation_script, directoryA + "/output_p1", directoryB + "/output_p1", output_for_this_run  )
        )
        
        fscript.write('eval "python ${ARGUMENTS_CORRELATION}" \n')

    fscript.write('echo "Goodbye $?" \n')

# main script
confSubEnvVars = dict(
    WORKDIR    = workDir[:-1] if workDir.endswith('/') else workDir,
    OUTPUTDIR  = dataOutDir[:-1] if dataOutDir.endswith('/') else dataOutDir,
    SHSCRPTDIR = 'conf_bash_job',
    SHSCRPTNAM = '/AKK_Correlations_%s'%(projParName),

    MAXNUMJOBS = maxJobsInQ,
    SLEEPTIME  = sleepTime,
    START      = startJobIdx,
    STOP       = endJobIdx,
    USERNAME   = username,
    QUEUE      = options.queue,
)

fmainscript=open('submit_jobs.sh','w')
fmainscript.write('#! /usr/bin/env bash\n')

for env_var, value in confSubEnvVars.iteritems():
    fmainscript.write('%s=%s\n'%(env_var,value))

fmainscript.write( 'source %s'%submit_script )

fmainscript.close()
os.system('chmod u+x %s'%fmainscript.name)


# submit production ignoring kill signals
_sbmt_cmd = 'python /project/bfys/vsyropou/repos/variusScripts/handle_distributed_data/submitingScripts/make_submit_nllScans.py'
print
print 'Submit as:'
print "nohup sh -c 'for PROFVAR in <proj_vars> ; do cd ${PROFVAR}; rm -rf *;  %s --parName ${PROFVAR}  --numJobs %s %s ; ./submit_nllScans.sh ; cd ../ ; done;' &"%(_sbmt_cmd,numOfJobs,'--floatExpAssym' )

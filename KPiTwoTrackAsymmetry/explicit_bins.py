"""
In case of using explicit bins instead of the HepML adaptive binning,
the bin-edges in pt/eta/phi are here.

The names of the variables much match the ones in the canonical names,
and are reconstructed from ReweightVariables.
"""

bins_kpipi_to_signal = {}

""""

default_reweigh_kspi_to_kpipi = [
                ReweightVariable( "dplus", "pt" ),
                ReweightVariable( "dplus", "eta" ),
                ReweightVariable( "trigger_pi", "pt" )
                                   ]
"""

bins_kspi_to_kpipi = {
    "dplus_pt": [0, 500, 1000, 1500,  2000, 2500, 3000, 4000, 5000, 5500, 7000, 10000],
    "dplus_eta": [1.9, 2.4, 2.65, 3.0, 3.3, 3.5, 3.8, 4.5, 5.1],
    "dplus_phi": [-3.15, 0.0, 3.15],
    "trigger_pi_pt": [750., 1750, 2000, 2500., 3000, 5000, 7000, 9000, 10000],
    "trigger_pi_eta": [1.9, 2.0, 2.5, 3.1, 5.0],
    "trigger_pi_phi": [-3.15,-2.0,-1.5,-0.5, 0.0, 0.5, 1.5, 2.0, 3.15]
                      }

"""
This script generates tuples with 
 1. The same-sign kaon
 2. The opposite-sign kaon
 3. A random pion from a calibration sample.

This generates a 'sauced' tuple, which enables the 
calculation of the asymmetry introduced by two oppositely
charged kaons in a signal tuple (and no pions involved).
"""

__author__ = "Laurent Dufour <laurent.dufour@cern.ch>"
__date__   = "September 2016"

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from KPiTwoTrackAsymmetry import file_library
from KPiTwoTrackAsymmetry import utilities

from KPiTwoTrackAsymmetry import configuration
from KPiTwoTrackAsymmetry.configuration_definitions import TupleVariableConfiguration
from KPiTwoTrackAsymmetry import calibration_file_configuration

from KPiTwoTrackAsymmetry import weighting
from KPiTwoTrackAsymmetry import plotting

from KPiTwoTrackAsymmetry.saucer import Saucer

import ROOT

def welcome_message():
    print "====="
    print "This application will combine your signal kaons with"
    print "random pions from the calibration sample. "
    print ""
    print "This is done in two steps: "
    print " 1. The calibration sample is reweighted to match your"
    print "    signal kaon. These weights are used to create a "
    print "    PDF for calibration pions."
    print " 2. Your signal tuple is trimmed and combined randomly"
    print "    (by using the earlier created PDF) with the pions"
    print "    from calibration. This is saved in the output file."
    print "====="

def get_program_options():
    parser = utilities.get_default_argument_parser(include_polarity=True, require_polarity=True)

    parser.add_argument('--k_px_alias', required=False,
                        help='Name of the Kaon PX variable in your signal tuple. ')
    parser.add_argument('--k_py_alias', required=False,
                        help='Name of the Kaon PY variable in your signal tuple. ')
    parser.add_argument('--k_pz_alias', required=False,
                        help='Name of the Kaon PZ variable in your signal tuple. ')

    ''' pt eta phi '''
    parser.add_argument('--k_pt_alias', required=False,
                        help='Name of the Kaon PT variable in your signal tuple. ')
    parser.add_argument('--k_eta_alias', required=False,
                        help='Name of the Kaon ETA variable in your signal tuple. ')
    parser.add_argument('--k_phi_alias', required=False,
                        help='Name of the Kaon PHI variable in your signal tuple. ')

    parser.add_argument('--sweight_var', required=False,
                        help='When set, sWeights are used for the given signal tuple.')

    parser.add_argument('--output_directory', required=True,
                        help='Directory where the sauced tuples and output histograms are saved. ' +
                        'If the path does not exist, we will try to create it.')

    parser.add_argument( "-f", "--input_tuple_file", action="store", dest="s_file_kpi",
                       required=True,
                       help="Path to file with signal kaon tuple.")

    parser.add_argument('-bdt', '--use_bdt_reweighting', action='store_true', default=False,
                        help='When true, BDT reweighting is enabled. (slower)')

    parser.add_argument( "-t", "--input_tuple_name", action="store", dest="s_tree_kpi",
                       required=True,
                       help="Path to tree in the root file (e.g. PhiPi/DecayTree)")
    parser.add_argument( "-e", "--extra_saved_branches", action="store", dest="to_save_branches",
                       required=False, nargs='*',
                       help="The list of argument is parsed as signal tuple branch names, will be copied when saucing.")

    return parser    

if __name__ == '__main__':
    """
        This program captures the user input to the configuration of the
        signal tuple. The possibility is to run this script with
        various weighting strategies and momentum variables (pt-eta-phi
        and Cartesian).

        Available options: see --help
    """
    parser = get_program_options()
    options = parser.parse_args()
    
    default_args = utilities.parse_default_arguments( options, include_polarity=True  )
    year = default_args["year"]
    polarity = default_args["polarity"]

    if options.k_pt_alias and options.k_eta_alias and options.k_phi_alias:
        """ use (pt,eta,phi) for the Kaon """
        kaon_signal_particle_config = TupleVariableConfiguration(
                                                                  options.k_pt_alias,
                                                                  options.k_eta_alias,
                                                                  options.k_phi_alias, False)
    elif options.k_px_alias and options.k_py_alias and options.k_pz_alias:
        kaon_signal_particle_config = TupleVariableConfiguration(
                                                                  options.k_px_alias,
                                                                  options.k_py_alias,
                                                                  options.k_pz_alias, True)
    else:
        print "Please specify the kinematics of the signal kaon."
        exit()

    ''' set the actual configuration of the tuple branches in the decay '''
    signal_tuple_confuguration = configuration.decays["signal"]
    signal_tuple_confuguration.daughter_particles["kminus"].set_tuple_configuration( kaon_signal_particle_config )
    del signal_tuple_confuguration.daughter_particles["piplus"]

    if options.sweight_var:
        signal_tuple_confuguration.setSWeightVar( options.sweight_var )

    file_path = options.s_file_kpi
    tuple_path = options.s_tree_kpi

    signal_file_configuration = file_library.TuplePathInformation( file_path, tuple_path )

    signal_file_configuration_library = file_library.MutableFileLibrary("signal")
    signal_file_configuration_library.add_tuple_information(year, polarity, signal_file_configuration)

    output_directory = utilities.ensure_trailing_slash_on_output_directory( options.output_directory )
    utilities.check_output_directory( output_directory )
    utilities.check_output_directory( output_directory + "verify_weighting/" )

    """ So far the parsing of the args. Now the actual work starts. """
    """ Signal distributions have been set."""
    calibration_settings = calibration_file_configuration.get( year, polarity )

    plotting.lhcb_style()
    
    welcome_message();
    """ Perform the reweighter, save the object for the output paths. """
    
    reweighter = weighting.perform_weighting( 
                                             signal_file_configuration_library, signal_tuple_confuguration,
                                             calibration_settings,
                                    weighting.default_reweigh_kpipi_to_signal_kaon_only,
                                    None,
                                    year, polarity, output_directory,
                                    use_BDT=options.use_bdt_reweighting, 
                                    n_bins_kpipi=10, n_bins_kspi=7, 
                                    do_reweighting=True,
                                    reweighting_steps=["kpipi"] )
    
    """ Use the reweighter to get the reweighted tuple locations. """
    path_reweighted_kpipi = reweighter.get_output_filename( "kpipi" )
    
    signal_decay_file_configuration = signal_file_configuration_library
    signal_file_information = signal_decay_file_configuration.get_tuple_information(year, polarity )[-1]

    signal_filename = signal_file_information.file_path
    signal_file = ROOT.TFile.Open( signal_filename, "READONLY" )
    signal_tree = signal_file.Get( signal_file_information.tuple_name )
    
    saucer = Saucer(signal_tree, path_reweighted_kpipi, signal_tuple_confuguration)
    
    if options.to_save_branches:
        saucer.add_branches( options.to_save_branches )
        
    saucer.generate_new_tuple (output_file=output_directory + "sauced_tuple.root")
    
"""
Script to compute the systematic due to the interaction and 
CP violation of the neutral kaon in the KsPi sample.

In effect, computes the correlation, rho, among the KsPi tuples 
and returns the (1-rho) * A(K0-bar). The latter is computed in 
LHCb-ANA-2013-013 (DACP) section 7.3, where it is found that
AD(K0)_LL = -0.0796 for 2011 and  
AD(K0)_LL = -0.0822 for 2012.

The dominating systematic of 0.05% is scaled with the same
factor and returned as well.
"""
from calculate_correlation import calculate_correlation
import argparse
import logging
import os.path

from KPiTwoTrackAsymmetry import configuration
from KPiTwoTrackAsymmetry import utilities

K0BAR_ASYMMETRY= {2011: {"value": -0.0796,
                         "error": 0.05},
                  2012: {"value": -0.0822,
                         "error": 0.05}}

def output_neutral_ak_in_text_file(output_directory, asymmetry, error):
   f = open('{}output_neutral_kaon.txt'.format(output_directory), 'w')
   f.write("{} {}".format(str(asymmetry), str(error)))
   f.close()
   
if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--directoryA', required=True,
                        help='Output directory of set A')
    parser.add_argument('--directoryB', required=True,
                        help='Output directory of set B')
    parser.add_argument('--year', type=int, required=True,
                        choices=configuration.years, help='Year of dataset')
    parser.add_argument('--output_directory', required=True,
                        help='Output directory for Neutral Kaon correction.')
    
    options = parser.parse_args( )
    logging.basicConfig(level=logging.INFO)
    
    directoryA = utilities.ensure_trailing_slash_on_output_directory( options.directoryA )
    directoryB = utilities.ensure_trailing_slash_on_output_directory( options.directoryB )
    output_directory = utilities.ensure_trailing_slash_on_output_directory( options.output_directory )
    utilities.check_output_directory( output_directory )
    
    logging.info("Calculating correlation factor.")
    rho_kspi = calculate_correlation("kspi", directoryA, directoryB,
                                     output_directory)
    logging.info("Ks0 correlation factor: {}".format( str(rho_kspi) ))
    
    asymmetry = K0BAR_ASYMMETRY[ options.year ]["value"] * (1.-rho_kspi)
    asymmetry_error = K0BAR_ASYMMETRY[ options.year ]["error"] * (1.-rho_kspi)
    
    logging.info("Writing k0 correction to {}.".format( output_directory + "output_neutral_kaon.txt"))
    output_neutral_ak_in_text_file( output_directory, asymmetry, asymmetry_error )
"""
Script to visualize the overlap between two samples of 
KsPi and Kpipi reweightings. 

@author Laurent Dufour <laurent.dufour@cern.ch>
"""
from calculate_correlation import get_spectra
from calculate_correlation import plot_kinematics

from KPiTwoTrackAsymmetry import names
from KPiTwoTrackAsymmetry import utilities
from KPiTwoTrackAsymmetry import plotting
from KPiTwoTrackAsymmetry import configuration

import argparse
import os.path
import logging

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--directoryA', required=True,
                        help='Output directory of set A')
    parser.add_argument('--directoryB', required=True,
                        help='Output directory of set B')
    parser.add_argument('--nameA', required=True,
                        help='Legend name for set A')
    parser.add_argument('--nameB', required=True,
                        help='Legend name for set B')
    parser.add_argument('--output_directory', required=True,
                        help='Output directory for correlation.')
    
    options = parser.parse_args( )
    logging.basicConfig(level=logging.INFO)
    
    
    directoryA = utilities.ensure_trailing_slash_on_output_directory( options.directoryA )
    directoryB = utilities.ensure_trailing_slash_on_output_directory( options.directoryB )
    output_directory = utilities.ensure_trailing_slash_on_output_directory( options.output_directory )
    utilities.check_output_directory( output_directory )
    
    plotting.lhcb_style()
    
    input_file_A = ROOT.TFile.Open( directoryA + file_name )
    input_file_B = ROOT.TFile.Open( directoryB + file_name )
    
    tree_A = input_file_A.Get( names.tree_name_reweighter_output )
    tree_B = input_file_B.Get( names.tree_name_reweighter_output )

    utilities.check_output_directory( "output_calculate_correlation/" )
    
    plot_kinematics(decay, tree_A, tree_B,
                    nameA, nameB,
                    output_directory)
    
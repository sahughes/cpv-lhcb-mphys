"""
Script to calculate the correlation between two measurements
of the K-pi asymmetry output.

In essence, takes the inner product of the two weight sets in
both K-pi-pi and Ks-pi to get two correlation coefficients. 

Also outputs the result of the difference of the two sets, with 
the correlation taken into account.

Actually more like a stand-alone application.

@author Laurent Dufour <laurent.dufour@cern.ch>
"""

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

import argparse
import logging
import os.path
import math
import ROOT

from KPiTwoTrackAsymmetry import names
from KPiTwoTrackAsymmetry import utilities
from KPiTwoTrackAsymmetry import plotting
from KPiTwoTrackAsymmetry import configuration

def get_data( filename ):
    channel_values = map(float, open(filename).read().split())

    return [ channel_values[0], channel_values[1] ]

def output_asymmetry_in_text_file(output_directory, asymmetry, error):
   f = open('{}output_combined.txt'.format(output_directory), 'w')
   f.write("{} {}".format(str(asymmetry), str(error)))
   f.close()

def output_correlation_in_text_file(output_directory, rho1, rho2):
   f = open('{}output_rho.txt'.format(output_directory), 'w')
   f.write("{} {}".format(str(rho1), str(rho2)))
   f.close()

def get_spectra( tree, name, particle_name, to_draw_variables, decay_name ):
    bins = {}
    spectra = {}
    normalisation = utilities.normalise_weights(tree, "weight")
    decay = configuration.decays[ decay_name ]
    particle = decay.daughter_particles[ particle_name ]
    
    for variable in to_draw_variables:
        bins[ variable ] = plotting.standard_bins[ variable ]
    
        spectra[variable] = ROOT.TH1F(name + variable, name + variable,
                         len(bins[ variable ])-1, plotting.to_root_bins(bins[ variable ]))
    weight = "{}*weight".format(str(normalisation))
    
    for variable in to_draw_variables:
        #to_draw_variable = particle_name + "_" + variable
        
        to_draw_variable = configuration.get_tformula_for_variable( decay, 
                                                                    particle,
                                                                    variable )
        logging.debug("Drawing {}".format(to_draw_variable))
        
        spectrum_from_temp = ROOT.TH1F(name + "temp_" + variable, name + "temp_" + variable,
                             len(bins[variable])-1, plotting.to_root_bins(bins[variable]))
        tree.Draw( "{}>>{}temp_{}".format(to_draw_variable, name, variable), weight)
        
        spectra[variable].Add( spectrum_from_temp )

    return spectra  

def plot_kinematics( decay_name, tree_A, tree_B, name_A, name_B,
                     output_path ):
    variables = ["pt", "eta", "momentum"]
    particles = configuration.decays[ decay_name ].daughter_particles
    
    for particle_name, particle in particles.iteritems():
        spectra_A = get_spectra( tree_A, decay_name + "_A_" +particle_name, particle_name, variables, decay_name )
        spectra_B = get_spectra( tree_B, decay_name + "_B_" +particle_name, particle_name, variables, decay_name )
        
        for variable in variables:
            plotting.output_kinematic_distributions(
                        ss_kaon_spectrum=spectra_A[ variable ],
                        os_kaon_spectrum=spectra_B[ variable ],
                        header=particle.tlatex_name,
                        particleA=name_A,
                        particleB=name_B,
                        variable=configuration.variables[ variable ],
                        output_file_name="{}comparison_{}_{}_{}".format(output_path,
                                                                        decay_name, 
                                                                        particle_name,
                                                                        variable))

def calculate_correlation( decay, directoryA, directoryB, 
                           output_directory ):
    file_name = "temp_{}.root".format(decay)
    
    if not os.path.isfile(directoryA + file_name) or not os.path.isfile(directoryB + file_name):
        logging.fatal("Could not find tuples (temp_{}) in these directories.".format(decay))
        exit()
    
    input_file_A = ROOT.TFile.Open( directoryA + file_name )
    input_file_B = ROOT.TFile.Open( directoryB + file_name )
    
    tree_A = input_file_A.Get( names.tree_name_reweighter_output )
    tree_B = input_file_B.Get( names.tree_name_reweighter_output )
    
    if (tree_A.GetEntries() != tree_B.GetEntries()):
        logging.fatal("Cannot compute correlation when the number of entries is different.")
        logging.fatal("{} for treeA, {} for tree B".format(
                                                           str(tree_A.GetEntries()),
                                                           str(tree_B.GetEntries())
                                                               ))
        exit()
    

    sum_weightsq_k1 = 0;
    sum_weightsq_k2 = 0;
    sum_prodweight = 0;
    
    # add the reduced weight alias.
    normalisation_A = utilities.normalise_weights(tree_A, "weight")
    normalisation_B = utilities.normalise_weights(tree_B, "weight")
    
    utilities.check_output_directory( output_directory )
    #plot_kinematics(decay, tree_A, tree_B,
    #                nameA, nameB,
    #                output_directory)

    for entryId in range(0, tree_A.GetEntries()):
        tree_A.GetEntry( entryId )
        tree_B.GetEntry( entryId )

        sum_weightsq_k1 += normalisation_A* normalisation_A * tree_A.weight * tree_A.weight;
        sum_weightsq_k2 += normalisation_B*normalisation_B*tree_B.weight*tree_B.weight;
        sum_prodweight += normalisation_A*normalisation_B*tree_A.weight * tree_B.weight;

    input_file_B.Close()
    input_file_A.Close()
    
    return sum_prodweight / (math.sqrt ( sum_weightsq_k1 * sum_weightsq_k2 ));

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--directoryA', required=True,
                        help='Output directory of set A')
    parser.add_argument('--directoryB', required=True,
                        help='Output directory of set B')
    parser.add_argument('--output_directory', required=True,
                        help='Output directory for correlation.')
    
    options = parser.parse_args( )
    logging.basicConfig(level=logging.INFO)
    
    print "Initialising correlation calculator."
    
    
    directoryA = utilities.ensure_trailing_slash_on_output_directory( options.directoryA )
    directoryB = utilities.ensure_trailing_slash_on_output_directory( options.directoryB )
    output_directory = utilities.ensure_trailing_slash_on_output_directory( options.output_directory )
    utilities.check_output_directory( output_directory )
    
    plotting.lhcb_style()
    print "Calculating correlations (Kpipi)..."
    rho_kpipi = calculate_correlation("kpipi", directoryA, directoryB,
                                      output_directory)
    print "Calculating correlations (Kspi)..."
    
    rho_kspi = calculate_correlation("kspi", directoryA, directoryB,
                                      output_directory)
    
    print "Rho_kpipi: {}".format(rho_kpipi)
    print "Rho_kspi: {}".format(rho_kspi)
    
    [value_kpipi_A, sigma_kpipi_A] = get_data(directoryA + "output_kpipi.txt")
    [value_kspi_A, sigma_kspi_A] = get_data(directoryA + "output_kspi.txt")
    [value_kpipi_B, sigma_kpipi_B] = get_data(directoryB + "output_kpipi.txt")
    [value_kspi_B, sigma_kspi_B] = get_data(directoryB + "output_kspi.txt")
    
    total_kpipi = sigma_kpipi_A*sigma_kpipi_A + sigma_kpipi_B*sigma_kpipi_B - 2 * rho_kpipi * sigma_kpipi_A * sigma_kpipi_B
    total_kspi = sigma_kspi_A*sigma_kspi_A + sigma_kspi_B*sigma_kspi_B - 2 * rho_kspi * sigma_kspi_A * sigma_kspi_B
    
    print "Total sigma(A(Kpipi) - A(Kpipi)): "
    print str(math.sqrt(total_kpipi))
    
    print "Total sigma(A(Kspi) - A(Kspi)): "
    print str(math.sqrt(total_kspi))
    
    total_akk = (value_kpipi_A - value_kpipi_B) - (value_kspi_A - value_kspi_B)
    total_error = math.sqrt(total_kspi + total_kpipi)
    print "Total A(kpi):"
    print str(total_error)
    
    output_asymmetry_in_text_file(output_directory, total_akk, total_error)
    output_correlation_in_text_file(output_directory, rho_kpipi, rho_kspi)
    
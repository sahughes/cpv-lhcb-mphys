In here the preparation for generating the asymmetry between two opposite-sign Kaons is placed. The saucing ensures a maximal use of the statistics of the calibration sample: a random Pion from the calibration sample is combined with a signal Kaon to form an artificial (K-Pi+) pair. 

Saucer:
This application will combine your signal kaons with random pions from the calibration sample.
This is done in two steps: 
 1. The calibration sample is reweighted to match your signal kaon. These weights are used to create a  PDF for calibration pions.
 2. Your signal tuple is trimmed and combined randoml (by using the earlier created PDF) with the pions from calibration. This is saved in the output file.
 
Correlation:
Computes the correlation among K-pi-pi and ks-pi calibration tuples, in order to compute the resulting error on the difference of two A(K-pi+).
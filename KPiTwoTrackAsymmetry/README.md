# K-pi asymmetry calculator for analyses.
 Package for determining the K-Pi+ detection asymmetry for any
 physics analysis (including reweighting tools).
 
 Rewritten for support of both Run-I and Run_II analyses.
 See LHCb-INT-2012-027 and [LHCb-INT-2017-023](https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/INT/2017/023/latest/latex/main.pdf) for details on the method.
 This subproject of AsymmetryTools can be checked out and used separately 
 from the rest.
 
 The method has been validated on fast simulation in [LHCb-INT-2017-023](https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/INT/2017/023/latest/latex/main.pdf). 
 Please cite this note when using this tool.
 
 Main structure of program's externals:
1. Provide a tuple with Kaons and Pions
2. Optional: provide sWeight information. 
           Must be in the same tuple.
3. Choose on a binning scheme 
           (default is pt, eta for both pion and kaon)
4. Choose a reweighting strategy: 
           Gradient Boosted (BDT) or binned (traditional).
           BDT is significantly slower, but gives better results.
5. Run the script and validate the reweighting 
           output histograms in the output directory.
6. Verify the Chi^2 of the fit. 
7. The result returned does not take into account 
           the material asymmetry of the neutral kaon in the
           VELO. Corrections for this can be found in. e.g.
           [LHCb-INT-2017-023](https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/INT/2017/023/latest/latex/main.pdf).

 Main structure of program's internals:
1. create a smaller tuple of signal to efficiently do the reweighting
2. load the signal distribution with their sweights
3. Assign weights to the k-pi-pi dataset to the signal dataset:
           Assign weights a 4-dimensional data set with (pt, eta) of pion
           and (pt, eta) of kaon
4. Weight the ks-pi dataset to the k-pi-pi dataset:
           cancel production asymmetry (reweigh D)
           Reweigh a 3-dimensional data set with (pt, eta) of the D+
           and pT of the trigger pion, using the earlier created weights.
           Assign weights to the KsPi sample.
5. Validate data by looking at all generated weights.
6. Fit the asymmetries from the K-pi-pi and Ks-pi samples.
        
 Applications to help calculating the asymmetry of a Kaon pair,
 instead of a K-pi pair, are available in the "akk" directory.

## How do I efficiently download this package
The most efficient way to check out this project is using Git's sparse check-out function.
Below is an example where the HTTPS repository was used. 
```
mkdir AsymmetryTools
cd AsymmetryTools
git init
git remote add -f origin https://gitlab.cern.ch/lhcb-slb/AsymmetryTools.git
git config core.sparseCheckout true
echo "projects/KPiTwoTrackAsymmetry/" >> .git/info/sparse-checkout
git pull origin master
```
The files are now in projects/KPiTwoTrackAsymmetry.

## How to setup this package.
 In order to run the script on lxplus, you might need to install some python modules:
 1. lb-run DaVinci/v42r5 bash --norc
 2. pip install --user root_numpy
 3. pip install --user root_pandas
 4. pip install --user hep_ml

After this, a simple 'lb-run DaVinci/v42r5 bash --norc' in a fresh shell on lxplus will allow the package to work. Note that Pandas >= 0.17 is recommended, as it enables a more 
efficient use of memory.

## How to use this package.
 An example is the following:
` python main.py --year 2016 --polarity magdown  --output_directory cluster_output/  -f input_tuple.root --input_tuple_name MyAnalysis/DecayTree  --k_eta_alias K_LOKI_ETA --k_pt_alias K_PT --k_phi_alias K_LOKI_PHI  --pi_eta_alias Pi_LOKI_ETA --pi_pt_alias Pi_PT --pi_phi_alias Pi_LOKI_PHI`

 When your tuple entries are not [pt, eta, phi] but [px, py, pz], 
 you can use --k_px_alias etc, this script will automatically recalculate
 the [pt,eta,phi] variables from this.
 
 
 In order to align the PID cuts of the K-pi-pi and Ks-pi sample to the ones of your sample, you can pass as argument also the following commands: 
 `--k_pidk=7 --pi_pidk=-5 `
that will apply the cuts DLL_Kpi(K) > 7 and DLL_Kpi(Pi)<-5 on the K-pi-pi and Ks-pi samples.
 
## Authors
 Authors include:
 * Adam Davis       <adam.davis@cern.ch>
 * Laurent Dufour   <laurent.dufour@cern.ch>
 * Fabio Ferrari    <fabio.ferrari@cern.ch>
 * Sascha Stahl     <sascha.stahl@cern.ch>
 * Jeroen Van Tilburg <jeroen.van.tilburg@cern.ch>
 * Mika Vesterinen  <mika.vesterinen@cern.ch>
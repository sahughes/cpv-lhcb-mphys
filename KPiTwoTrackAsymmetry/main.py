""""
 K-pi asymmetry calculator for analyses.
 Rewritten for support of 2015, 2016 analyses.
 Run-I is still supported. Has tools on board for
 some systematics as well.

 See LHCb-INT-2012-027 for details on the method itself.
 LHCb-ANA-2013-050 contains a good overview of the method as well.
 
 A new note with an actual validation of the method is in preparation. 
 
 Main structure of program's externals:
        1. Provide a tuple with kaons and pions
        2. Optional: provide sWeight information. 
           Must be in the same tuple.
        3. Choose on a binning scheme 
           (default is pt, eta for both pion and kaon)
        4. Choose a reweighting strategy: 
           Gradient Boosted (BDT) or binned (traditional).
           BDT is significantly slower, but gives better results.
        5. Run the script and validate the reweighting 
           output histograms in the output directory.
        6. Verify the Chi^2 of the fit. 
        7. The result returned does not take into account 
           the material asymmetry of the neutral kaon in the
           VELO. Corrections for this can be found in e.g.
           the Run-I DACP ANA note. 

 Main structure of program's internals:
        1. create a smaller tuple of signal to efficiently do the reweighting
        2. load the signal distribution with their sweights
        3. Assign weights to the k-pi-pi dataset to the signal dataset:
           Assign weights a 4-dimensional data set with (pt, eta) of pion
           and (pt, eta) of kaon
        4. Weight the ks-pi dataset to the k-pi-pi dataset:
           cancel production asymmetry (reweigh D)
           Reweigh a 3-dimensional data set with (pt, eta) of the D+
           and pT of the trigger pion, using the earlier created weights.
           Assign weights to the KsPi sample.
        5. Validate data by looking at all generated weights.
        6. Fit the asymmetries from the K-pi-pi and Ks-pi samples.

 Applications to help calculating the asymmetry of a Kaon pair,
 instead of a K-pi pair, are available in the "akk" directory.

 @author Adam Davis       <adam.davis@cern.ch>
         Laurent Dufour   <laurent.dufour@cern.ch>
         Fabio Ferrari    <fabio.ferrari@cern.ch>
         Sascha Stahl     <sascha.stahl@cern.ch>
         Mika Vesterinen  <mika.vesterinen@cern.ch>
"""

from KPiTwoTrackAsymmetry import file_library
from KPiTwoTrackAsymmetry import utilities

from KPiTwoTrackAsymmetry import configuration
from KPiTwoTrackAsymmetry.configuration_definitions import TupleVariableConfiguration
from KPiTwoTrackAsymmetry import calibration_file_configuration

from KPiTwoTrackAsymmetry import weighting
from KPiTwoTrackAsymmetry import plotting

from KPiTwoTrackAsymmetry.fit.HistogramBasedFitter import HistogramBasedAsymmetryFitter

import explicit_bins

def output_asymmetry_in_text_file(output_directory, decay, asymmetry, error, chi2_sum=0.0,chi2_diff=0.0):
   f = open('{}output_{}.txt'.format(output_directory, decay), 'w')
   f.write("{} {}\n".format(str(asymmetry), str(error)))
   f.write("{} {}".format(str(chi2_sum), str(chi2_diff)))
   f.close()
   
   return '{}output_{}.txt'.format(output_directory, decay)
   
def get_program_options():
    parser = utilities.get_default_argument_parser(include_polarity=True, require_polarity=True)

    parser.add_argument('--k_px_alias', required=False,
                        help='Name of the Kaon PX variable in your signal tuple. ')
    parser.add_argument('--k_py_alias', required=False,
                        help='Name of the Kaon PY variable in your signal tuple. ')
    parser.add_argument('--k_pz_alias', required=False,
                        help='Name of the Kaon PZ variable in your signal tuple. ')

    parser.add_argument('--pi_px_alias', required=False,
                        help='Name of the pion PX variable in your signal tuple. ')
    parser.add_argument('--pi_py_alias', required=False,
                        help='Name of the pion PX variable in your signal tuple. ')
    parser.add_argument('--pi_pz_alias', required=False,
                        help='Name of the pion PX variable in your signal tuple. ')

    ''' pt eta phi '''
    parser.add_argument('--pi_pt_alias', required=False,
                        help='Name of the pion PT variable in your signal tuple. ')
    parser.add_argument('--pi_eta_alias', required=False,
                        help='Name of the pion ETA variable in your signal tuple. ')
    parser.add_argument('--pi_phi_alias', required=False,
                        help='Name of the pion PHI variable in your signal tuple. ')

    parser.add_argument('--k_pt_alias', required=False,
                        help='Name of the Kaon PT variable in your signal tuple. ')
    parser.add_argument('--k_eta_alias', required=False,
                        help='Name of the Kaon ETA variable in your signal tuple. ')
    parser.add_argument('--k_phi_alias', required=False,
                        help='Name of the Kaon PHI variable in your signal tuple. ')
    
    
    parser.add_argument('--k_pidk', required=False, type=float,
                        help='Optional PIDK cut on the calibration kaon. Only changeable for 2016. ')
    parser.add_argument('--k_probnnk', required=False, type=float,
                        help='Optional ProbNNk cut on the calibration kaon. Only changeable for 2016. ')

    parser.add_argument('--pi_pidk', required=False, type=float,
                        help='Optional PIDK cut on the calibration Pion. Only changeable for 2016. ')
    
    
    parser.add_argument('--no_fiducial_cut', required=False, action='store_true',
                        help='By default, cuts are applied in the KPiPi pt/eta to enhance overlap. When this is true, the cut is no longer applied. Mostly effective for 2016. ')

    parser.add_argument('--external_fitter', required=False, action='store_true',
                        help='When true, an alternative fit model is used (RooRadiative + Gaussians).')

    parser.add_argument('--sweight_var', required=False,
                        help='When set, sWeights are used for the given signal tuple.')

    parser.add_argument('--output_directory', required=True,
                        help='Directory where the reweighted tuples and output histograms are saved. ' +
                        'If the path does not exist, we will try to create it.')

    parser.add_argument( "-f", "--input_tuple_file", action="store", dest="s_file_kpi",
                       required=True,
                       help="Path to file with signal tuples.")

    parser.add_argument('-bdt', '--use_bdt_reweighting', action='store_true', default=False,
                        help='When true, BDT reweighting is enabled. (slower)')
    
    parser.add_argument( "--n_bins_kpipi_to_signal", action="store", dest="n_bins_kpipi",
                       default=20, type=int, 
                       help="The number of bins to be used in the reweighting, per dimension (usually there are 3 to 4 dimensions per weighting.)")
    parser.add_argument( "--n_bins_kspi_to_kpipi", action="store", dest="n_bins_kspi",
                       default=14, type=int, 
                       help="The number of bins to be used in the reweighting of the KsPi to Kpipi, per dimension (usually there are 3 to 4 dimensions per weighting.)")

    #parser.add_argument('-noreweighting', '--use_bdt_reweighting', action='store_true', default=False,
    #                    help='When true, BDT reweighting is enabled. (slower)')
    
    parser.add_argument( "-t", "--input_tuple_name", action="store", dest="s_tree_kpi",
                       required=True,
                       help="Path to tree in the root file (e.g. PhiPi/DecayTree)")
    
    parser.add_argument( "--low_stats", action="store_true", dest="low_stats",
                       default=False, 
                       help="When true, a smaller calibration sample will be used when available.")
    
    parser.add_argument( "--skip_reweighting", action="store_true", dest="skip_reweighting",
                       default=False, 
                       help="When true, the reweighter will assume all temp files are there.")
    
    # Advanced options
    parser.add_argument( "--adv_weight_truncation", action="store", dest="adv_weight_truncation",
                       default=80., type=float, 
                       help="Maximum weight for events. Any weight above this value will be truncated to this value. (i.e. weight=max(truncated, event_weight))."
                       " (default 80)")
    
    parser.add_argument( "--adv_dplus_pt_fiducial_cut", action="store", dest="adv_dplus_pt_fiducial_cut",
                       type=float, 
                       help="Fiducial cut on the transverse momentum of the D+, in GeV. Will be applied for both the D+.")
    
    parser.add_argument( "--adv_use_explicit_bins_kspi_to_kpipi", action="store_true", 
                         dest="adv_use_explicit_bins_kspi_to_kpipi",
                       default=False, 
                       help="When true, uses the explicit bins (as set in explicit_bins.py) to weigh KsPi to KPiPi.")
    
    parser.add_argument( "--adv_use_explicit_bins_kpipi_to_signal", action="store_true", 
                         dest="adv_use_explicit_bins_kpipi_to_signal",
                       default=False, 
                       help="When true, uses the explicit bins (as set in explicit_bins.py) to weigh KPiPi to signal.")
    
    

    return parser

if __name__ == '__main__':
    """
        This program captures the user input to the configuration of the
        signal tuple. The possibility is to run this script with
        various weighting strategies and momentum variables (pt-eta-phi
        and Cartesian).

        Available options: see --help
    """
    parser = get_program_options()
    options = parser.parse_args()
    
    import ROOT
    ROOT.gROOT.ProcessLine(".L src/RooRadiativeTail3.cxx+");
    ROOT.gROOT.ProcessLine(".L src/RooRadiativeTail.cxx+");
    
    print "====="
    print "A(KPi) application initialized."
    print "Some of your settings are iterated below."
    print "====="

    default_args = utilities.parse_default_arguments( options, include_polarity=True  )
    year = default_args["year"]
    polarity = default_args["polarity"]
    print "Running for year {}, magnet polarity {}".format(year, polarity.canonical_name)

    if options.k_pt_alias and options.k_eta_alias and options.k_phi_alias:
        """ use (pt,eta,phi) for the Kaon """
        print "Using pt/eta/phi variables for your signal Kaon."
        kaon_signal_particle_config = TupleVariableConfiguration(
                                                                  options.k_pt_alias,
                                                                  options.k_eta_alias,
                                                                  options.k_phi_alias, False)
    elif options.k_px_alias and options.k_py_alias and options.k_pz_alias:
        print "Using px/pt/pz variables for your signal Kaon."
        kaon_signal_particle_config = TupleVariableConfiguration(
                                                                  options.k_px_alias,
                                                                  options.k_py_alias,
                                                                  options.k_pz_alias, True)
    else:
        print "Please specify the kinematics of the signal kaon."
        exit()

    if (options.pi_pt_alias and options.pi_phi_alias and options.pi_eta_alias):
        """ use (pt,eta,phi) for the Pion """
        print "Using pt/eta/phi variables for your signal Pion."
        pion_signal_particle_config = TupleVariableConfiguration(
                                                                  options.pi_pt_alias,
                                                                  options.pi_eta_alias,
                                                                  options.pi_phi_alias, False)
    elif options.pi_px_alias and options.pi_py_alias and options.pi_pz_alias:
        print "Using px/pt/pz variables for your signal Pion."
        pion_signal_particle_config = TupleVariableConfiguration(
                                                                  options.pi_px_alias,
                                                                  options.pi_py_alias,
                                                                  options.pi_pz_alias, True)
    else:
        print "Please specify the kinematics of the signal pion."
        exit()

    ''' set the actual configuration of the tuple branches in the decay '''
    signal_tuple_confuguration = configuration.decays["signal"]
    signal_tuple_confuguration.daughter_particles["kminus"].set_tuple_configuration( kaon_signal_particle_config )
    signal_tuple_confuguration.daughter_particles["piplus"].set_tuple_configuration( pion_signal_particle_config )

    if options.sweight_var:
        print "Using sWeights for your signal decay, under the branch name {}.".format(options.sweight_var)
        signal_tuple_confuguration.setSWeightVar( options.sweight_var )
    else:
        print "Not using any sWeights for your signal decay (to enable, use --sweight_var=xxx)";
    
    file_path = options.s_file_kpi
    tuple_path = options.s_tree_kpi

    signal_file_configuration = file_library.TuplePathInformation( file_path, tuple_path )

    signal_file_configuration_library = file_library.MutableFileLibrary("signal")
    signal_file_configuration_library.add_tuple_information(year, polarity, signal_file_configuration)
    
    output_directory = utilities.ensure_trailing_slash_on_output_directory( options.output_directory )
    
    utilities.check_output_directory( output_directory )
    utilities.check_output_directory( output_directory + "verify_weighting/" )

    """ So far the parsing of the args. Now the actual work starts. """
    """ Signal distributions have been set."""
    calibration_settings = calibration_file_configuration.get( year, polarity,
                                                               low_stats=options.low_stats )

    """ Add the PIDK cut for 2016. """
    if options.k_pidk or options.pi_pidk:
        calibration_settings["kpipi"]["decay_configuration"].cuts = ""
        
        if year != 2016:
            print "Cannot set the PIDK cut for other years than 2016. It's not avilable for " + str(year) + "..."
            exit()
    
        if options.k_pidk:
            print "Kaons will have a PID cut applied of PIDK>" + str(options.k_pidk)
            print "(Note that the minimum is -5)"
            calibration_settings["kpipi"]["decay_configuration"].addCut( "K_PIDK>" + str(options.k_pidk) )
            calibration_settings["kpipi"]["decay_configuration"].extra_variables_required_for_cut += ["K_PIDK"]
            
        if options.pi_pidk:
            print "Pions will have a PID cut applied of PIDK<" + str(options.pi_pidk)
            print "(Note that the maximum is 10)"
            calibration_settings["kpipi"]["decay_configuration"].addCut( "Pi2_PIDK<" + str(options.pi_pidk) )
            calibration_settings["kpipi"]["decay_configuration"].extra_variables_required_for_cut += ["Pi2_PIDK"]
    else:
        if year != 2016:
            print "Kaons will have a PID cut applied of PIDK>7 [--k_pidk]"
            print "Pions will have a PID cut applied of PIDK<X [--pi_pidk]" # FIXME
        else:
            print "Kaons will have a PID cut applied of PIDK>-5 [--k_pidk]"
            print "Pions will have a PID cut applied of PIDK<10 [--pi_pidk]"

    if options.k_probnnk:
        print "Kaons will have a ProbNNk cut applied of ProbNNk>" + str(options.k_probnnk)
        calibration_settings["kpipi"]["decay_configuration"].addCut( "K_ProbNNk>" + str(options.k_pidk) )
        calibration_settings["kpipi"]["decay_configuration"].extra_variables_required_for_cut += ["K_ProbNNk"]
    
    if options.adv_dplus_pt_fiducial_cut:
        print "D+ will have an upper pT cut applied of pt<" + str(options.adv_dplus_pt_fiducial_cut) + " GeV."
        
        calibration_settings["kpipi"]["decay_configuration"].addCut( "D_PT<" + str(options.adv_dplus_pt_fiducial_cut*1000.0) )
        calibration_settings["kspi"]["decay_configuration"].addCut( "D_PT<" + str(options.adv_dplus_pt_fiducial_cut*1000.0) )
        
    if  not options.no_fiducial_cut:
        formula = "((D_PT/1000.0 < -4.5*(D_ETA - 3) + (15.2)) && (D_PT/1000.0 < 15.5*TMath::TanH(4.0*(D_ETA-1.8))) && (D_PT/1000.0 < 98 - 19.5*D_ETA))"
    
        calibration_settings["kpipi"]["decay_configuration"].addCut( formula )
        calibration_settings["kspi"]["decay_configuration"].addCut( formula )

    if calibration_settings["kpipi"]["decay_configuration"].getCuts() is not None:
        configuration.using_friend_tree_for_illustrations = False
    
    if calibration_settings["kspi"]["decay_configuration"].getCuts() is not None:
        configuration.using_friend_tree_for_illustrations = False

    #calibration_settings["kpipi"]["decay_configuration"].addCut("K_TRACK_CHI2NDOF<3")
    #calibration_settings["kpipi"]["decay_configuration"].addCut("Pi2_TRACK_CHI2NDOF<3")
    #calibration_settings["kpipi"]["decay_configuration"].extra_variables_required_for_cut += ["Pi2_TRACK_CHI2NDOF"]
    #calibration_settings["kpipi"]["decay_configuration"].extra_variables_required_for_cut += ["K_TRACK_CHI2NDOF"]

    plotting.lhcb_style()
    print "Weights are truncated at {}. To change this, use --adv_weight_truncation".format(options.adv_weight_truncation)
    print "===============" + "\n"
    
    """ Init the reweighter, save the object for the output paths. """
    n_bins_kpipi = options.n_bins_kpipi
    n_bins_kspi = options.n_bins_kspi
    
    """   Explicit binning?   """
    if options.adv_use_explicit_bins_kpipi_to_signal:
        explicit_bins_kpipi_to_signal = []
        
        for reweigh_variable in weighting.default_reweigh_kpipi_to_signal:
            bin_edges = explicit_bins.bins_kpipi_to_signal[ reweigh_variable.decay_daughter_name + "_" + reweigh_variable.variable_name ]
            explicit_bins_kpipi_to_signal += [bin_edges]
    else:
        explicit_bins_kpipi_to_signal = None
    
    if options.adv_use_explicit_bins_kspi_to_kpipi:
        explicit_bins_kspi_to_kpipi = []
        
        for reweigh_variable in weighting.default_reweigh_kspi_to_kpipi:
            bin_edges = explicit_bins.bins_kspi_to_kpipi[ reweigh_variable.decay_daughter_name + "_" + reweigh_variable.variable_name ]
            explicit_bins_kspi_to_kpipi += [bin_edges]
    else:
        explicit_bins_kspi_to_kpipi = None

    # Check output directory
    
    """ Actually the reweighting   """ 
    reweighter = weighting.perform_weighting( signal_file_configuration_library, 
                                              signal_tuple_confuguration,
                                              calibration_settings,
                                weighting.default_reweigh_kpipi_to_signal,
                                weighting.default_reweigh_kspi_to_kpipi,
                       year, polarity, output_directory, 
                       options.use_bdt_reweighting, 
                       n_bins_kpipi, n_bins_kspi,
                       explicit_bins_kpipi_to_signal=explicit_bins_kpipi_to_signal,
                       explicit_bins_kspi_to_kpipi=explicit_bins_kspi_to_kpipi,
                       do_reweighting=(not options.skip_reweighting) )
    
    """ Use the reweighter to get the reweighted tuple locations. """
    path_reweighted_kpipi = reweighter.get_output_filename( "kpipi" )
    path_reweighted_kspi = reweighter.get_output_filename( "kspi" )
    
    # get the fit variable names (this is variable per year/magnet polarity)
    fit_variable_kpipi = calibration_settings["kpipi"]["decay_configuration"].get_mass_variable()
    fit_variable_kspi = calibration_settings["kspi"]["decay_configuration"].get_mass_variable()
    
    fitter_kpipi = HistogramBasedAsymmetryFitter( 
                              fit_configuration=calibration_settings["kpipi"]["fit_configuration"],
                              input_file_name=path_reweighted_kpipi,
                              input_tree_name=reweighter.get_weighted_tree_name(),
                              to_fit_variable=fit_variable_kpipi,
                              id_variable="D_ID",
                              weight_variable="weight",
                              decay_configuration=configuration.decays["kpipi"],
                              output_directory=output_directory,
                              low_stats=False
                              )

    fitter_kspi = HistogramBasedAsymmetryFitter( 
                              fit_configuration=calibration_settings["kspi"]["fit_configuration"],
                              input_file_name=path_reweighted_kspi,
                              input_tree_name=reweighter.get_weighted_tree_name(),
                              to_fit_variable=fit_variable_kspi,
                              id_variable="D_ID",
                              weight_variable="weight",
                              decay_configuration=configuration.decays["kspi"],
                              output_directory=output_directory,
                              low_stats=options.low_stats)
    
    fitter_kspi.weight_truncation_value = options.adv_weight_truncation
    fitter_kpipi.weight_truncation_value = options.adv_weight_truncation
    
    fitter_kspi.read_tree()
    [kspi_asymmetry_value, kspi_asymmetry_error ] = fitter_kspi.run_fit()
    
    fitter_kpipi.read_tree()
    [kpipi_asymmetry_value, kpipi_asymmetry_error ] = fitter_kpipi.run_fit()
    
    print "Asymmetries in percent:" 
    print "A(Kpipi) = {} +/- {}".format(kpipi_asymmetry_value, kpipi_asymmetry_error)
    print "A(Kspi) = {} +/- {}".format(kspi_asymmetry_value, kspi_asymmetry_error)
    
    output_filename_kpipi = output_asymmetry_in_text_file( output_directory, "kpipi", kpipi_asymmetry_value, kpipi_asymmetry_error, 
                                   (fitter_kpipi.chi2_positive+fitter_kpipi.chi2_negative), (fitter_kpipi.chi2_positive-fitter_kpipi.chi2_negative) )
    output_filename_kspi = output_asymmetry_in_text_file( output_directory, "kspi",  kspi_asymmetry_value, kspi_asymmetry_error,
                                   (fitter_kspi.chi2_positive+fitter_kspi.chi2_negative), (fitter_kspi.chi2_positive-fitter_kspi.chi2_negative) )
    
    print "Values of the asymmetries and chi2s for Kpipi are written to {}".format(output_filename_kpipi)
    print "Values of the asymmetries and chi2s for Kspi are written to {}".format(output_filename_kspi)
import ROOT

file = ROOT.TFile.Open("test_stbc_output_2011_PhiPi_MagDown_pt_8500_25000_y_30_35/temp_kpipi.root")
tree = file.Get("weightedTree")

histogram = ROOT.TH1F("weighted", "weighted", tree.GetEntries(), 0.5, tree.GetEntries()+0.5)
entryId = 0
print "Creating histogram of weights."
tree.Draw("(Entry$)>>weighted", "weight")

entry = ROOT.RooRealVar("entryId", "entryId", tree.GetEntries(), 0.5, tree.GetEntries()+0.5)
varlist = ROOT.RooArgList(entry)
varset = ROOT.RooArgSet(entry)
dataset = ROOT.RooDataHist( "dataset", "dataset", varlist, histogram)
pdf = ROOT.RooHistPdf("pdf", "pdf", varset, dataset)

generated_dataset = pdf.generate(varset, 1000000) ;
#generated_dataset.Print()

for testindex in range(0, 1000000):
    returnList = generated_dataset.get(testindex)
    
    if returnList:
        print returnList.getRealValue("entryId")
    else:
        print "Could not find entry {}".format(testindex)

frame1 = entry.frame() ;
generated_dataset.plotOn(frame1) ;
canvas = ROOT.TCanvas()
frame1.Draw()
canvas.Print("output.pdf")
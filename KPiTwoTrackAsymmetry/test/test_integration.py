from ROOT import TFile, TTree, TCanvas, TH1F

input_file = TFile.Open("output/temp_kpipi.root", "READONLY")
input_tree = input_file.Get("weightedTree")

input_tree.Draw("weight>>h")
histogram = input_file.Get("h")
print "Integral of weights by histogram: {}".format( histogram.GetMean() * input_tree.GetEntries() )
#print "Integral by histogram2: {}".format( histogram.Integral() )

sumOfWeights = 0
sumOfWeightsSq = 0

for entry in range(0, input_tree.GetEntries()):
    input_tree.GetEntry( entry )
    sumOfWeights = sumOfWeights + input_tree.weight
    sumOfWeightsSq = sumOfWeightsSq + input_tree.weight*input_tree.weight

print "Integral of weights by explicit sum: {}".format(sumOfWeights)

input_tree.Draw("weight*weight>>hh")
histogram = input_file.Get("hh")
print "Integral of weights-squared by histogram: {}".format( histogram.GetMean() * input_tree.GetEntries() )
print "Integral of weights-squared by explicit sum: {}".format(sumOfWeightsSq)
import ROOT
from KPiTwoTrackAsymmetry import utilities
from KPiTwoTrackAsymmetry import plotting

import logging
import random

def pdf_builder_from_weights( tree, weight_var):
    """ 
    Generates a histogram with [0, 1] on the X-axis
    and with bin-sizes corresponding to the normalized 
    weights. Here the weights are seen as a probability 
    for each event, and thus the normalization is s.t.
    the sum equals 1.  
    
    To use the returned pdf: 
        random_number = random.random()
        value = pdf.GetBinContent( pdf.FindBin( random_number ) )
    
    In principle, this could be replaced with a RooHistPdf. 
    However, for a large generation, this showed some  
    unexpected behavior (the .get returned null pointers), 
    so this is kept instead. 
    """
    logger = logging.getLogger("pdf_builder_from_weights")
    normalisation = utilities.compute_integral_of_expression_in_tree(tree, weight_var)
        
    sum_of_weights = 0.0
    bin_edges = tree.GetEntries()*[0.0]
    
    logger.info( "Generating bin edges." )
    entryId = 0
    for entry in tree:
        sum_of_weights += (getattr(tree, weight_var) / normalisation)
        bin_edges[ entryId ] = sum_of_weights
        entryId+= 1
    
    logger.info( "Generating pdf histogram." )
    pdf = ROOT.TH1F( "pdf", "pdf", len(bin_edges)-1, plotting.to_root_bins(bin_edges) )
    for binId in range (0, tree.GetEntries() ):
        pdf.SetBinContent( binId, binId )
    
    return pdf
    
file = ROOT.TFile.Open("test_stbc_output_2011_PhiPi_MagDown_pt_8500_25000_y_30_35/temp_kpipi.root")
tree = file.Get("weightedTree")

pdf = pdf_builder_from_weights( tree, "weight")

data_values = ROOT.TH1F("data_values", "data_values", 200, 0, tree.GetEntries())

for j in range(0,100):
    random_number = random.random()
    value = pdf.GetBinContent( pdf.FindBin( random_number) )
    
    data_values.Fill( value )

canvas = ROOT.TCanvas()
data_values.Draw()
canvas.Print("output_manual.pdf")
canvas.Print("output_manual.png")
"""
callkpipihist.py
calls script that generates histograms from tuple for kpipi

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""

import sys

#from comp_var_veto import plot_X
from kpipihist import plot_distr

############################################################################################################################################################

variables = [['D_ETA', 0, 5, 100], ['D_M', 1810, 1940, 100], ['D_PT', 2000, 15000, 100], ['K_PT', 0, 10000, 100], ['K_ETA', 0, 5, 100], ['K_PHI', -3.5, 3.5, 100], ['Pi1_PT', 0, 10000, 100], ['Pi1_ETA', 1.5, 5, 100], ['Pi1_PHI', -3.5, 3.5, 100], ['Pi2_PT', 0, 10000, 100], ['Pi2_ETA', 1.5, 5, 100], ['Pi2_PHI', -3.5, 3.5, 100], ['PiT_PT', 0, 10000, 100], ['PiT_ETA', 1.5, 5, 100], ['PiT_PHI', -3.5, 3.5, 100], ['PiS_PT', 0, 10000, 100], ['PiS_ETA', 1.5, 5, 100], ['PiS_PHI', -3.5, 3.5, 100] ]

tree_2 = "/afs/cern.ch/user/s/sahughes/public/D_plots/Kpipi_cut.root" #Data

output_tree = "test1.root"

plot_distr(variables, tree_2, output_tree)
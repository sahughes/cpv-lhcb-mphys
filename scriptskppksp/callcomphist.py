"""
callcomphist.py
calls comparative histogram generating function and specifies the parameters to generate histograms for.

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""
import sys

#from comp_var_veto import plot_X
from comphist import plot_distr

############################################################################################################################################################

#define variables we're interested in (in order)
variables1=[['D_M', 1810, 1940, 100], ['D_PT', 2000, 15000, 100], ['D_ETA', 0, 5, 100], ['K_PT', 0, 10000, 100], ['K_ETA', 0, 5, 100], ['K_PHI', -3.5, 3.5, 100], ['PiT_PT', 0, 10000, 100], ['PiT_ETA', 0, 5, 100], ['PiT_PHI', -3.5, 3.5, 100]] #, ['Pi2_PT', 0, 10000, 100], ['Pi2_ETA', 0, 5, 100], ['Pi2_PHI', -3.5, 3.5, 100]
variables2=[['D_M', 1810, 1940, 100], ['D_PT', 2000, 15000, 100], ['D_ETA', 0, 5, 100], ['KS_PT', 0, 10000, 100], ['KS_ETA', 0, 5, 100], ['KS_PHI', -3.5, 3.5, 100], ['H_PT', 0, 10000, 100], ['H_ETA', 0, 5, 100], ['H_PHI', -3.5, 3.5, 100]]

#define input trees
tree_1 = "/afs/cern.ch/user/s/sahughes/public/D_plots/Kpipi_cut.root"
tree_2 = "/afs/cern.ch/user/s/sahughes/public/D_plots/Kspi_cut.root"

#define output location/name etc
output_tree = "dkkspipipi_comp.root"
plot_distr(variables1, variables2, tree_1, tree_2, output_tree)
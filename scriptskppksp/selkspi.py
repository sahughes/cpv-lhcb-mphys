"""
selkspi.py
selects D->KsPi

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""

from ROOT import TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, TLorentzVector
import ROOT as r

############################################################################################################################################################

#Define function to cut histograms

def Apply_cut(Tree):

    import math

    cLight      = 0.299792458
    massB       = 5279.58
    massK       = 493.677
    massKS      = 497.611
    massPi      = 139.57
    massP       = 938.27
    massMu      = 105.658   
    massDp      = 1869.62    

    MyLorentzVector = r.Math.PtEtaPhiMVector
    ThreeVector = r.Math.XYZVector

# get variables from decay tree
    KS_PT          = getattr(Tree,"KS_PT")
    KS_ETA         = getattr(Tree,"KS_ETA")
    KS_PHI         = getattr(Tree,"KS_PHI")
    KS_IPCHI2      = getattr(Tree,"KS_IPCHI2")
    KS_ID          = getattr(Tree,"KS_ID")

    H_PT        = getattr(Tree,"H_PT")
    H_ETA       = getattr(Tree,"H_ETA")
    H_PHI       = getattr(Tree,"H_PHI")
    H_PIDp      = getattr(Tree,"H_PIDp")
    H_IPCHI2    = getattr(Tree,"H_IPCHI2")
    H_PIDK      = getattr(Tree,"H_PIDK")
    H_TOS       = getattr(Tree,"H_Hlt1TrackAllL0Decision_TOS")

    D_M           = getattr(Tree,"D_M")
    D_PT          = getattr(Tree,"D_PT")
    D_ETA         = getattr(Tree,"D_ETA")
    D_PHI         = getattr(Tree,"D_PHI")
    D_TOS         = getattr(Tree,"D_Hlt1TrackAllL0Decision_TOS")


## Stripping cuts#####################################
#trigger pion cuts (H)

    if(H_PT<1600 and H_PT>7000):
        return True
    if(H_ETA<1.9 and H_ETA<4.9):
        return True
    if(H_TOS != 1):
        return True
    if(H_PIDK>0):
        return True;

    #if(Pi2_IPCHI2<9.):
    #    return True

#D meson          
    if(D_PT<2500 and D_PT>12000):
        return True
    if(D_ETA<2.2 and D_ETA>4.6):
        return True
    if(D_TOS != 1):
        return True

#################################################################Create tuples function#################################################################

def write_small_tuple(input_file, output_file):

    import math

    cLight  = 0.299792458
    massB   = 5279.58
    massK   = 493.677
    massPi  = 139.57
    massP   = 938.27
    massMu  = 105.658

    fin = r.TFile(input_file)
    dirin = fin.GetDirectory("KsPiLL")
    tin = dirin.Get("DecayTree")
    fout = r.TFile(output_file,"RECREATE")

    #in principle you could read only the branches that are needed
    #but as you use many of them for the selection, I would leave commented
    #tin.SetBranchStatus("*",0);
    #tin.SetBranchStatus("B_M",1)

    tout = r.TTree("DecayTree","DecayTree")
    r.gROOT.ProcessLine(\
        "struct MyStruct{\
        Float_t Taufloat;\
        Float_t Mcorrfloat;\
        Int_t HPIDKint;\
        Float_t Dmfloat;\
        Float_t Dptfloat;\
        Float_t Detafloat;\
        Float_t Hptfloat;\
        Float_t Hetafloat;\
        Float_t Hphifloat;\
        Float_t KSptfloat;\
        Float_t KSetafloat;\
        Float_t KSphifloat;\
        };")

    from ROOT import MyStruct
    add_vars = MyStruct()
    tout.Branch('tauRaw', r.AddressOf(add_vars,'Taufloat'), 'tauRaw/F')
    tout.Branch('Mcorr', r.AddressOf(add_vars,'Mcorrfloat'), 'Mcorr/F')

    #what we've added

    tout.Branch('D_M', r.AddressOf(add_vars,'Dmfloat'), 'D_M/F')
    tout.Branch('D_PT', r.AddressOf(add_vars,'Dptfloat'), 'D_PT/F')
    tout.Branch('D_ETA', r.AddressOf(add_vars,'Detafloat'), 'D_ETA/F')
 
    tout.Branch('H_PT', r.AddressOf(add_vars,'Hptfloat'), 'H_PT/F')
    tout.Branch('H_PIDK', r.AddressOf(add_vars,'HPIDKint'), 'H_PIDK/I')
    tout.Branch('H_ETA', r.AddressOf(add_vars,'Hetafloat'), 'H_ETA/F')
    tout.Branch('H_PHI', r.AddressOf(add_vars,'Hphifloat'), 'H_PHI/F')

    tout.Branch('KS_PT', r.AddressOf(add_vars,'KSptfloat'), 'KS_PT/F')
    tout.Branch('KS_ETA', r.AddressOf(add_vars,'KSetafloat'), 'KS_ETA/F')
    tout.Branch('KS_PHI', r.AddressOf(add_vars,'KSphifloat'), 'KS_PHI/F')

    counter = 0   

#limit size
    nEntries = 62977

    for entry in xrange(nEntries):
        tin.GetEntry(entry)
        if(entry%10000==0):
            print("Processing event number", entry)
          
        #Apply cuts to read in tree
        if Apply_cut(tin):
            continue 

        counter += 1

        #Add variables

        add_vars.Dmfloat = getattr(tin,"D_M")
        add_vars.Dptfloat = getattr(tin,"D_PT")
        add_vars.Detafloat = getattr(tin,"D_ETA")

        add_vars.Hptfloat = getattr(tin,"H_PT")
        add_vars.Hphifloat = getattr(tin,"H_PHI")
        add_vars.Hetafloat = getattr(tin,"H_ETA")

        add_vars.KSptfloat = getattr(tin,"KS_PT")
        add_vars.KSphifloat = getattr(tin,"KS_PHI")
        add_vars.KSetafloat = getattr(tin,"KS_ETA")

        if( getattr(tin,"H_PIDK") < 0 ):
            add_vars.HIDint = -1
        else:
            add_vars.HIDint = 1
          
        tout.Fill()

    print ("All events processed.")
    print(counter, " events passed the cuts \n")

    tout.Write()
    fout.Close()
    fin.Close()
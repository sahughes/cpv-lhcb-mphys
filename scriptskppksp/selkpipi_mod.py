"""
applies cuts and creates new tuples

"""

from ROOT import TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, TLorentzVector
import ROOT as r

############################################################################################################################################################

#Define function to cut histograms

def Apply_cut(Tree):

    import math

    cLight      = 0.299792458
    massB       = 5279.58
    massK       = 493.677
    massPi      = 139.57
    massP       = 938.27
    massMu      = 105.658   
    massDp      = 1869.62

# get variables from decay tree
    K_PT          = getattr(Tree,"K_PT")
    K_ETA         = getattr(Tree,"K_ETA")
    K_PHI         = getattr(Tree,"K_PHI")
    K_IPCHI2      = getattr(Tree,"K_IPCHI2")
    K_PIDK        = getattr(Tree,"K_PIDK")

    Pi1_PT        = getattr(Tree,"Pi1_PT")
    Pi1_ETA       = getattr(Tree,"Pi1_ETA")
    Pi1_PHI       = getattr(Tree,"Pi1_PHI")
    Pi1_PIDp      = getattr(Tree,"Pi1_PIDp")
    Pi1_IPCHI2    = getattr(Tree,"Pi1_IPCHI2")
    Pi1_PIDK      = getattr(Tree,"Pi1_PIDK")
    Pi1_TOS       = getattr(Tree,"Pi1_Hlt1TrackAllL0Decision_TOS")

    Pi2_PT        = getattr(Tree,"Pi2_PT")
    Pi2_ETA       = getattr(Tree,"Pi2_ETA")
    Pi2_PHI       = getattr(Tree,"Pi2_PHI")
    Pi2_PIDp      = getattr(Tree,"Pi2_PIDp")
    Pi2_IPCHI2    = getattr(Tree,"Pi2_IPCHI2")
    Pi2_PIDK      = getattr(Tree,"Pi2_PIDK")
    Pi2_TOS       = getattr(Tree,"Pi2_Hlt1TrackAllL0Decision_TOS")

    D_M           = getattr(Tree,"D_M")
    D_PT          = getattr(Tree,"D_PT")
    D_ETA         = getattr(Tree,"D_ETA")
    D_PHI         = getattr(Tree,"D_PHI")
    D_TOS	  = getattr(Tree,"D_Hlt2CharmHadD2HHHDecision_TOS")

## Stripping cuts#######################################    So far only for D+ decay channel (nothing that concerns D0 and D*
#trigger pion cuts
#Pi_1 in Kpipi
    if(Pi1_PT>Pi2_PT):
        if(Pi1_PT<1600 and Pi1_PT>7000.):
            return True
	if(Pi1_ETA<1.9 and Pi1_ETA<4.9):
            return True
	if(Pi1_TOS != 1):
	    return True
	if(Pi1_PIDK>0):
            return True

#Pi_2 in Kpipi
    if(Pi2_PT>Pi1_PT):
        if(Pi2_PT<1600 and Pi2_PT>7000.):
            return True
	if(Pi2_ETA<1.9 and Pi2_ETA<4.9):
            return True
	if(Pi2_TOS != 1):
	    return True
	if(Pi2_PIDK>0):
            return True

    #if(Pi2_IPCHI2<9.):
    #    return True

#D meson          
    if(D_PT<2500 and D_PT>12000):
	return True
    if(D_ETA<2.2 and D_ETA>4.6):
	return True
    if(D_TOS != 1):
	return True

def write_small_tuple(input_file, output_file):

    import math

    cLight  = 0.299792458

    massB   = 5279.58
    massK   = 493.677
    massPi  = 139.57
    massP   = 938.27
    massMu  = 105.658


    fin = r.TFile(input_file)
    dirin = fin.GetDirectory("Kpipi")
    tin = dirin.Get("DecayTree")
    fout = r.TFile(output_file,"RECREATE")

    #in principle you could read only the branches that are needed
    #but as you use many of them for the selection, I would leave commented
    #tin.SetBranchStatus("*",0);
    #tin.SetBranchStatus("B_M",1)

    tout = r.TTree("DecayTree","DecayTree")
    r.gROOT.ProcessLine(\
        "struct MyStruct{\
        Int_t Pi1PIDKint;\
        Int_t Pi2PIDKint;\
        Float_t Dmfloat;\
        Float_t Dptfloat;\
        Float_t Detafloat;\
        Float_t Pi1ptfloat;\
        Float_t Pi1etafloat;\
        Float_t Pi1phifloat;\
        Float_t Pi2ptfloat;\
        Float_t Pi2etafloat;\
        Float_t Pi2phifloat;\
        Float_t Kptfloat;\
        Float_t Ketafloat;\
        Float_t Kphifloat;\
        };")
    from ROOT import MyStruct
    add_vars = MyStruct()

    #add Taufloat and Mcorrfloat to above list
    #tout.Branch('tauRaw', r.AddressOf(add_vars,'Taufloat'), 'tauRaw/F')
    #tout.Branch('Mcorr', r.AddressOf(add_vars,'Mcorrfloat'), 'Mcorr/F')

    #what we've added

    tout.Branch('D_M', r.AddressOf(add_vars,'Dmfloat'), 'D_M/F')
    tout.Branch('D_PT', r.AddressOf(add_vars,'Dptfloat'), 'D_PT/F')
    tout.Branch('D_ETA', r.AddressOf(add_vars,'Detafloat'), 'D_ETA')
    
    tout.Branch('Pi1_PT', r.AddressOf(add_vars,'Pi1ptfloat'), 'Pi1_PT/F')
    tout.Branch('Pi1_PIDK', r.AddressOf(add_vars,'Pi1PIDKint'), 'Pi1_PIDK/I')
    tout.Branch('Pi1_ETA', r.AddressOf(add_vars,'Pi1etafloat'), 'Pi1_ETA/F')
    tout.Branch('Pi1_PHI', r.AddressOf(add_vars,'Pi1phifloat'), 'Pi1_PHI/F')
    
    tout.Branch('Pi2_PT', r.AddressOf(add_vars,'Pi2ptfloat'), 'Pi2_PT/F')
    tout.Branch('Pi2_PIDK', r.AddressOf(add_vars,'Pi2PIDKint'), 'Pi2_PIDK/I')
    tout.Branch('Pi2_ETA', r.AddressOf(add_vars,'Pi2etafloat'), 'Pi2_ETA/F')
    tout.Branch('Pi2_PHI', r.AddressOf(add_vars,'Pi2phifloat'), 'Pi2_PHI/F')
### New branch for trigger pion        
    tout.Branch('PiT_PT', r.AddressOf(add_vars,'Pitptfloat'), 'PiT_PT/F')
    tout.Branch('PiT_PIDK', r.AddressOf(add_vars,'PitPIDKint'), 'PiT_PIDK/I')
    tout.Branch('PiT_ETA', r.AddressOf(add_vars,'Pitetafloat'), 'PiT_ETA/F')
    tout.Branch('PiT_PHI', r.AddressOf(add_vars,'Pitphifloat'), 'PiT_PHI/F')
###
    tout.Branch('K_PT', r.AddressOf(add_vars,'Kptfloat'), 'K_PT/F')
    tout.Branch('K_ETA', r.AddressOf(add_vars,'Ketafloat'), 'K_ETA/F')
    tout.Branch('K_PHI', r.AddressOf(add_vars,'Kphifloat'), 'K_PHI/F')

#   nEntries=tin.GetEntriesFast()
    counter = 0   
#limit size
    nEntries = 100000

    for entry in xrange(nEntries):
        tin.GetEntry(entry)
        if(entry%10000==0):
            print("Processing event number", entry)
            
        #Apply cuts to read in tree
        if Apply_cut(tin):
            continue 

        counter += 1

        #Add variables

        add_vars.Dmfloat = getattr(tin,"D_M")
        add_vars.Dptfloat = getattr(tin,"D_PT")
        add_vars.Detafloat = getattr(tin,"D_ETA")

        add_vars.Pi1ptfloat = getattr(tin,"Pi1_PT")
        add_vars.Pi1phifloat = getattr(tin,"Pi1_PHI")
        add_vars.Pi1etafloat = getattr(tin,"Pi1_ETA")

        add_vars.Pi2ptfloat = getattr(tin,"Pi2_PT")
        add_vars.Pi2phifloat = getattr(tin,"Pi2_PHI")
        add_vars.Pi2etafloat = getattr(tin,"Pi2_ETA")

        add_vars.Kptfloat = getattr(tin,"K_PT")
        add_vars.Kphifloat = getattr(tin,"K_PHI")
        add_vars.Ketafloat = getattr(tin,"K_ETA")
        
        ### Trigger Pion
        
        if (Pi1_PT>Pi2_PT):
            add_vars.Pitptfloat = getattr(tin,"Pi1_PT")
            add_vars.Pitphifloat = getattr(tin,"Pi1_PHI")
            add_vars.Pitetafloat = getattr(tin,"Pi1_ETA")
        else:
            add_vars.Pitptfloat = getattr(tin,"Pi2_PT")
            add_vars.Pitphifloat = getattr(tin,"Pi2_PHI")
            add_vars.Pitetafloat = getattr(tin,"Pi2_ETA")
            
        ### End of input
            


        if( getattr(tin,"Pi1_PIDK") < 0 ):
            add_vars.Pi1IDint = -1
        else:
            add_vars.Pi1IDint = 1

        if( getattr(tin,"Pi2_PIDK") < 0 ):
            add_vars.Pi2IDint = -1
        else:
            add_vars.Pi2IDint = 1

        tout.Fill()
        
    

    print ("All events processed.")
    print(counter, " events passed the cuts \n")


    tout.Write()
    fout.Close()
    fin.Close()
"""
callselkspi.py
calls kspi selection program

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""

import sys

from selkspi import write_small_tuple

############################################################################################################################################################

tree_2 = "/eos/lhcb/user/l/lgrillo/AsldRunITuples/SLB_ntuples_v3_241213/ControlSamples/v2r0/KpipiKsPi_Strip20_MagDown_12.root" #Run I data

output_tree = "Kspi_cut.root"

write_small_tuple(tree_2,output_tree)
"""
DBcallcomphist.py
calls comparative histogram generating function and specifies the parameters to generate histograms for.
adapted for D meson/B signal comparison

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""
import sys

#from comp_var_veto import plot_X
from DBcomphist import plot_distr

############################################################################################################################################################

#define variables we're interested in (in order)
variables1=[['PiT_PT', 0, 10000, 100], ['PiT_ETA', 0, 5, 100], ['PiT_PHI', -3.5, 3.5, 100],['PiS_PT', 0, 10000, 100], ['PiS_ETA', 0, 5, 100], ['PiS_PHI', -3.5, 3.5, 100]]
variables2=[['PiT_PT', 0, 10000, 100], ['PiT_ETA', 0, 5, 100], ['PiT_PHI', -3.5, 3.5, 100],['PiS_PT', 0, 10000, 100], ['PiS_ETA', 0, 5, 100], ['PiS_PHI', -3.5, 3.5, 100]]

#define input trees
tree_1 = "/afs/cern.ch/user/s/sahughes/public/D_plots/Kpipi_cut.root"
tree_2 = "/afs/cern.ch/user/s/sahughes/public/D_plots/Bsig2k15MD.root"

#define output location/name etc
output_tree = "KBcomp.root"
plot_distr(variables1, variables2, tree_1, tree_2, output_tree)
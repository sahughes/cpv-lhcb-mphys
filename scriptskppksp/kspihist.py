"""
kspihist.py
generates histogram for input kspi tuple

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

28 February 2019

"""

from ROOT import TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, TLorentzVector
import ROOT as r
import math

############################################################################################################################################################

# plot histograms
def plot_distr(variables, tree_2,  output_tree):

    # constants (needed for cuts)
    cLight      = 0.299792458
    massB       = 5279.58
    massK       = 493.677
    massPi      = 139.57
    massP       = 938.27
    massMu      = 105.658   
    massDp      = 1869.62 
    # define 4-vector and 3-vector classes
    MyLorentzVector = r.Math.PtEtaPhiMVector
    ThreeVector = r.Math.XYZVector
    
    # get variables from decay tree
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()

    input2=tree_2;

    print("input", input2)
    
    file2 = TFile.Open(input2);
    Tree = file2.Get("DecayTree")

	
    nEntries2=Tree.GetEntriesFast()                                                                                                                  
                                                                                                                        
    print("\nEntries in Kspi tuple = ", nEntries2)
    
    #limit large dataset
    nEntries2 = 38746	
    n_vars=len(variables)
    # create arrays for data
    histograms=[]

    # create canvas
    c=r.TCanvas()
    # specify stat box content
    r.gStyle.SetOptStat(2222)
    
    # set up arrays of histograms to fill with data
    for i in range(n_vars):
        title2="Kspi "+str(variables[i][0])
        name2=str(variables[i][0])+'_Kpipi'
        histograms.append(TH1F(name2, title2, variables[i][3], variables[i][1], variables[i][2]))
            
    print("\n\nKspi data:")    
    for entry in range(nEntries2):
        if(entry%10000==0):
            print(entry)
        
        Tree.GetEntry(entry) 
        # get variables from decay tree
        KS_PT          = getattr(Tree,"KS_PT")
        KS_ETA         = getattr(Tree,"KS_ETA")
        KS_PHI         = getattr(Tree,"KS_PHI")

        H_PT        = getattr(Tree,"H_PT")
        H_ETA       = getattr(Tree,"H_ETA")
        H_PHI       = getattr(Tree,"H_PHI")

        D_M           = getattr(Tree,"D_M")
        D_PT          = getattr(Tree,"D_PT")
        D_ETA         = getattr(Tree,"D_ETA")

        # loop over number of histograms required (number of variables)  
        for i in range(n_vars):

            #else:
            value2 = getattr(Tree,variables[i][0]);

             
            # fill histogram with data
            histograms[i].Fill(value2)
    
    # create file to write out to?
    f = TFile(output_tree,"RECREATE")
        
    for index in range(len(histograms)):
        # draw histograms
        histograms[index].SetMarkerColor(4)
        histograms[index].SetLineColor(4)
        histograms[index].GetXaxis().SetTitle(str(variables[index][0]))
        histograms[index].GetYaxis().SetTitle("Relative number of events")
        histograms[index].Draw()
        c.Update()

        # draw legend
        leg = r.TLegend(0.7,0.35,0.85,0.45)
        leg.SetFillStyle(0)
        leg.SetFillColor(0)
        leg.SetBorderSize(0)
        leg.SetTextFont(132)
        leg.SetTextSize(0.035)
        leg.AddEntry(histograms[index], str(variables[index][0])+' MC', "P")
        leg.Draw()
        c.Update()

	# save histograms as .pdf
        c.SaveAs("./kspiplots/Plot_"+str(variables[index][0])+'.pdf')
        histograms[index].Write()
        f.cd()
        c.Write()
# CPV LHCb MPhys

MPhys project looking at CPV in neutral B meson mixing at LHCb

This is a collection of scripts (mostly) and plots used/modified/created for the project


Samuel Hughes // in collaboration with Zain Akuji
University of Manchester, School of Physics and Astronomy
Manchester, United Kingdom


Any queries, please email me at 
samuel.hughes@cern.ch
"""
calladdsw.py
calls addsw

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

2019

"""
import sys;

from addsw import write_small_tuple;

############################################################################################################################################################

#tree_2 = "Kpipi_cut.root"
#output_tree = "Kpipi_cut_sw.root"

#/eos/lhcb/user/e/egoodwin/forSam

#Data2015_MagDown.root

#/afs/cern.ch/user/s/sahughes/public/week10/tuples

def run_(year,polarity):
    
    name=year+"_"+polarity
    
    inputdir="/eos/lhcb/user/e/egoodwin/forSam/";
    outputdir="/afs/cern.ch/user/s/sahughes/public/week10/tuples/";

    tree_2 = inputdir+"Data"+year+"_"+polarity+".root";
    output_tree = outputdir+"Data"+year+"_"+polarity+"_Sweightsadded.root";
    write_small_tuple(tree_2,output_tree,name);
    

pol=["MagUp","MagUp","MagDown","MagDown"];
yr=["2015","2016","2015","2016"];


for i in range(4):
    pol_=pol[i];
    yr_=yr[i];
        
    print(pol_+","+yr_);
        
    run_(yr_,pol_);

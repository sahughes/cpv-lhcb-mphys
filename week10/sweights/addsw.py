"""
addsw.py
adds sweights to tuple
adds PiT to tuple

MPhys, 2nd Semester

Samuel Hughes // Zain Akuji

30 April 2019

"""

from ROOT import TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, TLorentzVector
import ROOT as r

############################################################################################################################################################

def write_small_tuple(input_file, output_file,name):

    import math

    cLight  = 0.299792458

    massB   = 5279.58
    massK   = 493.677
    massPi  = 139.57
    massP   = 938.27
    massMu  = 105.658
    #mass limits for D mass fit (applied to histogram of invariant D mass)
    #low_mass = 1843.0 #original
    #high_mass = 1897.0x
    low_mass = 1843.0
    high_mass = 1897.0

    print("Getting the original tree...")
    fin_pre = r.TFile(input_file)
    tin_pre = fin_pre.Get("DecayTree")
    
    nentries = tin_pre.GetEntries()
    event = 0.0
    #tin_pre.SetBranchAddress("D_M",event)
    
    fin = r.TFile("/afs/cern.ch/user/s/sahughes/public/week10/sweights/temp/Temp_"+name+".root","recreate")
    tin = tin_pre.CloneTree(0)
    
    for i in range(nentries):
        tin_pre.GetEntry(i)
        D_M_ = getattr(tin_pre,"D_M")
        D_M = float(D_M_)
        if ( D_M < high_mass and D_M > low_mass):
            tin.Fill()
        #event.Clear()
    
    print(tin_pre.GetEntries(),tin.GetEntries())
    
    fout = r.TFile(output_file,"RECREATE")
    print("Tree copied.")

    tout = r.TTree("DecayTree","DecayTree")
    r.gROOT.ProcessLine(\
        "struct MyStruct{\
        Float_t Nsig_float;\
        Float_t Nbkg_float;\
        Int_t Pi1PIDKint;\
        Int_t Pi2PIDKint;\
        Int_t PitPIDKint;\
        Int_t PisPIDKint;\
        Float_t Dmfloat;\
        Float_t Dptfloat;\
        Float_t Detafloat;\
        Float_t Dphifloat;\
        Float_t Pi1ptfloat;\
        Float_t Pi1etafloat;\
        Float_t Pi1phifloat;\
        Float_t Pi2ptfloat;\
        Float_t Pi2etafloat;\
        Float_t Pi2phifloat;\
        Float_t Pitptfloat;\
        Float_t Pitetafloat;\
        Float_t Pitphifloat;\
        Float_t Pisptfloat;\
        Float_t Pisetafloat;\
        Float_t Pisphifloat;\
        Float_t Kptfloat;\
        Float_t Ketafloat;\
        Float_t Kphifloat;\
        Float_t SWDmfloat;\
        };")
    from ROOT import MyStruct
    add_vars = MyStruct()
    
#################################################_____------>>>>> calculate sweights
    
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()
    
    Dplus_M = r.RooRealVar("D_M","Mass D^{+}",low_mass,high_mass,"MeV/#it{c}^{2}")#tightM
    data = r.RooDataSet("data","fit input dataset", tin, r.RooArgSet( Dplus_M ))#, pass_all_cuts ), "pass_all_cuts>0")
    FC = r.RooRealVar("FC","FC",0.50,0.0,1.0);
    fracS = r.RooFormulaVar("fracS","(1.0-FC)",r.RooArgList(FC));
    N_sig =  r.RooRealVar ("N_sig", "N_sig", 20.E5, 0., 10.E6);
    N_bkg =  r.RooRealVar ("N_bkg", "N_bkg", 5.E5, 0., 10.E6);
    mean1 = r.RooRealVar("mean1","mean1",1866.1,1830.0,1900.0);
    sigma1 = r.RooRealVar("sigma1","sigma1",5.0,3.0,20.0);
    gauss1 = r.RooGaussian("#gauss1","gauss(D_M, mean1, sigma1)",Dplus_M,mean1,sigma1);
    sigma2 = r.RooRealVar("sigma2","sigma2",10.0,6.0,25.0);
    gauss2 = r.RooGaussian("#gauss2","gauss(D_M, mean1, sigma2)",Dplus_M,mean1,sigma2);
    
    SignalmassPdf = r.RooAddPdf("SignalmassPdf","Signal pdf for mass", r.RooArgList(gauss1,gauss2), r.RooArgList(FC, fracS));
    
    par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
    
    Bkg_D_M = r.RooChebychev("Bkg_D_M","Bkg_D_M", Dplus_M, r.RooArgList(par));
    
    model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_D_M), r.RooArgList(N_sig,N_bkg))
    model.fitTo(data,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(1,r.kTRUE),r.RooFit.PrintLevel(1));

    c=r.TCanvas()
    c.cd();
    Cmesframe = Dplus_M.frame(r.RooFit.Title("#font[12]{D^{+}} Mass;#font[12]{M(#font[12]{K^{-}#pi^{+}})} [MeV/#font[12]{c^{2}}]; Events/(60 MeV/#font[12]{c^{2}})"));

    data.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
    model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
    model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
    model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_D_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
    Cmesframe.Draw()
    c.Update()
    #raw_input("continue, press enter")
    c.SaveAs("/afs/cern.ch/user/s/sahughes/public/week10/sweights/fits/sweights_"+name+".C")
    mean1.setConstant()
    sigma1.setConstant()
    par.setConstant()
    N_sig.setConstant()
    N_bkg.setConstant()
    
    sData = r.RooStats.SPlot("sData","An sPlot", data, model, r.RooArgList(N_sig, N_bkg))

#################################################_____------>>>>>

###initialise output branches

    tout.Branch('D_M', r.AddressOf(add_vars,'Dmfloat'), 'D_M/F')
    
    tout.Branch('Pi1_PT', r.AddressOf(add_vars,'Pi1ptfloat'), 'Pi1_PT/F')
    tout.Branch('Pi1_ETA', r.AddressOf(add_vars,'Pi1etafloat'), 'Pi1_ETA/F')
    tout.Branch('Pi1_PHI', r.AddressOf(add_vars,'Pi1phifloat'), 'Pi1_PHI/F')
    
    tout.Branch('Pi2_PT', r.AddressOf(add_vars,'Pi2ptfloat'), 'Pi2_PT/F')
    tout.Branch('Pi2_ETA', r.AddressOf(add_vars,'Pi2etafloat'), 'Pi2_ETA/F')
    tout.Branch('Pi2_PHI', r.AddressOf(add_vars,'Pi2phifloat'), 'Pi2_PHI/F')
    
    tout.Branch('PiT_PT', r.AddressOf(add_vars,'Pitptfloat'), 'PiT_PT/F')
    tout.Branch('PiT_PIDK', r.AddressOf(add_vars,'PitPIDKint'), 'PiT_PIDK/I')
    tout.Branch('PiT_ETA', r.AddressOf(add_vars,'Pitetafloat'), 'PiT_ETA/F')
    tout.Branch('PiT_PHI', r.AddressOf(add_vars,'Pitphifloat'), 'PiT_PHI/F')

    tout.Branch('K_PT', r.AddressOf(add_vars,'Kptfloat'), 'K_PT/F')
    tout.Branch('K_ETA', r.AddressOf(add_vars,'Ketafloat'), 'K_ETA/F')
    tout.Branch('K_PHI', r.AddressOf(add_vars,'Kphifloat'), 'K_PHI/F')
    
    tout.Branch('N_sig', r.AddressOf(add_vars,'Nsig_float'), 'N_sig/F')
    tout.Branch('N_bkg', r.AddressOf(add_vars,'Nbkg_float'), 'N_bkg/F')

#limit size
    nEntries = tin.GetEntries()

    for entry in xrange(nEntries):
        tin.GetEntry(entry)
        if(entry%10000==0):
            print("Processing event number", entry)
            
        D_Mspool=getattr(tin,"D_M")
        if(D_Mspool<low_mass):
            continue;
        if(D_Mspool>high_mass):
            continue;

        pi1_pt=getattr(tin,"Pi1_PT")
        pi2_pt=getattr(tin,"Pi2_PT")
        
        add_vars.Dmfloat = getattr(tin,"D_M")

        add_vars.Pi1ptfloat = getattr(tin,"Pi1_PT")
        add_vars.Pi1phifloat = getattr(tin,"Pi1_PHI")
        add_vars.Pi1etafloat = getattr(tin,"Pi1_ETA")

        add_vars.Pi2ptfloat = getattr(tin,"Pi2_PT")
        add_vars.Pi2phifloat = getattr(tin,"Pi2_PHI")
        add_vars.Pi2etafloat = getattr(tin,"Pi2_ETA")

        add_vars.Kptfloat = getattr(tin,"K_PT")
        add_vars.Kphifloat = getattr(tin,"K_PHI")
        add_vars.Ketafloat = getattr(tin,"K_ETA")
        
        if(pi1_pt>pi2_pt):

            add_vars.Pitptfloat = pi1_pt
            add_vars.Pitphifloat = getattr(tin,"Pi1_PHI")
            add_vars.Pitetafloat = getattr(tin,"Pi1_ETA")
        else:
            add_vars.Pitptfloat = pi2_pt
            add_vars.Pitphifloat = getattr(tin,"Pi2_PHI")
            add_vars.Pitetafloat = getattr(tin,"Pi2_ETA")
        
        ### End of input
            
        add_vars.Nsig_float = sData.GetSWeight(entry, "N_sig")
        add_vars.Nbkg_float = sData.GetSWeight(entry, "N_bkg")

        tout.Fill()
    
    print ("Done mate.")

    tout.Write()
    fout.Close()
    fin.Close()
void spectrum_kminus_kminus_eta()
{
//=========Macro generated from canvas: c1_n2/c1_n2
//=========  (Wed May  8 23:12:36 2019) by ROOT version 6.12/06
   TCanvas *c1_n2 = new TCanvas("c1_n2", "c1_n2",10,32,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n2->SetHighLightColor(2);
   c1_n2->Range(1.346914,-0.06070211,5.297531,0.3186861);
   c1_n2->SetFillColor(0);
   c1_n2->SetBorderMode(0);
   c1_n2->SetBorderSize(2);
   c1_n2->SetTickx(1);
   c1_n2->SetTicky(1);
   c1_n2->SetLeftMargin(0.14);
   c1_n2->SetRightMargin(0.05);
   c1_n2->SetTopMargin(0.05);
   c1_n2->SetBottomMargin(0.16);
   c1_n2->SetFrameLineWidth(2);
   c1_n2->SetFrameBorderMode(0);
   c1_n2->SetFrameLineWidth(2);
   c1_n2->SetFrameBorderMode(0);
   Double_t xAxis3[9] = {1.9, 2.4, 2.65, 3, 3.3, 3.5, 3.8, 4.5, 5.1}; 
   
   TH1F *kpipi_to_signal_k_spectra_from_eta__3 = new TH1F("kpipi_to_signal_k_spectra_from_eta__3","kpipi_to_signal_k_spectra_from_eta",8, xAxis3);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(0,0.0001880983);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(1,0.05352265);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(2,0.1694768);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(3,0.2140833);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(4,0.2092486);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(5,0.1813552);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(6,0.1326963);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(7,0.03810747);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(8,0.00150974);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinContent(9,2.023158e-06);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(0,1.506141e-06);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(1,4.049135e-05);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(2,0.0001107414);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(3,0.000109239);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(4,0.0001174918);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(5,0.0001325695);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(6,9.161219e-05);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(7,3.035686e-05);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(8,5.429939e-06);
   kpipi_to_signal_k_spectra_from_eta__3->SetBinError(9,7.992001e-08);
   kpipi_to_signal_k_spectra_from_eta__3->SetMinimum(0);
   kpipi_to_signal_k_spectra_from_eta__3->SetMaximum(0.2997167);
   kpipi_to_signal_k_spectra_from_eta__3->SetEntries(4.332413e+07);
   kpipi_to_signal_k_spectra_from_eta__3->SetStats(0);
   kpipi_to_signal_k_spectra_from_eta__3->SetFillColor(4);
   kpipi_to_signal_k_spectra_from_eta__3->SetFillStyle(3004);
   kpipi_to_signal_k_spectra_from_eta__3->SetLineColor(4);
   kpipi_to_signal_k_spectra_from_eta__3->SetLineWidth(2);
   kpipi_to_signal_k_spectra_from_eta__3->SetMarkerStyle(22);
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetTitle("#it{#eta}");
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetNdivisions(505);
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetLabelFont(132);
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetLabelSize(0.06);
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetTitleSize(0.072);
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_k_spectra_from_eta__3->GetXaxis()->SetTitleFont(132);
   kpipi_to_signal_k_spectra_from_eta__3->GetYaxis()->SetTitle("Candidates (arb. units)");
   kpipi_to_signal_k_spectra_from_eta__3->GetYaxis()->SetLabelFont(132);
   kpipi_to_signal_k_spectra_from_eta__3->GetYaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_k_spectra_from_eta__3->GetYaxis()->SetLabelSize(0.06);
   kpipi_to_signal_k_spectra_from_eta__3->GetYaxis()->SetTitleSize(0.072);
   kpipi_to_signal_k_spectra_from_eta__3->GetYaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_k_spectra_from_eta__3->GetYaxis()->SetTitleFont(132);
   kpipi_to_signal_k_spectra_from_eta__3->GetZaxis()->SetLabelFont(132);
   kpipi_to_signal_k_spectra_from_eta__3->GetZaxis()->SetLabelSize(0.06);
   kpipi_to_signal_k_spectra_from_eta__3->GetZaxis()->SetTitleSize(0.072);
   kpipi_to_signal_k_spectra_from_eta__3->GetZaxis()->SetTitleOffset(1.2);
   kpipi_to_signal_k_spectra_from_eta__3->GetZaxis()->SetTitleFont(132);
   kpipi_to_signal_k_spectra_from_eta__3->Draw("HIST");
   Double_t xAxis4[9] = {1.9, 2.4, 2.65, 3, 3.3, 3.5, 3.8, 4.5, 5.1}; 
   
   TH1F *kpipi_to_signal_k_spectra_to_eta__4 = new TH1F("kpipi_to_signal_k_spectra_to_eta__4","kpipi_to_signal_k_spectra_to_eta",8, xAxis4);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(0,0.0003510964);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(1,0.05412122);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(2,0.1478183);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(3,0.197255);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(4,0.2105345);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(5,0.1895622);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(6,0.1459678);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(7,0.0499118);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(8,0.004829134);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinContent(9,2.188427e-05);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(0,1.640017e-05);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(1,0.0002902693);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(2,0.000676856);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(3,0.0006641303);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(4,0.0007520393);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(5,0.0008848489);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(6,0.0006465407);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(7,0.0002565213);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(8,9.180851e-05);
   kpipi_to_signal_k_spectra_to_eta__4->SetBinError(9,5.085398e-06);
   kpipi_to_signal_k_spectra_to_eta__4->SetEntries(625571);
   kpipi_to_signal_k_spectra_to_eta__4->SetStats(0);
   kpipi_to_signal_k_spectra_to_eta__4->SetFillColor(2);
   kpipi_to_signal_k_spectra_to_eta__4->SetFillStyle(3005);
   kpipi_to_signal_k_spectra_to_eta__4->SetLineColor(2);
   kpipi_to_signal_k_spectra_to_eta__4->SetLineWidth(2);
   kpipi_to_signal_k_spectra_to_eta__4->SetMarkerStyle(23);
   kpipi_to_signal_k_spectra_to_eta__4->GetXaxis()->SetNdivisions(505);
   kpipi_to_signal_k_spectra_to_eta__4->GetXaxis()->SetLabelFont(132);
   kpipi_to_signal_k_spectra_to_eta__4->GetXaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_k_spectra_to_eta__4->GetXaxis()->SetLabelSize(0.06);
   kpipi_to_signal_k_spectra_to_eta__4->GetXaxis()->SetTitleSize(0.072);
   kpipi_to_signal_k_spectra_to_eta__4->GetXaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_k_spectra_to_eta__4->GetXaxis()->SetTitleFont(132);
   kpipi_to_signal_k_spectra_to_eta__4->GetYaxis()->SetLabelFont(132);
   kpipi_to_signal_k_spectra_to_eta__4->GetYaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_k_spectra_to_eta__4->GetYaxis()->SetLabelSize(0.06);
   kpipi_to_signal_k_spectra_to_eta__4->GetYaxis()->SetTitleSize(0.072);
   kpipi_to_signal_k_spectra_to_eta__4->GetYaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_k_spectra_to_eta__4->GetYaxis()->SetTitleFont(132);
   kpipi_to_signal_k_spectra_to_eta__4->GetZaxis()->SetLabelFont(132);
   kpipi_to_signal_k_spectra_to_eta__4->GetZaxis()->SetLabelSize(0.06);
   kpipi_to_signal_k_spectra_to_eta__4->GetZaxis()->SetTitleSize(0.072);
   kpipi_to_signal_k_spectra_to_eta__4->GetZaxis()->SetTitleOffset(1.2);
   kpipi_to_signal_k_spectra_to_eta__4->GetZaxis()->SetTitleFont(132);
   kpipi_to_signal_k_spectra_to_eta__4->Draw("HIST SAME");
   
   TLegend *leg = new TLegend(0.555,0.73,0.89,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetTextSize(0.05);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("kpipi_to_signal_k_spectra_from_eta","[#it{D^{#pm}#rightarrowK^{#mp}#pi^{#pm}#pi^{#pm}}] K^{#mp}","f");
   entry->SetFillColor(4);
   entry->SetFillStyle(3004);
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   entry=leg->AddEntry("kpipi_to_signal_k_spectra_to_eta","[#it{Signal}] K^{#mp}","f");
   entry->SetFillColor(2);
   entry->SetFillStyle(3005);
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   leg->Draw();
   c1_n2->Modified();
   c1_n2->cd();
   c1_n2->SetSelected(c1_n2);
}

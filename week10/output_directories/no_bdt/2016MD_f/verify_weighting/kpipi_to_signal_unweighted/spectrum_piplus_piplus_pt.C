void spectrum_piplus_piplus_pt()
{
//=========Macro generated from canvas: c1_n2/c1_n2
//=========  (Fri May  3 16:13:26 2019) by ROOT version 6.12/06
   TCanvas *c1_n2 = new TCanvas("c1_n2", "c1_n2",10,32,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n2->SetHighLightColor(2);
   c1_n2->Range(-3456.79,-0.08953841,21234.57,0.4700767);
   c1_n2->SetFillColor(0);
   c1_n2->SetBorderMode(0);
   c1_n2->SetBorderSize(2);
   c1_n2->SetTickx(1);
   c1_n2->SetTicky(1);
   c1_n2->SetLeftMargin(0.14);
   c1_n2->SetRightMargin(0.05);
   c1_n2->SetTopMargin(0.05);
   c1_n2->SetBottomMargin(0.16);
   c1_n2->SetFrameLineWidth(2);
   c1_n2->SetFrameBorderMode(0);
   c1_n2->SetFrameLineWidth(2);
   c1_n2->SetFrameBorderMode(0);
   Double_t xAxis21[14] = {0, 500, 1000, 1500, 2000, 2500, 3000, 4000, 5000, 7500, 10000, 13000, 16000, 20000}; 
   
   TH1F *kpipi_to_signal_pi_spectra_from_pt__21 = new TH1F("kpipi_to_signal_pi_spectra_from_pt__21","kpipi_to_signal_pi_spectra_from_pt",13, xAxis21);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(1,0.1794695);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(2,0.3157828);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(3,0.238482);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(4,0.115894);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(5,0.06606238);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(6,0.04409644);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(7,0.02500517);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(8,0.01154109);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(9,0.003294514);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(10,0.0003596287);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(11,1.240928e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinContent(12,6.682436e-09);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(1,5.998233e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(2,7.956504e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(3,6.914422e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(4,4.820129e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(5,3.639193e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(6,2.973239e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(7,1.583171e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(8,1.075564e-05);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(9,3.63445e-06);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(10,1.200799e-06);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(11,2.036225e-07);
   kpipi_to_signal_pi_spectra_from_pt__21->SetBinError(12,4.725196e-09);
   kpipi_to_signal_pi_spectra_from_pt__21->SetMinimum(0);
   kpipi_to_signal_pi_spectra_from_pt__21->SetMaximum(0.4420959);
   kpipi_to_signal_pi_spectra_from_pt__21->SetEntries(5.243722e+07);
   kpipi_to_signal_pi_spectra_from_pt__21->SetStats(0);
   kpipi_to_signal_pi_spectra_from_pt__21->SetFillColor(4);
   kpipi_to_signal_pi_spectra_from_pt__21->SetFillStyle(3004);
   kpipi_to_signal_pi_spectra_from_pt__21->SetLineColor(4);
   kpipi_to_signal_pi_spectra_from_pt__21->SetLineWidth(2);
   kpipi_to_signal_pi_spectra_from_pt__21->SetMarkerStyle(22);
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetTitle("#it{p_{T}} [MeV/#it{c}]");
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetNdivisions(505);
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetLabelFont(132);
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetLabelSize(0.06);
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetTitleSize(0.072);
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_pi_spectra_from_pt__21->GetXaxis()->SetTitleFont(132);
   kpipi_to_signal_pi_spectra_from_pt__21->GetYaxis()->SetTitle("Candidates (arb. units)");
   kpipi_to_signal_pi_spectra_from_pt__21->GetYaxis()->SetLabelFont(132);
   kpipi_to_signal_pi_spectra_from_pt__21->GetYaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_pi_spectra_from_pt__21->GetYaxis()->SetLabelSize(0.06);
   kpipi_to_signal_pi_spectra_from_pt__21->GetYaxis()->SetTitleSize(0.072);
   kpipi_to_signal_pi_spectra_from_pt__21->GetYaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_pi_spectra_from_pt__21->GetYaxis()->SetTitleFont(132);
   kpipi_to_signal_pi_spectra_from_pt__21->GetZaxis()->SetLabelFont(132);
   kpipi_to_signal_pi_spectra_from_pt__21->GetZaxis()->SetLabelSize(0.06);
   kpipi_to_signal_pi_spectra_from_pt__21->GetZaxis()->SetTitleSize(0.072);
   kpipi_to_signal_pi_spectra_from_pt__21->GetZaxis()->SetTitleOffset(1.2);
   kpipi_to_signal_pi_spectra_from_pt__21->GetZaxis()->SetTitleFont(132);
   kpipi_to_signal_pi_spectra_from_pt__21->Draw("HIST");
   Double_t xAxis22[14] = {0, 500, 1000, 1500, 2000, 2500, 3000, 4000, 5000, 7500, 10000, 13000, 16000, 20000}; 
   
   TH1F *kpipi_to_signal_pi_spectra_to_pt__22 = new TH1F("kpipi_to_signal_pi_spectra_to_pt__22","kpipi_to_signal_pi_spectra_to_pt",13, xAxis22);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(1,3.866948e-07);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(3,0.1974828);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(4,0.2890594);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(5,0.2148814);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(6,0.1496841);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(7,0.08774815);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(8,0.04280627);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(9,0.01443562);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(10,0.003016337);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(11,0.00069451);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(12,0.0001577985);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(13,3.32718e-05);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinContent(14,0.02639107);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(1,3.866949e-07);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(3,0.0003096658);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(4,0.0003707275);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(5,0.0003189028);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(6,0.0002676578);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(7,0.000145897);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(8,0.0001030369);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(9,3.851811e-05);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(10,1.840896e-05);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(11,8.325539e-06);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(12,4.159101e-06);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(13,1.774159e-06);
   kpipi_to_signal_pi_spectra_to_pt__22->SetBinError(14,0.003758763);
   kpipi_to_signal_pi_spectra_to_pt__22->SetEntries(4030453);
   kpipi_to_signal_pi_spectra_to_pt__22->SetStats(0);
   kpipi_to_signal_pi_spectra_to_pt__22->SetFillColor(2);
   kpipi_to_signal_pi_spectra_to_pt__22->SetFillStyle(3005);
   kpipi_to_signal_pi_spectra_to_pt__22->SetLineColor(2);
   kpipi_to_signal_pi_spectra_to_pt__22->SetLineWidth(2);
   kpipi_to_signal_pi_spectra_to_pt__22->SetMarkerStyle(23);
   kpipi_to_signal_pi_spectra_to_pt__22->GetXaxis()->SetNdivisions(505);
   kpipi_to_signal_pi_spectra_to_pt__22->GetXaxis()->SetLabelFont(132);
   kpipi_to_signal_pi_spectra_to_pt__22->GetXaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_pi_spectra_to_pt__22->GetXaxis()->SetLabelSize(0.06);
   kpipi_to_signal_pi_spectra_to_pt__22->GetXaxis()->SetTitleSize(0.072);
   kpipi_to_signal_pi_spectra_to_pt__22->GetXaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_pi_spectra_to_pt__22->GetXaxis()->SetTitleFont(132);
   kpipi_to_signal_pi_spectra_to_pt__22->GetYaxis()->SetLabelFont(132);
   kpipi_to_signal_pi_spectra_to_pt__22->GetYaxis()->SetLabelOffset(0.01);
   kpipi_to_signal_pi_spectra_to_pt__22->GetYaxis()->SetLabelSize(0.06);
   kpipi_to_signal_pi_spectra_to_pt__22->GetYaxis()->SetTitleSize(0.072);
   kpipi_to_signal_pi_spectra_to_pt__22->GetYaxis()->SetTitleOffset(0.95);
   kpipi_to_signal_pi_spectra_to_pt__22->GetYaxis()->SetTitleFont(132);
   kpipi_to_signal_pi_spectra_to_pt__22->GetZaxis()->SetLabelFont(132);
   kpipi_to_signal_pi_spectra_to_pt__22->GetZaxis()->SetLabelSize(0.06);
   kpipi_to_signal_pi_spectra_to_pt__22->GetZaxis()->SetTitleSize(0.072);
   kpipi_to_signal_pi_spectra_to_pt__22->GetZaxis()->SetTitleOffset(1.2);
   kpipi_to_signal_pi_spectra_to_pt__22->GetZaxis()->SetTitleFont(132);
   kpipi_to_signal_pi_spectra_to_pt__22->Draw("HIST SAME");
   
   TLegend *leg = new TLegend(0.555,0.73,0.89,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetTextSize(0.05);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("kpipi_to_signal_pi_spectra_from_pt","[#it{D^{#pm}#rightarrowK^{#mp}#pi^{#pm}#pi^{#pm}}] #pi^{#pm}","f");
   entry->SetFillColor(4);
   entry->SetFillStyle(3004);
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   entry=leg->AddEntry("kpipi_to_signal_pi_spectra_to_pt","[#it{Signal}] #pi^{#pm}","f");
   entry->SetFillColor(2);
   entry->SetFillStyle(3005);
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(132);
   leg->Draw();
   c1_n2->Modified();
   c1_n2->cd();
   c1_n2->SetSelected(c1_n2);
}
